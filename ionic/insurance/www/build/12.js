webpackJsonp([12,115,116,117,118],{

/***/ 653:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormLayout1Module", function() { return FormLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__form_layout_1__ = __webpack_require__(831);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FormLayout1Module = /** @class */ (function () {
    function FormLayout1Module() {
    }
    FormLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_1__["a" /* FormLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__form_layout_1__["a" /* FormLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_1__["a" /* FormLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], FormLayout1Module);
    return FormLayout1Module;
}());

//# sourceMappingURL=form-layout-1.module.js.map

/***/ }),

/***/ 654:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormLayout2Module", function() { return FormLayout2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__form_layout_2__ = __webpack_require__(832);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FormLayout2Module = /** @class */ (function () {
    function FormLayout2Module() {
    }
    FormLayout2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_2__["a" /* FormLayout2 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__form_layout_2__["a" /* FormLayout2 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_2__["a" /* FormLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], FormLayout2Module);
    return FormLayout2Module;
}());

//# sourceMappingURL=form-layout-2.module.js.map

/***/ }),

/***/ 655:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormLayout3Module", function() { return FormLayout3Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__form_layout_3__ = __webpack_require__(833);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FormLayout3Module = /** @class */ (function () {
    function FormLayout3Module() {
    }
    FormLayout3Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_3__["a" /* FormLayout3 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__form_layout_3__["a" /* FormLayout3 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_3__["a" /* FormLayout3 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], FormLayout3Module);
    return FormLayout3Module;
}());

//# sourceMappingURL=form-layout-3.module.js.map

/***/ }),

/***/ 656:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormLayout4Module", function() { return FormLayout4Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__form_layout_4__ = __webpack_require__(834);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FormLayout4Module = /** @class */ (function () {
    function FormLayout4Module() {
    }
    FormLayout4Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_4__["a" /* FormLayout4 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__form_layout_4__["a" /* FormLayout4 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__form_layout_4__["a" /* FormLayout4 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], FormLayout4Module);
    return FormLayout4Module;
}());

//# sourceMappingURL=form-layout-4.module.js.map

/***/ }),

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemDetailsPageFormeModule", function() { return ItemDetailsPageFormeModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__item_details_form__ = __webpack_require__(936);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_forms_layout_1_form_layout_1_module__ = __webpack_require__(653);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_forms_layout_2_form_layout_2_module__ = __webpack_require__(654);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_forms_layout_3_form_layout_3_module__ = __webpack_require__(655);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_forms_layout_4_form_layout_4_module__ = __webpack_require__(656);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var ItemDetailsPageFormeModule = /** @class */ (function () {
    function ItemDetailsPageFormeModule() {
    }
    ItemDetailsPageFormeModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__item_details_form__["a" /* ItemDetailsPageForm */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__item_details_form__["a" /* ItemDetailsPageForm */]),
                __WEBPACK_IMPORTED_MODULE_3__components_forms_layout_1_form_layout_1_module__["FormLayout1Module"], __WEBPACK_IMPORTED_MODULE_4__components_forms_layout_2_form_layout_2_module__["FormLayout2Module"], __WEBPACK_IMPORTED_MODULE_5__components_forms_layout_3_form_layout_3_module__["FormLayout3Module"],
                __WEBPACK_IMPORTED_MODULE_6__components_forms_layout_4_form_layout_4_module__["FormLayout4Module"]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ItemDetailsPageFormeModule);
    return ItemDetailsPageFormeModule;
}());

//# sourceMappingURL=item-details-form.module.js.map

/***/ }),

/***/ 831:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FormLayout1 = /** @class */ (function () {
    function FormLayout1() {
    }
    FormLayout1.prototype.onEvent = function (event, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](this.getItemData());
            this.resetValue();
        }
    };
    FormLayout1.prototype.getItemData = function () {
        return {
            'name': this.name,
            'title': this.title,
            'description': this.description
        };
    };
    FormLayout1.prototype.resetValue = function () {
        this.name = "";
        this.title = "";
        this.description = "";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout1.prototype, "events", void 0);
    FormLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'form-layout-1',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-1/form.html"*/'<!--  Theme Form - Form + Write Comment -->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-row padding>\n\n      <ion-col col-12>\n\n        <ion-item no-lines box-shadow>\n\n         <ion-input no-margin type="text" [placeholder]="data.yourName" [(ngModel)]="name"></ion-input>\n\n      </ion-item>\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <ion-item no-lines box-shadow>\n\n           <ion-input no-margin type="text" [placeholder]="data.title" [(ngModel)]="title"></ion-input>\n\n        </ion-item>\n\n      </ion-col>\n\n      <ion-col col-12 textarea>\n\n        <ion-item no-lines box-shadow>\n\n          <ion-textarea no-margin  [placeholder]="data.description" [(ngModel)]="description"></ion-textarea>\n\n        </ion-item>\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <button ion-button button-clear-outline round outline block text-capitalize (click)="onEvent(\'onSubmit\', $event)">\n\n        {{data.button}}\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-1/form.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], FormLayout1);
    return FormLayout1;
}());

//# sourceMappingURL=form-layout-1.js.map

/***/ }),

/***/ 832:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FormLayout2 = /** @class */ (function () {
    function FormLayout2() {
    }
    FormLayout2.prototype.onEvent = function (event, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](this.getItemData());
            this.resetValue();
        }
    };
    FormLayout2.prototype.getItemData = function () {
        return {
            'description': this.description
        };
    };
    FormLayout2.prototype.resetValue = function () {
        this.description = "";
    };
    FormLayout2.prototype.onStarClass = function (items, index, e) {
        for (var i = 0; i < items.length; i++) {
            items[i].isActive = i <= index;
        }
        if (this.events['onRates']) {
            this.events['onRates'](index);
        }
    };
    ;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout2.prototype, "events", void 0);
    FormLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'form-layout-2',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-2/form.html"*/'<!-- Theme Form - Form + Write Reviev -->\n\n  <ion-content>\n\n    <ion-grid *ngIf="data != null">\n\n      <ion-row padding>\n\n        <ion-col col-12 text-center>\n\n          <span font-bold span-small>{{data.rateTitle}}</span>\n\n        </ion-col>\n\n        <ion-col col-12 text-center>\n\n            <ion-icon\n\n              *ngFor="let item of data.iconsStars;let i = index"\n\n              color="dark"\n\n              [name]="item.icon"\n\n              [ngClass]="{\'active\':item.isActive}"\n\n              (click)="onStarClass(data.iconsStars, i, $event)">\n\n            </ion-icon>\n\n        </ion-col>\n\n        <ion-col col-12 text-center>\n\n          <span font-bold span-small>{{data.title}}</span>\n\n        </ion-col>\n\n        <ion-col col-12 textarea>\n\n          <ion-item no-lines box-shadow>\n\n            <ion-textarea [placeholder]="data.descriptionPlaceholder" [(ngModel)]="description"></ion-textarea>\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-12>\n\n          <button ion-button button-clear-outline round outline block text-capitalize (click)="onEvent(\'onSubmit\', $event)">\n\n          {{data.btnSubmit}}\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-2/form.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], FormLayout2);
    return FormLayout2;
}());

//# sourceMappingURL=form-layout-2.js.map

/***/ }),

/***/ 833:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormLayout3; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FormLayout3 = /** @class */ (function () {
    function FormLayout3() {
    }
    FormLayout3.prototype.onEvent = function (event, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](this.getItemData());
            this.resetValue();
        }
    };
    FormLayout3.prototype.getItemData = function () {
        return {
            'firstName': this.firstName,
            'lastName': this.lastName,
            'address1': this.address1,
            'address2': this.address2,
            'city': this.city,
            'zipCode': this.zipCode
        };
    };
    FormLayout3.prototype.resetValue = function () {
        this.firstName = "";
        this.lastName = "";
        this.address1 = "";
        this.address2 = "";
        this.city = "";
        this.zipCode = "";
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout3.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout3.prototype, "events", void 0);
    FormLayout3 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'form-layout-3',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-3/form.html"*/'<!--  Theme Form - Form Write Address -->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-row padding>\n\n      <!-- Full Name -->\n\n      <ion-col col-12 align-self-start>\n\n        <ion-item no-lines box-shadow margin-bottom>\n\n          <ion-input no-margin type="text" [placeholder]="data.firstName" [(ngModel)]="firstName"></ion-input>\n\n       </ion-item>\n\n       <!-- Last Name -->\n\n       <ion-item no-lines box-shadow margin-bottom>\n\n          <ion-input no-margin type="text" [placeholder]="data.lastName" [(ngModel)]="lastName"></ion-input>\n\n       </ion-item>\n\n       <!-- Address Line 1 -->\n\n       <ion-item no-lines box-shadow margin-bottom>\n\n          <ion-input no-margin type="text" [placeholder]="data.addressLine1" [(ngModel)]="address1"></ion-input>\n\n       </ion-item>\n\n       <!-- Address Line 2 -->\n\n       <ion-item no-lines box-shadow margin-bottom>\n\n          <ion-input no-margin type="text" [placeholder]="data.addressLine2" [(ngModel)]="address2"></ion-input>\n\n       </ion-item>\n\n       <!-- City -->\n\n       <ion-item no-lines box-shadow half-col>\n\n          <ion-input no-margin type="text" [placeholder]="data.city" [(ngModel)]="city"></ion-input>\n\n       </ion-item>\n\n       <!-- Zip Code -->\n\n       <ion-item no-lines box-shadow half-col margin-left>\n\n          <ion-input no-margin type="number" [placeholder]="data.zipCode" [(ngModel)]="zipCode"></ion-input>\n\n       </ion-item>\n\n     </ion-col>\n\n      <!-- Button Write Comment -->\n\n      <ion-col col-12 align-self-end>\n\n      <button ion-button button-clear-outline round outline block text-capitalize (click)="onEvent(\'onSubmit\', $event)">\n\n        {{data.button}}\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-3/form.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], FormLayout3);
    return FormLayout3;
}());

//# sourceMappingURL=form-layout-3.js.map

/***/ }),

/***/ 834:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormLayout4; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(347);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FormLayout4 = /** @class */ (function () {
    function FormLayout4(camera, alertCtrl) {
        this.camera = camera;
        this.alertCtrl = alertCtrl;
        this.options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            saveToPhotoAlbum: true,
        };
        this.item = {
            'description': '',
            'stars': 1,
            'imageUrl': ''
        };
    }
    FormLayout4.prototype.onAddVideoPhoto = function () {
        var _this = this;
        this.camera.getPicture(this.options).then(function (imageData) {
            _this.item.imageUrl = window.Ionic.WebView.convertFileSrc(imageData);
        }, function (err) {
            _this.displayErrorAlert(err);
        });
    };
    ;
    FormLayout4.prototype.displayErrorAlert = function (err) {
        console.log(err);
        var alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Error while trying to capture picture',
            buttons: ['OK']
        });
        alert.present();
    };
    FormLayout4.prototype.onEvent = function (event, e, index) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](this.item);
        }
    };
    FormLayout4.prototype.onStarClass = function (items, index, e) {
        this.item.stars = index;
        for (var i = 0; i < items.length; i++) {
            items[i].isActive = i <= index;
        }
        if (this.events['onRates']) {
            this.events['onRates'](index);
        }
    };
    ;
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout4.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], FormLayout4.prototype, "events", void 0);
    FormLayout4 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'form-layout-4',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-4/form.html"*/'<!--  Theme Form - Form add Photo Or Video -->\n\n  <ion-content>\n\n    <ion-grid *ngIf="data != null">\n\n      <ion-row padding>\n\n        <ion-col col-12 text-center>\n\n          <span span-small>{{data.rateTitle}}</span>\n\n        </ion-col>\n\n        <ion-col col-12 text-center>\n\n            <ion-icon\n\n              *ngFor="let item of data.iconsStars;let i = index"\n\n              color="accent"\n\n              [name]="item.icon"\n\n              [ngClass]="{\'active\':item.isActive}"\n\n              (click)="onStarClass(data.iconsStars, i, $event)">\n\n            </ion-icon>\n\n        </ion-col>\n\n        <ion-col col-12 text-center>\n\n          <span span-small>{{data.title}}</span>\n\n        </ion-col>\n\n        <ion-col col-12 textarea>\n\n          <ion-item no-lines>\n\n            <ion-textarea [placeholder]="data.descriptionPlaceholder" [(ngModel)]="item.description"></ion-textarea>\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-12>\n\n          <div *ngIf="item.imageUrl" background-size [ngStyle]="{\'background-image\': \'url(\' + item.imageUrl + \')\'}"></div>\n\n        </ion-col>\n\n        <ion-col col-12>\n\n          <button ion-button button-clear-outline round outline block text-capitalize (click)="onAddVideoPhoto()">\n\n            {{data.btnAddPhotoOrVideo}}\n\n          </button>\n\n        </ion-col>\n\n        <ion-col col-12>\n\n          <button ion-button button-clear-outline round outline block text-capitalize (click)="onEvent(\'onSubmit\', $event)">\n\n            {{data.btnSubmit}}\n\n          </button>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/forms/layout-4/form.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], FormLayout4);
    return FormLayout4;
}());

//# sourceMappingURL=form-layout-4.js.map

/***/ }),

/***/ 936:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemDetailsPageForm; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemDetailsPageForm = /** @class */ (function () {
    function ItemDetailsPageForm(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.params = {};
        // If we navigated to this page, we will have an item available as a nav param
        this.page = navParams.get('page');
        this.service = navParams.get('service');
        if (this.service) {
            this.params = this.service.prepareParams(this.page, navCtrl);
            this.params.data = this.service.load(this.page);
        }
        else {
            navCtrl.setRoot("HomePage");
        }
    }
    ItemDetailsPageForm = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-form/item-details-form.html"*/'<!--Google card components-->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>{{params.title}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<!--Content-->\n\n<!--PAGE GOOGLE CARDS - Full image cards-->\n\n<form-layout-1 has-header *ngIf="params.formLayout1" [data]="params.data | async" [events]="params.events">\n\n</form-layout-1>\n\n\n\n<!--PAGE GOOGLE CARDS - Styled cards 2-->\n\n<form-layout-2 has-header *ngIf="params.formLayout2" [data]="params.data | async" [events]="params.events">\n\n</form-layout-2>\n\n\n\n<!--PAGE GOOGLE CARDS - Styled cards-->\n\n<form-layout-3 has-header *ngIf="params.formLayout3" [data]="params.data | async" [events]="params.events">\n\n</form-layout-3>\n\n\n\n<!--PAGE GOOGLE CARDS - Styled cards-->\n\n<form-layout-4 has-header *ngIf="params.formLayout4" [data]="params.data | async" [events]="params.events">\n\n</form-layout-4>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-form/item-details-form.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ItemDetailsPageForm);
    return ItemDetailsPageForm;
}());

//# sourceMappingURL=item-details-form.js.map

/***/ })

});
//# sourceMappingURL=12.js.map