webpackJsonp([8,90,91,92,93,94],{

/***/ 686:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLayout1Module", function() { return ProfileLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_layout_1__ = __webpack_require__(887);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileLayout1Module = /** @class */ (function () {
    function ProfileLayout1Module() {
    }
    ProfileLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_1__["a" /* ProfileLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_layout_1__["a" /* ProfileLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_1__["a" /* ProfileLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ProfileLayout1Module);
    return ProfileLayout1Module;
}());

//# sourceMappingURL=profile-layout-1.module.js.map

/***/ }),

/***/ 687:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLayout2Module", function() { return ProfileLayout2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_layout_2__ = __webpack_require__(888);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileLayout2Module = /** @class */ (function () {
    function ProfileLayout2Module() {
    }
    ProfileLayout2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_2__["a" /* ProfileLayout2 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_layout_2__["a" /* ProfileLayout2 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_2__["a" /* ProfileLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ProfileLayout2Module);
    return ProfileLayout2Module;
}());

//# sourceMappingURL=profile-layout-2.module.js.map

/***/ }),

/***/ 688:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLayout3Module", function() { return ProfileLayout3Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_layout_3__ = __webpack_require__(889);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileLayout3Module = /** @class */ (function () {
    function ProfileLayout3Module() {
    }
    ProfileLayout3Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_3__["a" /* ProfileLayout3 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_layout_3__["a" /* ProfileLayout3 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_3__["a" /* ProfileLayout3 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ProfileLayout3Module);
    return ProfileLayout3Module;
}());

//# sourceMappingURL=profile-layout-3.module.js.map

/***/ }),

/***/ 689:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLayout4Module", function() { return ProfileLayout4Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_layout_4__ = __webpack_require__(890);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileLayout4Module = /** @class */ (function () {
    function ProfileLayout4Module() {
    }
    ProfileLayout4Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_4__["a" /* ProfileLayout4 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_layout_4__["a" /* ProfileLayout4 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_4__["a" /* ProfileLayout4 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ProfileLayout4Module);
    return ProfileLayout4Module;
}());

//# sourceMappingURL=profile-layout-4.module.js.map

/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLayout5Module", function() { return ProfileLayout5Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_layout_5__ = __webpack_require__(891);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileLayout5Module = /** @class */ (function () {
    function ProfileLayout5Module() {
    }
    ProfileLayout5Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_5__["a" /* ProfileLayout5 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_layout_5__["a" /* ProfileLayout5 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_5__["a" /* ProfileLayout5 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ProfileLayout5Module);
    return ProfileLayout5Module;
}());

//# sourceMappingURL=profile-layout-5.module.js.map

/***/ }),

/***/ 743:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemDetailsPageProfileModule", function() { return ItemDetailsPageProfileModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__item_details_profile__ = __webpack_require__(944);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_profile_layout_1_profile_layout_1_module__ = __webpack_require__(686);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_profile_layout_2_profile_layout_2_module__ = __webpack_require__(687);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_profile_layout_3_profile_layout_3_module__ = __webpack_require__(688);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_profile_layout_4_profile_layout_4_module__ = __webpack_require__(689);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_profile_layout_5_profile_layout_5_module__ = __webpack_require__(690);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var ItemDetailsPageProfileModule = /** @class */ (function () {
    function ItemDetailsPageProfileModule() {
    }
    ItemDetailsPageProfileModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__item_details_profile__["a" /* ItemDetailsPageProfile */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__item_details_profile__["a" /* ItemDetailsPageProfile */]),
                __WEBPACK_IMPORTED_MODULE_3__components_profile_layout_1_profile_layout_1_module__["ProfileLayout1Module"], __WEBPACK_IMPORTED_MODULE_4__components_profile_layout_2_profile_layout_2_module__["ProfileLayout2Module"], __WEBPACK_IMPORTED_MODULE_5__components_profile_layout_3_profile_layout_3_module__["ProfileLayout3Module"],
                __WEBPACK_IMPORTED_MODULE_6__components_profile_layout_4_profile_layout_4_module__["ProfileLayout4Module"], __WEBPACK_IMPORTED_MODULE_7__components_profile_layout_5_profile_layout_5_module__["ProfileLayout5Module"]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ItemDetailsPageProfileModule);
    return ItemDetailsPageProfileModule;
}());

//# sourceMappingURL=item-details-profile.module.js.map

/***/ }),

/***/ 887:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileLayout1 = /** @class */ (function () {
    function ProfileLayout1() {
        this.slider = {};
    }
    ProfileLayout1.prototype.slideHasChanged = function (slider, index) {
        this.slider[index] = slider;
        if (2 == slider._activeIndex) {
            if (this.data.items) {
                this.data.items.splice(index, 1);
            }
            else {
                this.data.splice(index, 1);
            }
        }
    };
    ProfileLayout1.prototype.onClickEvent = function (index) {
        if (this.slider[index]) {
            this.slider[index].slidePrev(300);
        }
    };
    ProfileLayout1.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout1.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ProfileLayout1.prototype, "content", void 0);
    ProfileLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-1',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-1/profile.html"*/'<!--Theme Profile - Profie With Avatar-->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-col col-12>\n\n      <ion-card padding box-shadow>\n\n        <ion-avatar>\n\n          <img [src]="data.image" alt="">\n\n        </ion-avatar>\n\n        <ion-card-content text-center>\n\n          <h1>{{data.title}}</h1>\n\n          <p>{{data.subtitle}}</p>\n\n        </ion-card-content>\n\n      </ion-card>\n\n    </ion-col>\n\n    <span text-center font-bold ion-text color="accent" text-uppercase margin-top>{{data.category}}</span>\n\n    <ion-col col-12 text-center *ngFor="let item of data.items;let i = index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n      <ion-card padding box-shadow>\n\n        <ion-card-content text-center>\n\n          <span font-bold ion-text text-uppercase color="accent">{{item.category}}</span>\n\n          <h1 card-title margin-top text-wrap>{{item.title}}</h1>\n\n        </ion-card-content>\n\n        <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onLike\', item, $event)">\n\n          <ion-icon [ngClass]="{\'active\' : item.like.isActive}" [name]="item.like.icon"></ion-icon>\n\n           {{item.like.text}}\n\n        </button>\n\n        <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onComment\', item, $event)">\n\n          <ion-icon [ngClass]="{\'active\' : item.comment.isActive}" [name]="item.comment.icon"></ion-icon>\n\n          {{item.comment.number}} {{item.comment.text}}\n\n        </button>\n\n      </ion-card>\n\n    </ion-col>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-1/profile.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProfileLayout1);
    return ProfileLayout1;
}());

//# sourceMappingURL=profile-layout-1.js.map

/***/ }),

/***/ 888:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileLayout2 = /** @class */ (function () {
    function ProfileLayout2() {
    }
    ProfileLayout2.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout2.prototype, "events", void 0);
    ProfileLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-2',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-2/profile.html"*/'<!--Profile- Profile With Slider + Comments-->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-row>\n\n      <ion-col col-12 align-self-start>\n\n        <ion-card profile padding text-center box-shadow>\n\n          <ion-avatar>\n\n            <img [src]="data.image" alt="">\n\n          </ion-avatar>\n\n          <ion-card-content text-center>\n\n            <h1 ion-text color="primary">{{data.title}}</h1>\n\n            <p ion-text color="primary">{{data.subtitle}}</p>\n\n          </ion-card-content>\n\n          <ion-row no-padding>\n\n            <ion-col>\n\n              <button ion-button button-clear clear no-padding>\n\n                <span small-font font-bold ion-text color="accent">{{data.valueFollowers}}</span>\n\n                <span text-capitalize ion-text color="accent">{{data.followers}}</span>\n\n              </button>\n\n            </ion-col>\n\n            <ion-col>\n\n              <button ion-button button-clear clear no-padding>\n\n                <span small-font font-bold ion-text color="accent">{{data.valueFollowing}}</span>\n\n                <span text-capitalize ion-text color="accent">{{data.following}}</span>\n\n              </button>\n\n            </ion-col>\n\n            <ion-col>\n\n              <button ion-button button-clear clear no-padding>\n\n                <span small-font font-bold ion-text color="accent">{{data.valuePosts}}</span>\n\n                <span ion-text text-capitalize color="accent">{{data.posts}}</span>\n\n              </button>\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n      <!-- Slider-->\n\n      <ion-col col-12 align-self-start text-center>\n\n        <span font-bold ion-text text-uppercase margin-top color="accent">{{data.category}}</span>\n\n        <ion-slides pager="true">\n\n          <ion-slide text-center *ngFor="let item of data.items;let i = index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <ion-card padding box-shadow>\n\n              <span font-bold ion-text text-uppercase color="accent">{{item.category}}</span>\n\n              <h1 card-title margin-top text-wrap>{{item.title}}</h1>\n\n              <button padding-top ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onLike\', item, $event)">\n\n                <ion-icon [ngClass]="{\'active\' : item.like.isActive}" [name]="item.like.icon"></ion-icon>\n\n                {{item.like.text}}\n\n              </button>\n\n              <button padding-top ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onComment\', item, $event)">\n\n                <ion-icon [ngClass]="{\'active\' : item.comment.isActive}" [name]="item.comment.icon"></ion-icon>\n\n                {{item.comment.number}} {{item.comment.text}}\n\n              </button>\n\n            </ion-card>\n\n          </ion-slide>\n\n        </ion-slides>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-2/profile.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProfileLayout2);
    return ProfileLayout2;
}());

//# sourceMappingURL=profile-layout-2.js.map

/***/ }),

/***/ 889:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout3; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileLayout3 = /** @class */ (function () {
    function ProfileLayout3() {
        this.slider = {};
    }
    ProfileLayout3.prototype.slideHasChanged = function (slider, index) {
        this.slider[index] = slider;
        if (2 == slider._activeIndex) {
            if (this.data.items) {
                this.data.items.splice(index, 1);
            }
            else {
                this.data.splice(index, 1);
            }
        }
    };
    ProfileLayout3.prototype.onClickEvent = function (index) {
        if (this.slider[index]) {
            this.slider[index].slidePrev(300);
        }
    };
    ProfileLayout3.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout3.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout3.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ProfileLayout3.prototype, "content", void 0);
    ProfileLayout3 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-3',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-3/profile.html"*/'<!--Profile- Profile 1-->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-row>\n\n    <ion-col col-12>\n\n      <ion-card profile text-center box-shadow>\n\n        <ion-item transparent>\n\n          <ion-avatar item-start>\n\n            <img [src]="data.image" alt="">\n\n          </ion-avatar>\n\n          <h1 ion-text color="primary">{{data.title}}</h1>\n\n          <p text-wrap ion-text color="primary">{{data.subtitle}}</p>\n\n        </ion-item>\n\n        <ion-row no-padding>\n\n          <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valueFollowers}}</span>\n\n              <span ion-text text-capitalize color="accent">{{data.followers}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valueFollowing}}</span>\n\n              <span ion-text text-capitalize color="accent">{{data.following}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valuePosts}}</span>\n\n              <span ion-text text-capitalize color="accent">{{data.posts}}</span>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n    <!-- <span text-center text-uppercase margin-top small-font font-bold ion-text color="accent">{{data.category}}</span> -->\n\n    <ion-col col-12 col-md-6 col-lg-4 text-center *ngFor="let item of data.items;let i = index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n      <ion-card padding box-shadow>\n\n        <img [src]="item.backgroundCard" alt="">\n\n        <ion-card-content text-center>\n\n        <span text-uppercase font-bold ion-text color="accent">{{item.category}}</span>\n\n        <h1 card-title margin-top text-wrap>{{item.title}}</h1>\n\n        </ion-card-content>\n\n        <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onLike\', item, $event)">\n\n          <ion-icon [ngClass]="{\'active\' : item.like.isActive}" [name]="item.like.icon"></ion-icon>\n\n          {{item.like.text}}\n\n        </button>\n\n        <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onComment\', item, $event)">\n\n          <ion-icon [ngClass]="{\'active\' : item.comment.isActive}" [name]="item.comment.icon"></ion-icon>\n\n          {{item.comment.number}} {{item.comment.text}}\n\n        </button>\n\n      </ion-card>\n\n    </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-3/profile.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProfileLayout3);
    return ProfileLayout3;
}());

//# sourceMappingURL=profile-layout-3.js.map

/***/ }),

/***/ 890:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout4; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProfileLayout4 = /** @class */ (function () {
    function ProfileLayout4() {
    }
    ProfileLayout4.prototype.onEvent = function (event, item, e) {
        if (e) {
            e.stopPropagation();
        }
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout4.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout4.prototype, "events", void 0);
    ProfileLayout4 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-4',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-4/profile.html"*/'<!--Theme Profile- Profile With Slider-->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-col col-12 text-center>\n\n      <ion-card profile padding text-center box-shadow>\n\n        <ion-avatar>\n\n          <img [src]="data.image" alt="">\n\n        </ion-avatar>\n\n        <ion-card-content text-center>\n\n          <h1 ion-text color="primary">{{data.title}}</h1>\n\n          <p ion-text color="primary">{{data.subtitle}}</p>\n\n        </ion-card-content>\n\n        <ion-row no-padding>\n\n          <ion-col no-padding>\n\n            <button ion-button button-clear clear no-padding>\n\n              <h2 small-font font-bold ion-text color="primary">{{data.valueFollowers}}</h2>\n\n              <span ion-text text-capitalize color="primary">{{data.followers}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="primary">{{data.valueFollowing}}</span>\n\n              <span ion-text text-capitalize color="primary">{{data.following}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col no-padding>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="primary">{{data.valuePosts}}</span>\n\n              <span ion-text text-capitalize color="primary">{{data.posts}}</span>\n\n            </button>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-card>\n\n    </ion-col>\n\n    <!-- Slider -->\n\n    <ion-col col-12 text-center padding-top>\n\n      <span category ion-text font-bold padding-top text-uppercase color="accent">{{data.category}}</span>\n\n      <ion-slides pager="true">\n\n        <ion-slide *ngFor="let item of data.items;let i = index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n          <ion-col col-12 text-center>\n\n            <ion-card padding box-shadow>\n\n              <img [src]="item.backgroundCard">\n\n              <ion-card-content text-center>\n\n                <span text-uppercase font-bold ion-text color="accent">{{item.category}}</span>\n\n                <h1 card-title margin-top text-wrap>{{item.title}}</h1>\n\n              </ion-card-content>\n\n              <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onLike\', item, $event)">\n\n                <ion-icon [ngClass]="{\'active\' : item.like.isActive}" [name]="item.like.icon"></ion-icon>\n\n               {{item.like.text}}\n\n              </button>\n\n              <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onComment\', item, $event)">\n\n                <ion-icon [ngClass]="{\'active\' : item.comment.isActive}" [name]="item.comment.icon"></ion-icon>\n\n                {{item.comment.number}} {{item.comment.text}}\n\n              </button>\n\n            </ion-card>\n\n          </ion-col>\n\n        </ion-slide>\n\n      </ion-slides>\n\n    </ion-col>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-4/profile.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProfileLayout4);
    return ProfileLayout4;
}());

//# sourceMappingURL=profile-layout-4.js.map

/***/ }),

/***/ 891:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout5; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dal_dal__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_fingerprint_aio_ngx__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settersandgetters_settersandgetters__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utility_utility__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_android_fingerprint_auth__ = __webpack_require__(349);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfileLayout5 = /** @class */ (function () {
    function ProfileLayout5(androidFingerprintAuth, alertCtrl, dal, navCtrl, navParams, utility, faio, setAndGet) {
        this.androidFingerprintAuth = androidFingerprintAuth;
        this.alertCtrl = alertCtrl;
        this.dal = dal;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.utility = utility;
        this.faio = faio;
        this.setAndGet = setAndGet;
        this.slider = {};
        this.otpValReq = {
            otp: "",
            userId: "",
            mobile: "",
            email: ""
        };
    }
    ProfileLayout5.prototype.ngOnInit = function () {
        // this.getCustomerList();
        this.getBackupData();
        this.txt = "Please enter your OTP";
    };
    ProfileLayout5.prototype.otpPrompt = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.txt,
            message: 'Enter OTP sent to your registered mobile or email !!',
            inputs: [
                {
                    name: 'otp',
                    placeholder: 'Enter OTP here'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: 'validate',
                    handler: function (data) {
                        _this.otpValReq.otp = data.otp;
                        console.log("otp prompt>>>>", data);
                        _this.otpAuth();
                    }
                }
            ]
        });
        alert.present();
    };
    ProfileLayout5.prototype.getBackupData = function () {
        var _this = this;
        this.dal.getCustomer({ name: this.dal.getUserId() }).subscribe(function (x) {
            _this.customerBackup = x[0];
            console.log("backup data>>>>", _this.customerBackup);
            _this.otpValReq.userId = _this.customerBackup.name;
            _this.otpValReq.mobile = _this.customerBackup.mobile;
            _this.custLocation = _this.customerBackup.location;
            _this.otpValReq.email = _this.customerBackup.email;
        });
    };
    ProfileLayout5.prototype.getCustomerList = function () {
        var _this = this;
        this.dal.getCustomer({ name: this.dal.getUserId() }).subscribe(function (y) {
            _this.customers = y[0];
            console.log("customer list >>>>>", _this.customers);
        });
    };
    ProfileLayout5.prototype.getOtp = function () {
        var _this = this;
        this.dal.requestOtp(this.otpValReq).subscribe(function (z) {
            // this.utility.presentAlert("otp received to your registered mobile and email");
            console.log("otp received>>>>>>>>", z);
            _this.otpPrompt();
        });
    };
    ProfileLayout5.prototype.otpAuth = function () {
        var _this = this;
        this.dal.validateOtp(this.otpValReq).subscribe(function (p) {
            if (p.success == true) {
                _this.getCustomerList();
                // console.log("after true OTP>>>>>>>>>>", this.customers);
                _this.utility.presentAlert("OTP validated and Data is Retrieved successfully !!");
            }
            else {
                _this.txt = "OTP Failed to validate !! please try again..";
                _this.otpPrompt();
                // this.otpAuth()
                return;
            }
        });
    };
    ProfileLayout5.prototype.bioAuth = function () {
        var _this = this;
        console.log("faio says>>>>>>>", this.androidFingerprintAuth);
        this.androidFingerprintAuth.isAvailable()
            .then(function (result) {
            if (result.isAvailable) {
                // it is available
                _this.androidFingerprintAuth.encrypt({ clientId: _this.otpValReq.userId, username: _this.otpValReq.userId, password: _this.otpValReq.userId })
                    .then(function (result) {
                    if (result.withFingerprint) {
                        // this.utility.presentAlert("Successfully Retrived Data.")
                        // this.utility.presentAlert(result);
                        _this.getOtp();
                        console.log('Successfully encrypted credentials.');
                        console.log('Encrypted credentials: ' + result.token);
                    }
                    else
                        _this.utility.presentAlert("Didn\'t authenticate!");
                })
                    .catch(function (error) {
                    if (error === _this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
                        console.log('Fingerprint authentication cancelled');
                        _this.utility.presentAlert("Fingerprint authentication cancelled");
                    }
                    else
                        console.error(error);
                });
            }
            else {
                // fingerprint auth isn't available
            }
        })
            .catch(function (error) {
            console.error(error);
            _this.utility.presentAlert(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout5.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout5.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ProfileLayout5.prototype, "content", void 0);
    ProfileLayout5 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-5',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-5/profile.html"*/'<!--Profile- Profile 1-->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-card transparent text-center>\n\n          <ion-item box-shadow transparent text-center *ngIf="data != null" background-size\n\n            [ngStyle]="{\'background-image\': \'url(\' + data.headerImage + \')\'}">\n\n            <ion-avatar>\n\n              <img [src]="data.image" alt="">\n\n            </ion-avatar>\n\n            <h1 item-title>{{otpValReq.userId}}</h1>\n\n            <p text-wrap item-title>{{custLocation}}</p>\n\n          </ion-item>\n\n          <ion-row no-padding margin-top>\n\n            <!-- <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valueFollowers}}</span>\n\n              <span ion-text text-caprasad@iamrisingstar:~/data/projects/be-project/e-insurance-app$ pitalize color="accent">{{data.followers}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valueFollowing}}</span>\n\n              <span ion-text text-capitalize color="accent">{{data.following}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valuePosts}}</span>\n\n              <span ion-text text-capitalize color="accent">{{data.posts}}</span>\n\n            </button>\n\n          </ion-col> -->\n\n            <!-- Social Button-->\n\n            <ion-col col-12 text-center>\n\n              <button ion-button button-icon icon-only button-icon (click)="bioAuth()">\n\n                <ion-icon icon-small name="md-archive"></ion-icon>\n\n              </button>\n\n\n\n              <!--<button ion-button icon-only button-icon (click)="onEvent(\'onTwitter\', item, $event)">\n\n                <ion-icon icon-small name="{{data.iconTwitter}}"></ion-icon>\n\n              </button> -->\n\n\n\n              <!-- <button ion-button icon-only button-icon (click)="onEvent(\'onInstagram\', item, $event)">\n\n                <ion-icon icon-small name="{{data.iconInstagram}}"></ion-icon>\n\n              </button> -->\n\n\n\n            </ion-col>\n\n            <!-- Info Author-->\n\n            <div *ngIf="customers != null">\n\n              <ion-col col-12 text-left margin-top box-shadow>\n\n                <ion-item transparent>\n\n                  <h1 subitem-title ion-text>Private Data</h1>\n\n                  <p subitem-subtitle padding-top text-wrap>This data is secured with biometric and otp !!</p>\n\n\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Policy No :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.policyNo}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Sum Assurred :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.sumAssured}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Plan :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.plan}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Policy Term :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.term}} years</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Mobile No. :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.mobile}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Issued By :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.location}} Division</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Purpose :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.purpose}}</p>\n\n                </ion-item>\n\n\n\n              </ion-col>\n\n            </div>\n\n\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-5/profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_native_android_fingerprint_auth__["a" /* AndroidFingerprintAuth */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_dal_dal__["a" /* DalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__providers_utility_utility__["a" /* UtilityProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_fingerprint_aio_ngx__["a" /* FingerprintAIO */], __WEBPACK_IMPORTED_MODULE_4__providers_settersandgetters_settersandgetters__["a" /* SettersandgettersProvider */]])
    ], ProfileLayout5);
    return ProfileLayout5;
}());

//# sourceMappingURL=profile-layout-5.js.map

/***/ }),

/***/ 944:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemDetailsPageProfile; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemDetailsPageProfile = /** @class */ (function () {
    function ItemDetailsPageProfile(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.params = {};
        // If we navigated to this page, we will have an item available as a nav param
        this.page = navParams.get('page');
        this.service = navParams.get('service');
        if (this.service) {
            this.params = this.service.prepareParams(this.page, navCtrl);
            this.params.data = this.service.load(this.page);
        }
        else {
            navCtrl.setRoot("HomePage");
        }
    }
    ItemDetailsPageProfile = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-profile/item-details-profile.html"*/'<!--Google card components-->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n        <ion-title>{{params.title}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<!--Content-->\n\n\n\n<!--PAGE GOOGLE CARDS - Full image cards-->\n\n<profile-layout-1 has-header *ngIf="params.profileLayout1" [data]="params.data | async" [events]="params.events">\n\n</profile-layout-1>\n\n\n\n<!--PAGE GOOGLE CARDS - Styled cards 2-->\n\n<profile-layout-2 has-header *ngIf="params.profileLayout2" [data]="params.data | async" [events]="params.events">\n\n</profile-layout-2>\n\n\n\n<!--PAGE GOOGLE CARDS - Styled cards-->\n\n<profile-layout-3 has-header *ngIf="params.profileLayout3" [data]="params.data | async" [events]="params.events">\n\n</profile-layout-3>\n\n\n\n<!--PAGE GOOGLE CARDS - Styled cards-->\n\n<profile-layout-4 has-header *ngIf="params.profileLayout4" [data]="params.data | async" [events]="params.events">\n\n</profile-layout-4>\n\n\n\n<!--PAGE GOOGLE CARDS - Styled cards-->\n\n<profile-layout-5 has-header *ngIf="params.profileLayout5" [data]="params.data | async" [events]="params.events">\n\n</profile-layout-5>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-profile/item-details-profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ItemDetailsPageProfile);
    return ItemDetailsPageProfile;
}());

//# sourceMappingURL=item-details-profile.js.map

/***/ })

});
//# sourceMappingURL=8.js.map