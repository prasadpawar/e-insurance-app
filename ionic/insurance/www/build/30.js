webpackJsonp([30,119,120],{

/***/ 651:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentLayout1Module", function() { return CommentLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comment_layout_1__ = __webpack_require__(829);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CommentLayout1Module = /** @class */ (function () {
    function CommentLayout1Module() {
    }
    CommentLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__comment_layout_1__["a" /* CommentLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__comment_layout_1__["a" /* CommentLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__comment_layout_1__["a" /* CommentLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], CommentLayout1Module);
    return CommentLayout1Module;
}());

//# sourceMappingURL=comment-layout-1.module.js.map

/***/ }),

/***/ 652:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommentLayout2Module", function() { return CommentLayout2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comment_layout_2__ = __webpack_require__(830);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CommentLayout2Module = /** @class */ (function () {
    function CommentLayout2Module() {
    }
    CommentLayout2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__comment_layout_2__["a" /* CommentLayout2 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__comment_layout_2__["a" /* CommentLayout2 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__comment_layout_2__["a" /* CommentLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], CommentLayout2Module);
    return CommentLayout2Module;
}());

//# sourceMappingURL=comment-layout-2.module.js.map

/***/ }),

/***/ 732:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemDetailsPageCommentModule", function() { return ItemDetailsPageCommentModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__item_details_comment__ = __webpack_require__(933);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_comment_layout_1_comment_layout_1_module__ = __webpack_require__(651);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_comment_layout_2_comment_layout_2_module__ = __webpack_require__(652);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ItemDetailsPageCommentModule = /** @class */ (function () {
    function ItemDetailsPageCommentModule() {
    }
    ItemDetailsPageCommentModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__item_details_comment__["a" /* ItemDetailsPageComment */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__item_details_comment__["a" /* ItemDetailsPageComment */]),
                __WEBPACK_IMPORTED_MODULE_3__components_comment_layout_1_comment_layout_1_module__["CommentLayout1Module"], __WEBPACK_IMPORTED_MODULE_4__components_comment_layout_2_comment_layout_2_module__["CommentLayout2Module"]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__item_details_comment__["a" /* ItemDetailsPageComment */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ItemDetailsPageCommentModule);
    return ItemDetailsPageCommentModule;
}());

//# sourceMappingURL=item-details-comment.module.js.map

/***/ }),

/***/ 829:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CommentLayout1 = /** @class */ (function () {
    function CommentLayout1() {
    }
    CommentLayout1.prototype.onEvent = function (event, item, e) {
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], CommentLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], CommentLayout1.prototype, "events", void 0);
    CommentLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'comment-layout-1',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/comment/layout-1/comment.html"*/'<!--Theme Comment - Comments Basic -->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle *ngIf="data != null">\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <div *ngIf="data != null">\n\n            <ion-title>{{data.headerTitle}}</ion-title>\n\n        </div>\n\n    </ion-navbar>\n\n</ion-header>\n\n<!-- Content -->\n\n<ion-content>\n\n    <ion-grid *ngIf="data != null">\n\n      <ion-col col-12 padding-top>\n\n        <span font-bold span-small padding-left>{{data.allComments}}</span>\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <ion-list>\n\n          <ion-item box-shadow margin-top no-lines *ngFor="let item of data.items; let i = index;" (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <!-- Avatar -->\n\n            <ion-avatar item-start>\n\n              <img [src]="item.image">\n\n            </ion-avatar>\n\n            <!-- Title -->\n\n            <h2 item-title>{{item.title}}</h2>\n\n            <p>{{item.time}}</p>\n\n            <!-- Description -->\n\n            <h3 text-wrap margin-top>\n\n              {{item.description}}\n\n            </h3>\n\n          </ion-item>\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/comment/layout-1/comment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], CommentLayout1);
    return CommentLayout1;
}());

//# sourceMappingURL=comment-layout-1.js.map

/***/ }),

/***/ 830:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CommentLayout2 = /** @class */ (function () {
    function CommentLayout2() {
    }
    CommentLayout2.prototype.onEvent = function (event, item, e) {
        if (this.events[event]) {
            this.events[event](item);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], CommentLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], CommentLayout2.prototype, "events", void 0);
    CommentLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'comment-layout-2',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/comment/layout-2/comment.html"*/'<!--Theme Comment - Comments With Timeline -->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle *ngIf="data != null">\n\n          <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n        <div *ngIf="data != null">\n\n            <ion-title>{{data.headerTitle}}</ion-title>\n\n        </div>\n\n    </ion-navbar>\n\n</ion-header>\n\n<!-- Content -->\n\n<ion-content>\n\n    <ion-grid *ngIf="data != null">\n\n      <ion-col col-12 padding-top>\n\n        <span font-bold span-small padding-left>{{data.allComments}}</span>\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <ion-list>\n\n          <ion-item margin-top transparent no-lines *ngFor="let item of data.items; let i = index;" (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <!-- Avatar -->\n\n            <ion-avatar item-start>\n\n              <img [src]="item.image">\n\n            </ion-avatar>\n\n            <!-- Title -->\n\n            <h2 item-title>{{item.title}}</h2>\n\n            <p>{{item.time}}</p>\n\n            <!-- Description -->\n\n            <h3 text-wrap margin-top>\n\n              {{item.description}}\n\n            </h3>\n\n          </ion-item>\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/comment/layout-2/comment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], CommentLayout2);
    return CommentLayout2;
}());

//# sourceMappingURL=comment-layout-2.js.map

/***/ }),

/***/ 933:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemDetailsPageComment; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemDetailsPageComment = /** @class */ (function () {
    function ItemDetailsPageComment(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.params = {};
        // If we navigated to this page, we will have an item available as a nav param
        this.page = navParams.get('page');
        this.service = navParams.get('service');
        if (this.service) {
            this.params = this.service.prepareParams(this.page, navCtrl);
            this.params.data = this.service.load(this.page);
        }
        else {
            navCtrl.setRoot("HomePage");
        }
    }
    ItemDetailsPageComment = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-comment/item-details-comment.html"*/'<!-- Content -->\n\n<comment-layout-1 *ngIf="params.commentLayout1" [data]="params.data | async" [events]="params.events">\n\n</comment-layout-1>\n\n\n\n<comment-layout-2 *ngIf="params.commentLayout2" [data]="params.data | async" [events]="params.events">\n\n</comment-layout-2>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-comment/item-details-comment.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ItemDetailsPageComment);
    return ItemDetailsPageComment;
}());

//# sourceMappingURL=item-details-comment.js.map

/***/ })

});
//# sourceMappingURL=30.js.map