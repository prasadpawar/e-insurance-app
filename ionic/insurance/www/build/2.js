webpackJsonp([2],{

/***/ 761:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsPageModule", function() { return ItemsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__items__ = __webpack_require__(962);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ItemsPageModule = /** @class */ (function () {
    function ItemsPageModule() {
    }
    ItemsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__items__["a" /* ItemsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__items__["a" /* ItemsPage */]),
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ItemsPageModule);
    return ItemsPageModule;
}());

//# sourceMappingURL=items.module.js.map

/***/ }),

/***/ 773:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_home_service__ = __webpack_require__(774);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_app_settings__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.service = service;
        this.data = {};
        this.isBuyButtonEnabled = false;
        service.load().subscribe(function (snapshot) {
            _this.data = snapshot;
        });
        this.isBuyButtonEnabled = __WEBPACK_IMPORTED_MODULE_3__services_app_settings__["a" /* AppSettings */].BUY_BUTTON;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/home/home.html"*/'<!--Fist Screen-->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon class="icon-menu" name="menu"></ion-icon>\n\n        </button>\n\n        <div buy *ngIf="isBuyButtonEnabled">\n\n            <ion-title>{{data.toolbarTitle}}</ion-title>\n\n            <button ion-button defoult-button\n\n                onclick="window.open(\'https://codecanyon.net/item/ionic-3-ui-themetemplate-app-material-design-blue-light/20234423?ref=CreativeForm\')">\n\n                BUY NOW\n\n            </button>\n\n        </div>\n\n        <!---Title-->\n\n        <ion-title *ngIf="!isBuyButtonEnabled">{{data.toolbarTitle}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<!--Fist Screen Content-->\n\n<ion-content>\n\n    <div *ngIf="data.background!=null" background-size default-background\n\n        [ngStyle]="{\'background-image\': \'url(\' + data.background + \')\'}">\n\n        <div class="ionic-description" text-center>\n\n            <h2 item-title>{{data.title}}</h2>\n\n            <h2 item-title>{{data.subtitle}}</h2>\n\n            <p item-subtitle>{{data.subtitle2}}</p>\n\n        </div>\n\n        <!-- <a [href]="data.link">\n\n            {{data.description}}\n\n        </a> -->\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_home_service__["a" /* HomeService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_home_service__["a" /* HomeService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomeService = /** @class */ (function () {
    function HomeService(af) {
        this.af = af;
        // Set data for - HOME PAGE
        this.getData = function () {
            return {
                "toolbarTitle": "Welcome to E-Insurance App",
                "title": "Online Insurance App",
                "subtitle": "For Mobile",
                "subtitle2": "Insure And Be Secure !!",
                "background": "https://res.cloudinary.com/brainethic/image/upload/v1546862799/WhatsApp_Image_2019-01-07_at_5.33.29_PM_fouj6p.jpg"
            };
        };
    }
    HomeService.prototype.load = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('home')
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                observer.next(_this.getData());
                observer.complete();
            });
        }
    };
    HomeService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], HomeService);
    return HomeService;
}());

//# sourceMappingURL=home-service.js.map

/***/ }),

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsService = /** @class */ (function () {
    function TabsService(af, loadingService) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.getId = function () { return 'tabs'; };
        this.getTitle = function () { return 'Tabs'; };
        this.getAllThemes = function () {
            return [
                { "title": "Footer tab - text", "theme": "layout1" },
                { "title": "Footer tab - icons", "theme": "layout2" },
                { "title": "Header tab - text", "theme": "layout3" }
            ];
        };
        this.getEventsForTheme = function (menuItem) {
            return {};
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.charAt(0).toUpperCase() +
                menuItem.slice(1)]();
        };
        // Set Data For Tabs - PAGE 1
        this.getDataForTab1 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/1.jpg",
                "description": "Sed satis est ut probe exercuisse. Experiri problems est ut minae ultra solitum, sed re vera non excusat, quia in illis agit pessimus modo fieri potest.",
                "title": "Census Introduction"
            };
        };
        // Set Data For Tabs - PAGE 2
        this.getDataForTab2 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/2.jpg",
                "description": "In secunda parte, constitue 'est maxime alumni. Qui non scit, cum non communicent cum homine lingua Latina igitur difficultatem se sentire. Et ante facere damnationem mens eorum: tunc loquetur. Haec est secunda pars dicere cum fiducia et sine ope facere periculum faciendi animo sententia.",
                "title": "Majoribus tuis Census"
            };
        };
        // Set Data For Tabs - PAGE 3
        this.getDataForTab3 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/3.jpg",
                "description": "Tertia Vade est provectae doctrinam. Si primo didicit duas partes, et tertiam partem hæc faciam vobis: scientes quod de Anglis. ",
                "title": "Vos may reperio scissa est impar, aut damnum"
            };
        };
        // Set Data For Tabs - PAGE 4
        this.getDataForTab4 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/4.jpg",
                "description": "Tibi poterit, de qua fiducia et assistent coadunationibus a-membra loqui linguis difficile. Cras interdum sollicitudin ante facile legitur vasa quibus difficile verba.",
                "title": "Ordo est ut media Teresiae"
            };
        };
        // Set Data For Tabs - PAGE 5
        this.getDataForTab5 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/5.jpg",
                "description": "In finem, non possumus dicere non debemus quod ab Anglis liber PDF Guru ad alumni. Vos can adepto a liberum web oratio infra per exemplum a visitare. Omnes tres partes in unum zip lima.",
                "title": "Procurator sine dixeritis visionem"
            };
        };
        // Set Data For Tabs - PAGE 6
        this.getDataForTab6 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/6.jpg",
                "description": "Ut nos omnes scitis quod in Latina lingua est maxime momenti in hoc mundo. Et dicunt ei possumus est internationalis lingua communicationis. Iusta putant vel si patriam loci in quo nunc es, ad iter ad loci linguam intelligere non posset, tum in aliis linguis Latina non solum ut auxilium vobis. Ita magna est nimis discere et intelligunt linguae.",
                "title": "Carpe occasiones Brexit"
            };
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
    }
    TabsService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('tab/' + item)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ;
    TabsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__loading_service__["a" /* LoadingService */]])
    ], TabsService);
    return TabsService;
}());

//# sourceMappingURL=tabs-service.js.map

/***/ }),

/***/ 962:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_HttpService__ = __webpack_require__(963);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_validation__ = __webpack_require__(964);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_mail_chimp_service__ = __webpack_require__(965);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_check_box_service__ = __webpack_require__(966);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_text_view_service__ = __webpack_require__(967);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_spinner_service__ = __webpack_require__(968);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_splash_screen_service__ = __webpack_require__(969);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_search_bar_service__ = __webpack_require__(970);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_wizard_service__ = __webpack_require__(971);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_tabs_service__ = __webpack_require__(785);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__services_login_service__ = __webpack_require__(972);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_register_service__ = __webpack_require__(973);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_list_view_service__ = __webpack_require__(974);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_list_view_expandable_service__ = __webpack_require__(975);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_list_view_drag_and_drop_service__ = __webpack_require__(976);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_list_view_swipe_to_dismiss_service__ = __webpack_require__(977);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__services_list_view_appearance_animation_service__ = __webpack_require__(978);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__services_list_view_google_card_service__ = __webpack_require__(979);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__services_parallax_service__ = __webpack_require__(980);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__services_image_gallery_service__ = __webpack_require__(981);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__services_maps_service__ = __webpack_require__(982);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__services_qrcode_service__ = __webpack_require__(983);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__services_radio_button_service__ = __webpack_require__(984);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__services_range_service__ = __webpack_require__(985);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__services_toggle_service__ = __webpack_require__(986);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__services_select_service__ = __webpack_require__(987);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__services_action_sheet_service__ = __webpack_require__(988);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__services_time_line_service__ = __webpack_require__(989);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__services_form_service__ = __webpack_require__(990);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__services_comment_service__ = __webpack_require__(991);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__services_profile_service__ = __webpack_require__(992);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__services_payment_service__ = __webpack_require__(993);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__services_segment_service__ = __webpack_require__(994);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__services_alert_service__ = __webpack_require__(995);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__home_home__ = __webpack_require__(773);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







































var ItemsPage = /** @class */ (function () {
    // services: array
    function ItemsPage(navCtrl, textViewService, spinnerService, imageGalleryService, searchBarService, checkBoxService, parallaxService, wizardService, tabsService, listViewService, listViewExpandableService, listViewDragAndDropService, listViewSwipeToDismissService, listViewAppearanceAnimationService, listViewGoogleCardsService, loginService, registerService, splashScreenService, httpService, validationService, mailChimpService, mapsService, qRCodeService, radioButtonService, rangeService, toggleService, selectService, actionSheetService, timeLineService, formService, commentService, alertCtrl, profileService, segmentService, paymentService, alertService, navParams) {
        this.navCtrl = navCtrl;
        this.textViewService = textViewService;
        this.spinnerService = spinnerService;
        this.imageGalleryService = imageGalleryService;
        this.searchBarService = searchBarService;
        this.checkBoxService = checkBoxService;
        this.parallaxService = parallaxService;
        this.wizardService = wizardService;
        this.tabsService = tabsService;
        this.listViewService = listViewService;
        this.listViewExpandableService = listViewExpandableService;
        this.listViewDragAndDropService = listViewDragAndDropService;
        this.listViewSwipeToDismissService = listViewSwipeToDismissService;
        this.listViewAppearanceAnimationService = listViewAppearanceAnimationService;
        this.listViewGoogleCardsService = listViewGoogleCardsService;
        this.loginService = loginService;
        this.registerService = registerService;
        this.splashScreenService = splashScreenService;
        this.httpService = httpService;
        this.validationService = validationService;
        this.mailChimpService = mailChimpService;
        this.mapsService = mapsService;
        this.qRCodeService = qRCodeService;
        this.radioButtonService = radioButtonService;
        this.rangeService = rangeService;
        this.toggleService = toggleService;
        this.selectService = selectService;
        this.actionSheetService = actionSheetService;
        this.timeLineService = timeLineService;
        this.formService = formService;
        this.commentService = commentService;
        this.alertCtrl = alertCtrl;
        this.profileService = profileService;
        this.segmentService = segmentService;
        this.paymentService = paymentService;
        this.alertService = alertService;
        this.isBuyButtonEnabled = false;
        this.pages = {};
        this.listServices = {};
        this.listServices = {
            'checkBoxes': this.checkBoxService,
            'login': this.loginService,
            'register': this.registerService,
            'imageGallery': this.imageGalleryService,
            'textViews': this.textViewService,
            'spinner': this.spinnerService,
            'parallax': this.parallaxService,
            'wizard': this.wizardService,
            'searchBars': this.searchBarService,
            'tabs': this.tabsService,
            'listViews': this.listViewService,
            'expandable': this.listViewExpandableService,
            'dragAndDrop': this.listViewDragAndDropService,
            'swipeToDismiss': this.listViewSwipeToDismissService,
            'appearanceAnimation': this.listViewAppearanceAnimationService,
            'googleCards': this.listViewGoogleCardsService,
            'splashScreens': this.splashScreenService,
            'maps': this.mapsService,
            'timeline': this.timeLineService,
            'qrcode': this.qRCodeService,
            'radioButton': this.radioButtonService,
            'range': this.rangeService,
            'toggle': this.toggleService,
            'select': this.selectService,
            'form': this.formService,
            'comment': this.commentService,
            'profile': this.profileService,
            'actionSheet': this.actionSheetService,
            'segment': this.segmentService,
            'payment': this.paymentService,
            'alert': this.alertService,
        };
        this.componentName = navParams.get('componentName');
        this.service = this.listServices[this.componentName];
        this.isBuyButtonEnabled = __WEBPACK_IMPORTED_MODULE_5__services_app_settings__["a" /* AppSettings */].BUY_BUTTON;
        if (this.service) {
            this.pages = this.service.getAllThemes();
            this.checkAndLoadPage();
            this.title = this.service.getTitle();
        }
        else {
            navCtrl.setRoot("HomePage");
            return;
        }
    }
    ItemsPage.prototype.checkAndLoadPage = function () {
        var page = null;
        var asRoot = false;
        // if (this.componentName != "login" && this.componentName != "register") {
        //   let page = "login"
        //   this.componentName = page
        //   this.service = this.listServices[page]
        // }
        this.pages = this.service.getAllThemes();
        switch (this.componentName) {
            case "payment":
                page = this.pages[0]; //load first layout
                break;
            case "timeline":
                // asRoot = true
                page = this.pages[0]; //load first layout
                break;
            case "login":
                page = this.pages[0]; //load first layout
                break;
            case "register":
                page = this.pages[0]; //load first layout
                break;
            case "profile":
                page = this.pages[0]; //load third layout
                break;
            case "alert":
                page = this.pages[0]; //load third layout
                break;
            default:
                break;
        }
        if (page) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_37__home_home__["a" /* HomePage */]); //making sure we coming back to home page
            this.openPage(page, asRoot);
        }
    };
    ItemsPage.prototype.selectPageForOpen = function (value) {
        var page;
        switch (value) {
            case "spinner":
                page = "ItemDetailsPageSpinner";
                break;
            case "profile":
                page = "ItemDetailsPageProfile";
                break;
            case "alert":
                page = "ItemDetailsPageAlert";
                break;
            case "payment":
                page = "ItemDetailsPagePayment";
                break;
            case "login":
                page = "ItemDetailsPageLogin";
                break;
            case "register":
                page = "ItemDetailsPageRegister";
                break;
            case "actionSheet":
                page = "ItemDetailsPageActionSheet";
                break;
            case "timeline":
                page = "ItemDetailsPageTimeLine";
                break;
            default:
                page = "ItemDetailsPage";
        }
        return page;
    };
    // openPage(page: any) {
    //   if (AppSettings.SUBSCRIBE) {
    //     if (this.mailChimpService.showMailChimpForm()) {
    //       this.mailChimpService.setMailChimpForm(false);
    //       this.showPrompt();
    //     } else {
    //       this.navigation(page);
    //     }
    //   } else {
    //     this.navigation(page);
    //   }
    // }
    ItemsPage.prototype.openPage = function (page, asRoot) {
        if (asRoot === void 0) { asRoot = false; }
        if (__WEBPACK_IMPORTED_MODULE_5__services_app_settings__["a" /* AppSettings */].SUBSCRIBE) {
            if (this.mailChimpService.showMailChimpForm()) {
                this.mailChimpService.setMailChimpForm(false);
                this.showPrompt();
            }
            else {
                this.navigation(page);
            }
        }
        else {
            this.navigation(page, asRoot);
        }
    };
    // navigation(page: any) {
    //   if (page.listView) {
    //     this.navCtrl.push(ItemsPage, {
    //       componentName: page.theme
    //     });
    //   } else {
    //     this.navCtrl.push(this.selectPageForOpen(this.componentName), {
    //       service: this.service,
    //       page: page
    //     });
    //   }
    // }
    ItemsPage.prototype.navigation = function (page, asRoot) {
        if (asRoot === void 0) { asRoot = false; }
        console.log("Nav", page);
        if (page.listView) {
            /* this.navCtrl.push(ItemsPage, {
              componentName: page.theme
            }); */
        }
        else {
            this.navCtrl.push(this.selectPageForOpen(this.componentName), {
                service: this.service,
                page: page
            });
        }
    };
    ItemsPage.prototype.showPrompt = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'STAY TUNED FOR NEW <br> THEMES AND FREEBIES',
            message: "SUBSCRIBE TO <br> OUR NEWSLETTER",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Your e-mail'
                },
            ],
            buttons: [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        if (data) {
                            if (_this.validationService.isMail(data.email)) {
                                _this.httpService.sendData(data.email).subscribe(function (data) {
                                    _this.mailChimpService.hideMailChimp();
                                }, function (err) {
                                    alert(err);
                                }, null);
                            }
                            else {
                                return false;
                            }
                        }
                        else {
                            return false;
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    ItemsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/items/items.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <div buy *ngIf="isBuyButtonEnabled">\n\n      <ion-title>{{title}}</ion-title>\n\n      <button ion-button defoult-button\n\n        onclick="window.open(\'https://codecanyon.net/item/ionic-3-ui-themetemplate-app-material-design-blue-light/20234423?ref=CreativeForm\')">\n\n        BUY NOW\n\n      </button>\n\n    </div>\n\n    <!---Title-->\n\n    <ion-title *ngIf="!isBuyButtonEnabled">{{title}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content no-padding>\n\n  <ion-list no-padding no-margin *ngIf="pages.length">\n\n    <button ion-item padding-left no-lines item-title submenu *ngFor="let p of pages" (click)="checkAndLoadPage(p)">\n\n      {{p.title}}\n\n    </button>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/items/items.html"*/,
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__services_mail_chimp_service__["a" /* MailChimpService */], __WEBPACK_IMPORTED_MODULE_7__services_text_view_service__["a" /* TextViewService */], __WEBPACK_IMPORTED_MODULE_8__services_spinner_service__["a" /* SpinnerService */],
                __WEBPACK_IMPORTED_MODULE_9__services_splash_screen_service__["a" /* SplashScreenService */], __WEBPACK_IMPORTED_MODULE_13__services_login_service__["a" /* LoginService */], __WEBPACK_IMPORTED_MODULE_14__services_register_service__["a" /* RegisterService */],
                __WEBPACK_IMPORTED_MODULE_10__services_search_bar_service__["a" /* SearchBarService */], __WEBPACK_IMPORTED_MODULE_6__services_check_box_service__["a" /* CheckBoxService */], __WEBPACK_IMPORTED_MODULE_11__services_wizard_service__["a" /* WizardService */], __WEBPACK_IMPORTED_MODULE_12__services_tabs_service__["a" /* TabsService */], __WEBPACK_IMPORTED_MODULE_21__services_parallax_service__["a" /* ParallaxService */],
                __WEBPACK_IMPORTED_MODULE_15__services_list_view_service__["a" /* ListViewService */], __WEBPACK_IMPORTED_MODULE_16__services_list_view_expandable_service__["a" /* ListViewExpandableService */], __WEBPACK_IMPORTED_MODULE_17__services_list_view_drag_and_drop_service__["a" /* ListViewDragAndDropService */], __WEBPACK_IMPORTED_MODULE_22__services_image_gallery_service__["a" /* ImageGalleryService */],
                __WEBPACK_IMPORTED_MODULE_18__services_list_view_swipe_to_dismiss_service__["a" /* ListViewSwipeToDismissService */], __WEBPACK_IMPORTED_MODULE_20__services_list_view_google_card_service__["a" /* ListViewGoogleCardsService */], __WEBPACK_IMPORTED_MODULE_19__services_list_view_appearance_animation_service__["a" /* ListViewAppearanceAnimationService */],
                __WEBPACK_IMPORTED_MODULE_2__services_HttpService__["a" /* HttpService */], __WEBPACK_IMPORTED_MODULE_3__services_validation__["a" /* ValidationService */], __WEBPACK_IMPORTED_MODULE_23__services_maps_service__["a" /* MapsService */], __WEBPACK_IMPORTED_MODULE_24__services_qrcode_service__["a" /* QRCodeService */], __WEBPACK_IMPORTED_MODULE_25__services_radio_button_service__["a" /* RadioButtonService */], __WEBPACK_IMPORTED_MODULE_26__services_range_service__["a" /* RangeService */],
                __WEBPACK_IMPORTED_MODULE_27__services_toggle_service__["a" /* ToggleService */], __WEBPACK_IMPORTED_MODULE_28__services_select_service__["a" /* SelectService */], __WEBPACK_IMPORTED_MODULE_29__services_action_sheet_service__["a" /* ActionSheetService */], __WEBPACK_IMPORTED_MODULE_30__services_time_line_service__["a" /* TimeLineService */], __WEBPACK_IMPORTED_MODULE_31__services_form_service__["a" /* FormService */], __WEBPACK_IMPORTED_MODULE_32__services_comment_service__["a" /* CommentService */],
                __WEBPACK_IMPORTED_MODULE_33__services_profile_service__["a" /* ProfileService */], __WEBPACK_IMPORTED_MODULE_35__services_segment_service__["a" /* SegmentService */], __WEBPACK_IMPORTED_MODULE_36__services_alert_service__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_34__services_payment_service__["a" /* PaymentService */]
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_7__services_text_view_service__["a" /* TextViewService */],
            __WEBPACK_IMPORTED_MODULE_8__services_spinner_service__["a" /* SpinnerService */],
            __WEBPACK_IMPORTED_MODULE_22__services_image_gallery_service__["a" /* ImageGalleryService */],
            __WEBPACK_IMPORTED_MODULE_10__services_search_bar_service__["a" /* SearchBarService */],
            __WEBPACK_IMPORTED_MODULE_6__services_check_box_service__["a" /* CheckBoxService */],
            __WEBPACK_IMPORTED_MODULE_21__services_parallax_service__["a" /* ParallaxService */],
            __WEBPACK_IMPORTED_MODULE_11__services_wizard_service__["a" /* WizardService */],
            __WEBPACK_IMPORTED_MODULE_12__services_tabs_service__["a" /* TabsService */],
            __WEBPACK_IMPORTED_MODULE_15__services_list_view_service__["a" /* ListViewService */],
            __WEBPACK_IMPORTED_MODULE_16__services_list_view_expandable_service__["a" /* ListViewExpandableService */],
            __WEBPACK_IMPORTED_MODULE_17__services_list_view_drag_and_drop_service__["a" /* ListViewDragAndDropService */],
            __WEBPACK_IMPORTED_MODULE_18__services_list_view_swipe_to_dismiss_service__["a" /* ListViewSwipeToDismissService */],
            __WEBPACK_IMPORTED_MODULE_19__services_list_view_appearance_animation_service__["a" /* ListViewAppearanceAnimationService */],
            __WEBPACK_IMPORTED_MODULE_20__services_list_view_google_card_service__["a" /* ListViewGoogleCardsService */],
            __WEBPACK_IMPORTED_MODULE_13__services_login_service__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_14__services_register_service__["a" /* RegisterService */],
            __WEBPACK_IMPORTED_MODULE_9__services_splash_screen_service__["a" /* SplashScreenService */],
            __WEBPACK_IMPORTED_MODULE_2__services_HttpService__["a" /* HttpService */],
            __WEBPACK_IMPORTED_MODULE_3__services_validation__["a" /* ValidationService */],
            __WEBPACK_IMPORTED_MODULE_4__services_mail_chimp_service__["a" /* MailChimpService */],
            __WEBPACK_IMPORTED_MODULE_23__services_maps_service__["a" /* MapsService */],
            __WEBPACK_IMPORTED_MODULE_24__services_qrcode_service__["a" /* QRCodeService */],
            __WEBPACK_IMPORTED_MODULE_25__services_radio_button_service__["a" /* RadioButtonService */],
            __WEBPACK_IMPORTED_MODULE_26__services_range_service__["a" /* RangeService */],
            __WEBPACK_IMPORTED_MODULE_27__services_toggle_service__["a" /* ToggleService */],
            __WEBPACK_IMPORTED_MODULE_28__services_select_service__["a" /* SelectService */],
            __WEBPACK_IMPORTED_MODULE_29__services_action_sheet_service__["a" /* ActionSheetService */],
            __WEBPACK_IMPORTED_MODULE_30__services_time_line_service__["a" /* TimeLineService */],
            __WEBPACK_IMPORTED_MODULE_31__services_form_service__["a" /* FormService */],
            __WEBPACK_IMPORTED_MODULE_32__services_comment_service__["a" /* CommentService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_33__services_profile_service__["a" /* ProfileService */],
            __WEBPACK_IMPORTED_MODULE_35__services_segment_service__["a" /* SegmentService */],
            __WEBPACK_IMPORTED_MODULE_34__services_payment_service__["a" /* PaymentService */],
            __WEBPACK_IMPORTED_MODULE_36__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ItemsPage);
    return ItemsPage;
}());

//# sourceMappingURL=items.js.map

/***/ }),

/***/ 963:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by DRAGAN on 3/22/2017.
 */


var HttpService = /** @class */ (function () {
    function HttpService(http) {
        this.http = http;
    }
    HttpService.prototype.sendData = function (email) {
        return this.http.get("http://facebook.us14.list-manage.com/subscribe/post-json?u=2c0f7baa8dc004a62ff3922e3&id=456928d791&EMAIL=" + encodeURI(email) + "&b_2c0f7baa8dc004a62ff3922e3_456928d791");
    };
    HttpService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClient */]])
    ], HttpService);
    return HttpService;
}());

//# sourceMappingURL=HttpService.js.map

/***/ }),

/***/ 964:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ValidationService; });
/**
 * Created by DRAGAN on 3/22/2017.
 */
var ValidationService = /** @class */ (function () {
    function ValidationService() {
    }
    ValidationService.prototype.isMail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    return ValidationService;
}());

//# sourceMappingURL=validation.js.map

/***/ }),

/***/ 965:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MailChimpService; });
var MailChimpService = /** @class */ (function () {
    function MailChimpService() {
    }
    MailChimpService.prototype.showMailChimpForm = function () {
        var mailChimp = localStorage.getItem("mailChimp") == "true";
        var mailChimpLocal = localStorage.getItem("mailChimpLocal") == "true";
        if (mailChimp) {
            return false;
        }
        else {
            return mailChimpLocal;
        }
    };
    MailChimpService.prototype.setMailChimpForm = function (isVisible) {
        localStorage.setItem("mailChimpLocal", isVisible + "");
    };
    MailChimpService.prototype.hideMailChimp = function () {
        localStorage.setItem("mailChimp", "true");
    };
    return MailChimpService;
}());

//# sourceMappingURL=mail-chimp-service.js.map

/***/ }),

/***/ 966:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckBoxService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CheckBoxService = /** @class */ (function () {
    function CheckBoxService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'checkBoxes'; };
        this.getTitle = function () { return 'Check Boxes'; };
        this.getAllThemes = function () {
            return [
                { "title": "Simple", "theme": "layout1" },
                { "title": "With Avatar", "theme": "layout2" },
                { "title": "Simple 2", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set data for Check Box - SIMPLE
        this.getDataForLayout1 = function () {
            return [
                {
                    "id": 1,
                    "title": "Song",
                    "icon": "",
                    "favorite": false,
                    "image": ""
                },
                {
                    "id": 2,
                    "title": "Album",
                    "icon": "",
                    "favorite": false,
                    "image": ""
                },
                {
                    "id": 3,
                    "title": "Artist",
                    "icon": "",
                    "favorite": false,
                    "image": ""
                },
                {
                    "id": 4,
                    "title": "Song",
                    "icon": "",
                    "favorite": true,
                    "image": ""
                },
                {
                    "id": 5,
                    "title": "Album",
                    "icon": "",
                    "favorite": false,
                    "image": ""
                },
                {
                    "id": 6,
                    "title": "Artist",
                    "icon": "",
                    "favorite": false,
                    "image": ""
                }
            ];
        };
        // Set data for Check Box - WITH AVATAR
        this.getDataForLayout2 = function () {
            return [
                {
                    "id": 1,
                    "title": "Vanessa Ryan",
                    "icon": "",
                    "favorite": true,
                    "image": "assets/images/avatar/1.jpg"
                },
                {
                    "id": 2,
                    "title": "Lara Lynn",
                    "icon": "",
                    "favorite": false,
                    "image": "assets/images/avatar/2.jpg"
                },
                {
                    "id": 3,
                    "title": "Gayle Gaines",
                    "icon": "",
                    "favorite": false,
                    "image": "assets/images/avatar/3.jpg"
                },
                {
                    "id": 4,
                    "title": "Barbara Bernard",
                    "icon": "",
                    "favorite": false,
                    "image": "assets/images/avatar/4.jpg"
                },
                {
                    "id": 5,
                    "title": "Josefa Gardner",
                    "icon": "",
                    "favorite": false,
                    "image": "assets/images/avatar/5.jpg"
                },
                {
                    "id": 6,
                    "title": "Juliette Medina",
                    "icon": "",
                    "favorite": true,
                    "image": "assets/images/avatar/6.jpg"
                }
            ];
        };
        // Set data for Check Box - SIMPLE 2
        this.getDataForLayout3 = function () {
            return [
                {
                    "id": 1,
                    "title": "Song",
                    "favorite": true,
                    "icon": "icon-music-box"
                },
                {
                    "id": 2,
                    "title": "Album",
                    "favorite": false,
                    "icon": "icon-music-box"
                },
                {
                    "id": 3,
                    "title": "Artist",
                    "favorite": false,
                    "icon": "icon-music-box"
                },
                {
                    "id": 4,
                    "title": "Song",
                    "favorite": true,
                    "icon": "icon-music-box"
                },
                {
                    "id": 5,
                    "title": "Album",
                    "favorite": false,
                    "icon": "icon-music-box"
                },
                {
                    "id": 6,
                    "title": "Artist",
                    "favorite": false,
                    "icon": "icon-music-box"
                }
            ];
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                onButton: function (item) {
                    that.toastCtrl.presentToast(item.title);
                }
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    CheckBoxService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('checkBoxes/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    CheckBoxService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], CheckBoxService);
    return CheckBoxService;
}());

//# sourceMappingURL=check-box-service.js.map

/***/ }),

/***/ 967:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TextViewService; });
var TextViewService = /** @class */ (function () {
    function TextViewService() {
        var _this = this;
        this.getId = function () { return 'textViews'; };
        this.getTitle = function () { return 'Typo + small components'; };
        this.getAllThemes = function () {
            return [
                { "title": "All", "theme": "all" },
            ];
        };
        this.getEventsForTheme = function (menuItem) {
            return {};
        };
        this.prepareParams = function (item) {
            return {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
        };
    }
    TextViewService.prototype.load = function (url) {
        return null;
    };
    return TextViewService;
}());

//# sourceMappingURL=text-view-service.js.map

/***/ }),

/***/ 968:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpinnerService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SpinnerService = /** @class */ (function () {
    function SpinnerService(loadingService) {
        var _this = this;
        this.loadingService = loadingService;
        this.getId = function () { return 'spinners'; };
        this.getTitle = function () { return 'Spinners'; };
        this.getAllThemes = function () {
            return [
                { "title": "All", "theme": "all" }
            ];
        };
        this.getDataForAll = function () {
            return [
                { "icon": "tail-spin" },
                { "icon": "oval" },
                { "icon": "audio" },
                { "icon": "bars" },
                { "icon": "hearts" },
                { "icon": "three-dots" },
                { "icon": "puff" },
                { "icon": "grid" },
                { "icon": "ball-triangle" },
                { "icon": "circles" }
            ];
        };
        this.getEventsForTheme = function (menuItem) {
            return {};
        };
        this.prepareParams = function (item) {
            return {
                spinner: true,
                title: item.title,
                data: {},
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
        };
    }
    SpinnerService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        return new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) {
            that.loadingService.hide();
            observer.next(_this.getDataForAll());
            observer.complete();
        });
    };
    ;
    SpinnerService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__loading_service__["a" /* LoadingService */]])
    ], SpinnerService);
    return SpinnerService;
}());

//# sourceMappingURL=spinner-service.js.map

/***/ }),

/***/ 969:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashScreenService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SplashScreenService = /** @class */ (function () {
    function SplashScreenService(loadingService) {
        var _this = this;
        this.loadingService = loadingService;
        this.getId = function () { return 'splashScreens'; };
        this.getTitle = function () { return 'Splash screens'; };
        this.getAllThemes = function () {
            return [
                { "title": "Fade in + Ken Burns 1", "theme": "layout1" },
                { "title": "Down + fade in + Ken Burns", "theme": "layout2" },
                { "title": "Down + Ken Burns", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Splash Screen - FADE IN + KEN BURNS 1
        this.getDataForLayout1 = function () {
            return {
                "duration": 10000,
                "backgroundImage": 'assets/images/background/29.jpg',
                "logo": 'assets/images/logo/login.png',
                "title": ""
            };
        };
        // Set Data For Splash Screen - DOWN + FADE IN + KEN BURNS
        this.getDataForLayout2 = function () {
            return {
                "duration": 10000,
                "backgroundImage": 'assets/images/background/29.jpg',
                "logo": 'assets/images/logo/login.png',
                "title": ""
            };
        };
        // Set Data For Splash Screen - DOWN + KEN BURNS
        this.getDataForLayout3 = function () {
            return {
                "duration": 10000,
                "backgroundImage": 'assets/images/background/31.jpg',
                "logo": 'assets/images/logo/login-3.png',
                "title": "IONICTEMPLATE"
            };
        };
        this.getEventsForTheme = function (menuItem, navCtrl) {
            return {
                "onRedirect": function () {
                    navCtrl.pop();
                }
            };
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
        this.prepareParams = function (item, navCtrl) {
            var result = {
                title: item.title,
                data: _this.getDataForTheme(item),
                events: _this.getEventsForTheme(item, navCtrl)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
    }
    SplashScreenService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        return new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) {
            that.loadingService.hide();
            observer.next(_this.getDataForTheme(item));
            observer.complete();
        });
    };
    ;
    SplashScreenService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__loading_service__["a" /* LoadingService */]])
    ], SplashScreenService);
    return SplashScreenService;
}());

//# sourceMappingURL=splash-screen-service.js.map

/***/ }),

/***/ 970:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchBarService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SearchBarService = /** @class */ (function () {
    function SearchBarService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'searchBars'; };
        this.getTitle = function () { return 'Search bars'; };
        this.getAllThemes = function () {
            return [
                { "title": "Simple", "theme": "layout1" },
                { "title": "Field + header", "theme": "layout2" },
                { "title": "Field + header 2", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Search Bars - SIMPLE
        this.getDataForLayout1 = function () {
            return {
                "toolBarTitle": "Simple",
                "items": [
                    {
                        "title": "Friends",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Enemies",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Neutral",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Family",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Guests",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Students",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Friends",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Enemies",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Neutral",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Family",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Guests",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    },
                    {
                        "title": "Students",
                        "description": "254 people",
                        "icon": "icon-account-check"
                    }
                ]
            };
        };
        // Set Data For Search Bars - FIELD + HEADER
        this.getDataForLayout2 = function () {
            return {
                "toolBarTitle": "Field + header",
                "headerImage": "assets/images/background/30.jpg",
                "searchText": "Search by",
                "items": [
                    {
                        "title": "Song",
                        "icon": "icon-music-box"
                    },
                    {
                        "title": "Album",
                        "icon": "icon-headset"
                    },
                    {
                        "title": "Artist",
                        "icon": "icon-human-child"
                    },
                    {
                        "title": "Genre",
                        "icon": "icon-menu"
                    },
                    {
                        "title": "Song",
                        "icon": "icon-music-box"
                    },
                    {
                        "title": "Album",
                        "icon": "icon-headset"
                    },
                    {
                        "title": "Artist",
                        "icon": "icon-human-child"
                    },
                    {
                        "title": "Genre",
                        "icon": "icon-menu"
                    },
                    {
                        "title": "Album",
                        "icon": "icon-headset"
                    }
                ]
            };
        };
        // Set Data For Search Bars - FIELD + HEADER 2
        this.getDataForLayout3 = function () {
            return {
                "headerImage": "assets/images/background/9.jpg",
                "items": [
                    {
                        "id": 1,
                        "title": "Monument walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    },
                    {
                        "id": 2,
                        "title": "River walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    },
                    {
                        "id": 3,
                        "title": "City walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    },
                    {
                        "id": 4,
                        "title": "Park walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    },
                    {
                        "id": 5,
                        "title": "Vilage walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    },
                    {
                        "id": 6,
                        "title": "Lake walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    },
                    {
                        "id": 7,
                        "title": "Castle walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    },
                    {
                        "id": 8,
                        "title": "Beach walk tour",
                        "description": "3:30min walking tour",
                        "price": "123$",
                        "icon": "icon-map-marker-radius"
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onTextChange': function (text) {
                    that.toastCtrl.presentToast(text);
                },
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item.title);
                }
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: {},
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    SearchBarService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('searchBars/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ;
    SearchBarService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], SearchBarService);
    return SearchBarService;
}());

//# sourceMappingURL=search-bar-service.js.map

/***/ }),

/***/ 971:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WizardService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var WizardService = /** @class */ (function () {
    function WizardService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'wizard'; };
        this.getTitle = function () { return 'Wizard'; };
        this.getAllThemes = function () {
            return [
                { "title": "Simple + icon", "theme": "layout1" },
                { "title": "Big image", "theme": "layout2" },
                { "title": "Big Image + Text", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Wizard - SIMPLE + ICON
        this.getDataForLayout1 = function () {
            return {
                "toolBarTitle": "Simple + icon",
                "btnPrev": "Previous",
                "btnNext": "Next",
                "btnFinish": "Finish",
                "items": [
                    {
                        "iconSlider": "icon-star-outline",
                        "title": "Fortuitu ad aeroportus",
                        "description": "Morbi lacinia interdum nulla penatibus amet nibh adipiscing semper ligula, tempor sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minima veniam, quis exercitationem ullamco nostrud laboris nisi ut ex ea commodo aliquip consequat.",
                        "buttonNext": "Next"
                    },
                    {
                        "iconSlider": "icon-star-half",
                        "title": "Communications moderatoris",
                        "description": "Dolor in reprehenderit in duis irure voluptate velit esse fugiat dolore eu nulla pariatur cillum. Non cupidatat excepteur occaecat proident sint, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                        "buttonNext": "Next",
                        "buttonPrevious": "Previous"
                    },
                    {
                        "iconSlider": "icon-star",
                        "title": "Hoc est exortum",
                        "description": "Ut enim ad minima veniam, quis exercitationem ullamco nostrud laboris nisi ut ex ea commodo aliquip consequat. Dolor in reprehenderit in duis irure voluptate velit esse fugiat dolore eu nulla pariatur cillum.",
                        "buttonPrevious": "Previous",
                        "buttonFinish": "Finish"
                    }
                ]
            };
        };
        // Set Data For Wizard - BIG IMAGE
        this.getDataForLayout2 = function () {
            return {
                "toolBarTitle": "Big image",
                "btnNext": "Next",
                "btnFinish": "Finish",
                "items": [
                    {
                        "backgroundImage": "assets/images/avatar-large/1.jpg",
                        "title": "Fortuitu ad aeroportus"
                    },
                    {
                        "backgroundImage": "assets/images/avatar-large/2.jpg",
                        "title": "Communications moderatoris"
                    },
                    {
                        "backgroundImage": "assets/images/avatar-large/3.jpg",
                        "title": "Hoc est exortum"
                    }
                ]
            };
        };
        // Set Data For Wizard - BIG IMAGE + TEXT
        this.getDataForLayout3 = function () {
            return {
                "btnNext": "Next",
                "btnFinish": "Finish",
                "btnSkip": "Skip",
                "items": [
                    {
                        "backgroundImage": "assets/images/avatar-large/5.jpg",
                        "title": "Lorem Ipsum is simply dummy text of the printing and typesetting industry",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
                        "button": "Next",
                        "skip": "Skip"
                    },
                    {
                        "backgroundImage": "assets/images/avatar-large/6.jpg",
                        "title": "All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary",
                        "description": "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                        "button": "Next",
                        "skip": "Skip"
                    },
                    {
                        "backgroundImage": "assets/images/avatar-large/7.jpg",
                        "title": "The generated Lorem Ipsum is therefore always free from repetition, injected humour",
                        "description": " Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem",
                        "button": "Finish",
                        "skip": "Skip"
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onFinish': function (event) {
                    that.toastCtrl.presentToast("Finish");
                }
            };
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
    }
    WizardService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('wizard/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ;
    WizardService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], WizardService);
    return WizardService;
}());

//# sourceMappingURL=wizard-service.js.map

/***/ }),

/***/ 972:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginService = /** @class */ (function () {
    function LoginService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'login'; };
        this.getTitle = function () { return 'User Login'; };
        this.getAllThemes = function () {
            return [
                { "title": "User Login", "theme": "layout1" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Login Pages - lOGIN + LOGO 1
        this.getDataForLayout1 = function () {
            return {
                "username": "Username",
                "password": "Password",
                "register": "Register",
                "login": "Login",
                "skip": "Skip",
                "logo": "https://res.cloudinary.com/brainethic/image/upload/v1546860632/EI-logo-transperent_vvwqdz.png",
                "errorUser": "Field can't be empty.",
                "errorPassword": "Field can't be empty."
            };
        };
        // Set Data For Login Pages - lOGIN + LOGO 2
        // getDataForLayout2 = (): any => {
        //     return {
        //         "username": "Username",
        //         "password": "Password",
        //         "register": "Register",
        //         "login": "Login",
        //         "skip": "Skip",
        //         "logo": "assets/images/logo/login.png",
        //         "errorUser" : "Field can't be empty.",
        //         "errorPassword" : "Field can't be empty."
        //     };
        // };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                onLogin: function (params) {
                    that.toastCtrl.presentToast('onLogin:' + JSON.stringify(params));
                },
                onRegister: function (params) {
                    that.toastCtrl.presentToast('onRegister:' + JSON.stringify(params));
                },
                onSkip: function (params) {
                    that.toastCtrl.presentToast('onSkip:' + JSON.stringify(params));
                },
                onFacebook: function (params) {
                    that.toastCtrl.presentToast('onFacebook:' + JSON.stringify(params));
                },
                onTwitter: function (params) {
                    that.toastCtrl.presentToast('onTwitter:' + JSON.stringify(params));
                },
                onGoogle: function (params) {
                    that.toastCtrl.presentToast('onGoogle:' + JSON.stringify(params));
                },
                onPinterest: function (params) {
                    that.toastCtrl.presentToast('onPinterest:' + JSON.stringify(params));
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                theme: item.theme,
                data: {},
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    LoginService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('login/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    LoginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], LoginService);
    return LoginService;
}());

//# sourceMappingURL=login-service.js.map

/***/ }),

/***/ 973:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RegisterService = /** @class */ (function () {
    function RegisterService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'register'; };
        this.getTitle = function () { return 'User Registration'; };
        this.getAllThemes = function () {
            return [
                { "title": "User Registration", "theme": "layout2" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Register Pages - REGISTER + LOGO 1
        // getDataForLayout1 = (): any => {
        //     return {
        //         "logo": "",
        //         "register": "Register",
        //         "username": "Username",
        //         "city": "City",
        //         "country": "Country",
        //         "password": "Password",
        //         "email": "Email",
        //         "button": "submit",
        //         "skip": "Skip",
        //         "errorUser": "Field can't be empty.",
        //         "errorPassword": "Field can't be empty.",
        //         "errorEmail": "Invalid email address.",
        //         "errorCountry": "Field can't be empty.",
        //         "errorCity": "Field can't be empty."
        //     };
        // };
        // Set Data For Register Pages - REGISTER + LOGO 2
        this.getDataForLayout2 = function () {
            return {
                "logo": "https://res.cloudinary.com/brainethic/image/upload/v1546860632/EI-logo-transperent_vvwqdz.png",
                "iconAccount": "icon-account",
                "username": "Username",
                "iconHome": "icon-home-variant",
                "iconCity": "icon-city",
                "city": "City",
                "iconWeb": "icon-web",
                "country": "Country",
                "iconLock": "icon-lock",
                "password": "Password",
                "iconEmail": "icon-email-outline",
                "email": "Email",
                "submit": "submit",
                "skip": "Skip",
                "errorUser": "Field can't be empty.",
                "errorPassword": "Field can't be empty.",
                "errorEmail": "Invalid email address.",
                "errorCountry": "Field can't be empty.",
                "errorCity": "Field can't be empty."
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                onRegister: function (params) {
                    that.toastCtrl.presentToast('onRegister:' + JSON.stringify(params));
                },
                onSkip: function (params) {
                    that.toastCtrl.presentToast('onSkip:' + JSON.stringify(params));
                }
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                theme: item.theme,
                data: {},
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    RegisterService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('register/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ;
    RegisterService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], RegisterService);
    return RegisterService;
}());

//# sourceMappingURL=register-service.js.map

/***/ }),

/***/ 974:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListViewService; });
var ListViewService = /** @class */ (function () {
    function ListViewService() {
        var _this = this;
        this.getId = function () { return 'listViews'; };
        this.getTitle = function () { return 'List views'; };
        this.getAllThemes = function () {
            return [
                { "title": "Expandable", "theme": "expandable", "listView": true },
                { "title": "Drag&Drop", "theme": "dragAndDrop", "listView": true },
                { "title": "Swipe-to-dismiss", "theme": "swipeToDismiss", "listView": true },
                { "title": "Appearance animations", "theme": "appearanceAnimation", "listView": true },
                { "title": "Google Cards", "theme": "googleCards", "listView": true },
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return [];
        };
        this.getEventsForTheme = function (menuItem) {
            return {};
        };
        this.prepareParams = function (item) {
            return {
                title: item.title,
                data: _this.getDataForTheme(item),
                events: _this.getEventsForTheme(item)
            };
        };
    }
    ListViewService.prototype.load = function (url) {
        return null;
    };
    return ListViewService;
}());

//# sourceMappingURL=list-view-service.js.map

/***/ }),

/***/ 975:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListViewExpandableService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListViewExpandableService = /** @class */ (function () {
    function ListViewExpandableService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'expandable'; };
        this.getTitle = function () { return 'Expandable'; };
        this.getAllThemes = function () {
            return [
                { "title": "List big image", "theme": "layout1" },
                { "title": "Full image with CTA", "theme": "layout2" },
                { "title": "Centered with header", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Expandable - LIST BIG IMAGE
        this.getDataForLayout1 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "title": "Benton Willis",
                        "description": "SINGER",
                        "image": "assets/images/avatar/15.jpg",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant",
                        "items": [
                            {
                                "id": 1,
                                "title": "Smokestack Lightning",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/10.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 2,
                                "title": "Boogie Chillen",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/11.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 3,
                                "title": "Call It Stormy Mondaye",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/12.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 4,
                                "title": "I’m Tore Down",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/13.jpg",
                                "iconPlay": "icon-play-circle"
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "title": "Jessica Miles",
                        "description": "BASSO",
                        "image": "assets/images/avatar/2.jpg",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant",
                        "items": [
                            {
                                "id": 1,
                                "title": "Bell Bottom Blue",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/14.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 2,
                                "title": "Still Got The Blues",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/15.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 3,
                                "title": "Mustang Sally",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/14.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 4,
                                "title": "Ball N’ Chain",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/13.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 5,
                                "title": "Sweet Home Chicago",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/12.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 6,
                                "title": "Born Under A Bad Sign",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/11.jpg",
                                "iconPlay": "icon-play-circle"
                            }
                        ]
                    },
                    {
                        "id": 3,
                        "title": "Holman Valencia",
                        "description": "GUITARIST",
                        "image": "assets/images/avatar/3.jpg",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant",
                        "items": [
                            {
                                "id": 1,
                                "title": "Dust My Broom",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/11.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 2,
                                "title": "Hold On, I’m Coming",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/12.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 3,
                                "title": "The Little Red Rooster",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/13.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 4,
                                "title": "Bright Lights",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/14.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 5,
                                "title": "Down In The Hole",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/15.jpg",
                                "iconPlay": "icon-play-circle"
                            }
                        ]
                    },
                    {
                        "id": 4,
                        "title": "Natasha Gambl",
                        "description": "SINGER",
                        "image": "assets/images/avatar/4.jpg",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant",
                        "items": [
                            {
                                "id": 1,
                                "title": "Got My Mojo Working",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/0.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 2,
                                "title": "A Little Less Conversation",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/1.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 3,
                                "title": "Life By The Drop",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/2.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 4,
                                "title": "Boom Boom",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/3.jpg",
                                "iconPlay": "icon-play-circle"
                            }
                        ]
                    },
                    {
                        "id": 5,
                        "title": "Carol Kelly",
                        "description": "DRUMMER",
                        "image": "assets/images/avatar/5.jpg",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant",
                        "items": [
                            {
                                "id": 1,
                                "title": "Thing Called Love",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/14.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 2,
                                "title": "Green Onions",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/15.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 3,
                                "title": "The Midnight Special",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/6.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 4,
                                "title": "Mess Around",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/7.jpg",
                                "iconPlay": "icon-play-circle"
                            }
                        ]
                    },
                    {
                        "id": 6,
                        "title": "Mildred Clark",
                        "description": "DRUMMER",
                        "image": "assets/images/avatar/3.jpg",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant",
                        "items": [
                            {
                                "id": 1,
                                "title": "Little Wing",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/14.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 2,
                                "title": "Bad Penny",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/15.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 3,
                                "title": "Farther on Up the Road",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/6.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 4,
                                "title": "Mannish Boy",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/7.jpg",
                                "iconPlay": "icon-play-circle"
                            }
                        ]
                    },
                    {
                        "id": 7,
                        "title": "Megan Singleton",
                        "description": "DRUMMER",
                        "image": "assets/images/avatar/4.jpg",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant",
                        "items": [
                            {
                                "id": 1,
                                "title": "Trouble No More",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/14.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 2,
                                "title": "Hellhound On My Trail",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/15.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 3,
                                "title": "Help Me",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/6.jpg",
                                "iconPlay": "icon-play-circle"
                            },
                            {
                                "id": 4,
                                "title": "A Man Of Many Words",
                                "description": "Universal, 2016",
                                "image": "assets/images/avatar/7.jpg",
                                "iconPlay": "icon-play-circle"
                            }
                        ]
                    }
                ]
            };
        };
        // Set Data For Expandable - FULL IMAGE WITH CTA
        this.getDataForLayout2 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "title": "Rubus idaeus Pi",
                        "backgroundImage": "assets/images/background/22.jpg",
                        "button": "BUY",
                        "items": [
                            "PAY WITH PAYPAL",
                            "PAY WITH VISA CARD",
                            "PAY WITH MAESTRO CARD"
                        ]
                    },
                    {
                        "id": 2,
                        "title": "Nidum Thermostat",
                        "backgroundImage": "assets/images/background/23.jpg",
                        "button": "BUY",
                        "items": [
                            "PAY WITH PAYPAL",
                            "PAY WITH VISA CARD",
                            "PAY WITH MAESTRO CARD"
                        ]
                    },
                    {
                        "id": 3,
                        "title": "Baculum Magicum",
                        "backgroundImage": "assets/images/background/24.jpg",
                        "button": "BUY",
                        "items": [
                            "PAY WITH PAYPAL",
                            "PAY WITH VISA CARD",
                            "PAY WITH MAESTRO CARD"
                        ]
                    },
                    {
                        "id": 4,
                        "title": "Palm Nauclerus",
                        "backgroundImage": "assets/images/background/25.jpg",
                        "button": "BUY",
                        "items": [
                            "PAY WITH PAYPAL",
                            "PAY WITH VISA CARD",
                            "PAY WITH MAESTRO CARD"
                        ]
                    },
                    {
                        "id": 5,
                        "title": "Commodore LXIV",
                        "backgroundImage": "assets/images/background/26.jpg",
                        "button": "BUY",
                        "items": [
                            "PAY WITH PAYPAL",
                            "PAY WITH VISA CARD",
                            "PAY WITH MAESTRO CARD"
                        ]
                    },
                    {
                        "id": 6,
                        "title": "Optio Fridericus Hultsch Box",
                        "backgroundImage": "assets/images/background/27.jpg",
                        "button": "BUY",
                        "items": [
                            "PAY WITH PAYPAL",
                            "PAY WITH VISA CARD",
                            "PAY WITH MAESTRO CARD"
                        ]
                    },
                    {
                        "id": 7,
                        "title": "Oculi Odium",
                        "backgroundImage": "assets/images/background/28.jpg",
                        "button": "BUY",
                        "items": [
                            "PAY WITH PAYPAL",
                            "PAY WITH VISA CARD",
                            "PAY WITH MAESTRO CARD"
                        ]
                    }
                ]
            };
        };
        // Set Data For Expandable - CENTERED WITH HEADER
        this.getDataForLayout3 = function () {
            return {
                "title": "New York",
                "headerImage": "assets/images/background-small/10.jpg",
                "items": [
                    {
                        "title": "Where to go",
                        "icon": "icon-map-marker-radius",
                        "items": [
                            "Monuments",
                            "Sightseeing",
                            "Historical",
                            "Sport"
                        ]
                    },
                    {
                        "title": "Where to sleep",
                        "icon": "icon-hotel",
                        "items": [
                            "Hotels",
                            "Hostels",
                            "Motels",
                            "Rooms"
                        ]
                    },
                    {
                        "title": "Where to eat",
                        "icon": "icon-silverware-variant",
                        "items": [
                            "Fast Food",
                            "Restorants",
                            "Pubs",
                            "Hotels"
                        ]
                    },
                    {
                        "title": "Where to drink",
                        "icon": "icon-martini",
                        "items": [
                            "Caffes",
                            "Bars",
                            "Pubs",
                            "Clubs"
                        ]
                    },
                    {
                        "title": "Where to go",
                        "icon": "icon-map-marker-radius",
                        "items": [
                            "Monuments",
                            "Sightseeing",
                            "Historical",
                            "Sport"
                        ]
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item);
                },
                'onLike': function (item) {
                    that.toastCtrl.presentToast("Like");
                },
                'onFavorite': function (item) {
                    that.toastCtrl.presentToast("Favorite");
                },
                'onShare': function (item) {
                    that.toastCtrl.presentToast("Share");
                },
                'onFab': function (item) {
                    that.toastCtrl.presentToast("Fab");
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                theme: item.theme,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ListViewExpandableService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                console.log(item.theme);
                _this.af
                    .object('listView/expandable/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ListViewExpandableService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ListViewExpandableService);
    return ListViewExpandableService;
}());

//# sourceMappingURL=list-view-expandable-service.js.map

/***/ }),

/***/ 976:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListViewDragAndDropService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListViewDragAndDropService = /** @class */ (function () {
    function ListViewDragAndDropService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'dragAndDrop'; };
        this.getTitle = function () { return 'Drag and Drop'; };
        this.getAllThemes = function () {
            return [
                { "title": "Small item + header", "theme": "layout1" },
                { "title": "Products + CTA header", "theme": "layout2" },
                { "title": "Medium item with avatar", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Drag And Drop - SMALL ITEM + HEADER
        this.getDataForLayout1 = function () {
            return {
                "title": "Playlist Name",
                "description": "Author: White Castaneda",
                "duration": "35:72",
                "icon": "icon-check",
                "items": [
                    {
                        "id": 1,
                        "title": "Surprise Of Midnight",
                        "author": "Author: Vanessa Ryan",
                        "image": "assets/images/avatar/0.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 2,
                        "title": "Brave Heart",
                        "author": "Author: Meredith Hendricks",
                        "image": "assets/images/avatar/1.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 3,
                        "title": "Broken Fever",
                        "author": "Author: Carol Kelly",
                        "image": "assets/images/avatar/2.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 4,
                        "title": "Come With Me",
                        "author": "Author: Barrera Ramsey",
                        "image": "assets/images/avatar/3.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 5,
                        "title": "Petty Game",
                        "author": "Author: Holman Valencia",
                        "image": "assets/images/avatar/4.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 6,
                        "title": "Business Of Tears",
                        "author": "Author: Marisa Cain",
                        "image": "assets/images/avatar/5.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 7,
                        "title": "She Said I Miss You",
                        "author": "Author: Dejesus Norris",
                        "image": "assets/images/avatar/6.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 8,
                        "title": "Darling, So Do I",
                        "author": "Author: Gayle Gaines",
                        "image": "assets/images/avatar/0.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 9,
                        "title": "I'm All Alone",
                        "author": "Author: Prince Phelps",
                        "image": "assets/images/avatar/1.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 10,
                        "title": "Need My Worries",
                        "author": "Author: Keri Hudson",
                        "image": "assets/images/avatar/2.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 11,
                        "title": "Dark Of Midnight",
                        "author": "Author: Duran Clayton",
                        "image": "assets/images/avatar/3.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 12,
                        "title": "I Know I'm Lonely",
                        "author": "Author: Schmidt English",
                        "image": "assets/images/avatar/4.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 13,
                        "title": "She's Trouble",
                        "author": "Author: Lara Lynn",
                        "image": "assets/images/avatar/5.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    },
                    {
                        "id": 14,
                        "title": "He Heard He's Crazy",
                        "author": "Author: Randall Hurley",
                        "image": "assets/images/avatar/6.jpg",
                        "leftIcon": "icon-play-circle",
                        "rightIcon": "icon-unfold-more"
                    }
                ]
            };
        };
        // Set Data For Drag And Drop - PRODUCT + CTA HEADER
        this.getDataForLayout2 = function () {
            return {
                "title": "Order No. 1",
                "description": "Will be shipped: 15.5.2016.",
                "buttonText": "PROCEED",
                "headerImage": "assets/images/background/17.jpg",
                "price": "$42.99",
                "items": [
                    {
                        "id": 1,
                        "title": "Black Shirt",
                        "seller": "Seller Name",
                        "image": "assets/images/avatar/17.jpg",
                        "oldPrice": "$42.99",
                        "shipping": "free shipping",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 2,
                        "title": "Black Sweater",
                        "seller": "Seller Name",
                        "image": "assets/images/avatar/18.jpg",
                        "oldPrice": "$42.99",
                        "shipping": "free shipping",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 3,
                        "title": "Shirt",
                        "seller": "Seller Name",
                        "image": "assets/images/avatar/19.jpg",
                        "oldPrice": "$42.99",
                        "shipping": "free shipping",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 4,
                        "title": "White Shirt",
                        "seller": "Seller Name",
                        "image": "assets/images/avatar/20.jpg",
                        "oldPrice": "$42.99",
                        "shipping": "free shipping",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 5,
                        "title": "White T shirt",
                        "seller": "Seller Name",
                        "image": "assets/images/avatar/21.jpg",
                        "oldPrice": "$42.99",
                        "shipping": "free shipping",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 6,
                        "title": "T shirt",
                        "seller": "Seller Name",
                        "image": "assets/images/avatar/22.jpg",
                        "oldPrice": "$42.99",
                        "shipping": "free shipping",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 7,
                        "title": "Hoodies",
                        "seller": "Seller Name",
                        "image": "assets/images/avatar/23.jpg",
                        "oldPrice": "$42.99",
                        "shipping": "free shipping",
                        "newPrice": "$35.99"
                    }
                ]
            };
        };
        // Set Data For Drag And Drop - MEDIUM ITEM WITH AVATAR
        this.getDataForLayout3 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "title": "Isaac Reid",
                        "description": "from Brogan ",
                        "image": "assets/images/avatar/0.jpg"
                    },
                    {
                        "id": 2,
                        "title": "Jason Graham",
                        "description": "from Rehrersburg",
                        "image": "assets/images/avatar/1.jpg"
                    },
                    {
                        "id": 3,
                        "title": "Abigail Ross",
                        "description": "from Durham ",
                        "image": "assets/images/avatar/2.jpg"
                    },
                    {
                        "id": 4,
                        "title": "Justin Rutherford",
                        "description": "from Callaghan",
                        "image": "assets/images/avatar/3.jpg"
                    },
                    {
                        "id": 5,
                        "title": "Nicholas Henderson",
                        "description": "from Manitou",
                        "image": "assets/images/avatar/4.jpg"
                    },
                    {
                        "id": 6,
                        "title": "Elizabeth Mackenzie",
                        "description": "from Weedville",
                        "image": "assets/images/avatar/5.jpg"
                    },
                    {
                        "id": 7,
                        "title": "Melanie Ferguson",
                        "description": "from Curtice",
                        "image": "assets/images/avatar/6.jpg"
                    },
                    {
                        "id": 8,
                        "title": "Fiona Kelly",
                        "description": "from Barronett",
                        "image": "assets/images/avatar/7.jpg"
                    },
                    {
                        "id": 9,
                        "title": "Nicholas King",
                        "description": "from Urie",
                        "image": "assets/images/avatar/8.jpg"
                    },
                    {
                        "id": 10,
                        "title": "Victoria Mitchell",
                        "description": "from Blackgum",
                        "image": "assets/images/avatar/9.jpg"
                    },
                    {
                        "id": 11,
                        "title": "Sophie Lyman",
                        "description": "from Williston",
                        "image": "assets/images/avatar/10.jpg"
                    },
                    {
                        "id": 12,
                        "title": "Carl Ince",
                        "description": "from Norvelt",
                        "image": "assets/images/avatar/11.jpg"
                    },
                    {
                        "id": 13,
                        "title": "Michelle Slater",
                        "description": "from Maxville",
                        "image": "assets/images/avatar/12.jpg"
                    },
                    {
                        "id": 14,
                        "title": "Ryan Mathis",
                        "description": "from Bannock",
                        "image": "assets/images/avatar/13.jpg"
                    },
                    {
                        "id": 15,
                        "title": "Julia Grant",
                        "description": "from Nutrioso",
                        "image": "assets/images/avatar/14.jpg"
                    },
                    {
                        "id": 16,
                        "title": "Hannah Martin",
                        "description": "from Bluetown",
                        "image": "assets/images/avatar/15.jpg"
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item);
                },
                'onProceed': function (item) {
                    that.toastCtrl.presentToast("Proceed");
                },
                'onFab': function (item) {
                    that.toastCtrl.presentToast("Fab");
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                theme: item.theme,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ListViewDragAndDropService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('listView/dragAndDrop/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ListViewDragAndDropService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ListViewDragAndDropService);
    return ListViewDragAndDropService;
}());

//# sourceMappingURL=list-view-drag-and-drop-service.js.map

/***/ }),

/***/ 977:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListViewSwipeToDismissService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListViewSwipeToDismissService = /** @class */ (function () {
    function ListViewSwipeToDismissService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'swipeToDismiss'; };
        this.getTitle = function () { return 'Swipe to dismiss'; };
        this.getAllThemes = function () {
            return [
                { "title": "Small item + header", "theme": "layout1" },
                { "title": "Products + CTA", "theme": "layout2" },
                { "title": "Full with image", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Swipe To Dismiss - SMALL ITEM + HEADER
        this.getDataForLayout1 = function () {
            return {
                "title": "HeaderTitle",
                "description": "HeaderSubtitle",
                "shortDescription": "35:72",
                "iconLike": "icon-thumb-up",
                "iconFavorite": "icon-heart",
                "iconShare": "icon-share-variant",
                "iconPlay": "icon-play-circle-outline",
                "items": [
                    {
                        "id": 1,
                        "title": "Song Of Souls",
                        "description": "Shawna Norman",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/0.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 2,
                        "title": "Tune Of My Obsessions",
                        "description": "Janice Wilder",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/1.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 3,
                        "title": "Reject Her Tomorrow",
                        "description": "Lucy Bender",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/2.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 4,
                        "title": "Troubles Of My Blues",
                        "description": "ouglas Burks",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/3.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 5,
                        "title": "Broken Tonight",
                        "description": "Sophia Cochran",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/4.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 6,
                        "title": "Sure Choices",
                        "description": "Lara Lynn",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/5.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 7,
                        "title": "A Storm Is Coming",
                        "description": "Juliette Medina",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/6.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 8,
                        "title": "He Heard He's Crazy",
                        "description": "Vanessa Ryan",
                        "shortDescription": "3:42",
                        "image": "assets/images/avatar/7.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    }
                ]
            };
        };
        // Set Data For Swipe To Dismiss - PRODUCT + CTA
        this.getDataForLayout2 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "title": "Black Shirt",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/17.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 2,
                        "title": "Black Sweater",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/18.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 3,
                        "title": "Shirt",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/19.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 4,
                        "title": "White Shirt",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/20.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 5,
                        "title": "White T shirt",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/21.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 6,
                        "title": "T shirt",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/22.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 7,
                        "title": "Hoodies",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/23.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 8,
                        "title": "Sweater",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/17.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 9,
                        "title": "White Sweater",
                        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
                        "image": "assets/images/avatar/18.jpg",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    }
                ]
            };
        };
        // Set Data For Swipe To Dismiss - FULL WITH IMAGE 
        this.getDataForLayout3 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "title": "Weedville",
                        "description": "Northern Mariana Islands",
                        "image": "assets/images/background-small/7.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 2,
                        "title": "Curtice",
                        "description": "Nauru",
                        "image": "assets/images/background-small/9.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 3,
                        "title": "Norvelt",
                        "description": "Indonesia",
                        "image": "assets/images/background-small/10.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 4,
                        "title": "Vincent",
                        "description": "Antarctica",
                        "image": "assets/images/background-small/11.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 5,
                        "title": "Fairacres",
                        "description": "Colombia",
                        "image": "assets/images/background-small/12.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 6,
                        "title": "Greenwich",
                        "description": "Tajikistan",
                        "image": "assets/images/background-small/13.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 7,
                        "title": "Ryderwood",
                        "description": "Sao Tome and Principe",
                        "image": "assets/images/background-small/9.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    },
                    {
                        "id": 8,
                        "title": "Lithium",
                        "description": "Puerto Rico",
                        "image": "assets/images/background-small/15.jpg",
                        "iconDelate": "icon-delete",
                        "iconUndo": "icon-undo-variant"
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item);
                },
                'onLike': function (item) {
                    that.toastCtrl.presentToast("Like");
                },
                'onFavorite': function (item) {
                    that.toastCtrl.presentToast("Favorite");
                },
                'onShare': function (item) {
                    that.toastCtrl.presentToast("Share");
                },
                'onFab': function (item) {
                    that.toastCtrl.presentToast("Fab");
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: {},
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ListViewSwipeToDismissService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('listView/swipeToDismiss/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ListViewSwipeToDismissService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ListViewSwipeToDismissService);
    return ListViewSwipeToDismissService;
}());

//# sourceMappingURL=list-view-swipe-to-dismiss-service.js.map

/***/ }),

/***/ 978:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListViewAppearanceAnimationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListViewAppearanceAnimationService = /** @class */ (function () {
    function ListViewAppearanceAnimationService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'appearanceAnimations'; };
        this.getTitle = function () { return 'Appearance animations'; };
        this.getAllThemes = function () {
            return [
                { "title": "Fade in left", "theme": "layout1" },
                { "title": "Fade in right", "theme": "layout2" },
                { "title": "Fade in down", "theme": "layout3" },
                { "title": "Fade in", "theme": "layout4" },
                { "title": "Zoom in", "theme": "layout5" }
            ];
        };
        // Set Data For Appearance Animations - FADE IN LEFT, fADE IN RIGHT, FADE IN DOWN, FADE IN, ZOOM IN
        this.getDataForTheme = function (menuItem) {
            return {
                "items": [
                    {
                        "id": 1,
                        "title": "Isaac Raid",
                        "image": "assets/images/avatar/0.jpg",
                        "favorite": true
                    },
                    {
                        "id": 2,
                        "title": "Jason Graham",
                        "image": "assets/images/avatar/1.jpg",
                        "favorite": false
                    },
                    {
                        "id": 3,
                        "title": "Abigail Ross",
                        "image": "assets/images/avatar/2.jpg",
                        "favorite": true
                    },
                    {
                        "id": 4,
                        "title": "Justin Rutherford",
                        "image": "assets/images/avatar/3.jpg",
                        "favorite": false
                    },
                    {
                        "id": 5,
                        "title": "Nicholas Henderson",
                        "image": "assets/images/avatar/4.jpg",
                        "favorite": false
                    },
                    {
                        "id": 6,
                        "title": "Elizabeth Mackenzie",
                        "image": "assets/images/avatar/5.jpg",
                        "favorite": true
                    },
                    {
                        "id": 7,
                        "title": "Melanie Ferguson",
                        "image": "assets/images/avatar/6.jpg",
                        "favorite": false
                    },
                    {
                        "id": 8,
                        "title": "Fiona Kelly",
                        "image": "assets/images/avatar/7.jpg",
                        "favorite": true
                    },
                    {
                        "id": 9,
                        "title": "Nicholas King",
                        "image": "assets/images/avatar/8.jpg",
                        "favorite": true
                    },
                    {
                        "id": 10,
                        "title": "Victoria Mitchell",
                        "image": "assets/images/avatar/9.jpg",
                        "favorite": true
                    },
                    {
                        "id": 11,
                        "title": "Sophie Lyman",
                        "image": "assets/images/avatar/10.jpg",
                        "favorite": false
                    },
                    {
                        "id": 12,
                        "title": "Carl Ince",
                        "image": "assets/images/avatar/11.jpg",
                        "favorite": false
                    },
                    {
                        "id": 13,
                        "title": "Michelle Slater",
                        "image": "assets/images/avatar/12.jpg",
                        "favorite": false
                    },
                    {
                        "id": 14,
                        "title": "Ryan Mathis",
                        "image": "assets/images/avatar/13.jpg",
                        "favorite": false
                    },
                    {
                        "id": 15,
                        "title": "Julia Grant",
                        "image": "assets/images/avatar/14.jpg",
                        "favorite": false
                    },
                    {
                        "id": 16,
                        "title": "Hannah Martin",
                        "image": "assets/images/avatar/15.jpg",
                        "favorite": false
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item);
                },
                'onFavorite': function (item) {
                    item.favorite = !item.favorite;
                },
                'onFab': function (item) {
                    that.toastCtrl.presentToast("Fab");
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                theme: item.theme,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ListViewAppearanceAnimationService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('listView/appearanceAnimations/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ListViewAppearanceAnimationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ListViewAppearanceAnimationService);
    return ListViewAppearanceAnimationService;
}());

//# sourceMappingURL=list-view-appearance-animation-service.js.map

/***/ }),

/***/ 979:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListViewGoogleCardsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ListViewGoogleCardsService = /** @class */ (function () {
    function ListViewGoogleCardsService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'googleCards'; };
        this.getTitle = function () { return 'Google Cards'; };
        this.getAllThemes = function () {
            return [
                { "title": "Styled cards", "theme": "layout1" },
                { "title": "Styled cards 2", "theme": "layout2" },
                { "title": "Full image cards", "theme": "layout3" }
            ];
        };
        // Set Data For Google Card - STYLED CARDS
        this.getDataForLayout1 = function () {
            return {
                "title": "PlaylistName",
                "description": "Author:Username",
                "duration": "35:72",
                "items": [
                    {
                        "id": 1,
                        "title": "Jessica Miles",
                        "image": "assets/images/avatar-small/0.jpg",
                        "description": "Birth year: 1984",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    },
                    {
                        "id": 2,
                        "title": "Natasha Gamble",
                        "image": "assets/images/avatar-small/1.jpg",
                        "description": "Birth year: 1980",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    },
                    {
                        "id": 3,
                        "title": "Julia Petersen",
                        "image": "assets/images/avatar-small/2.jpg",
                        "description": "Birth year: 1984",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    },
                    {
                        "id": 4,
                        "title": "Marisa Cain",
                        "image": "assets/images/avatar-small/3.jpg",
                        "description": "Birth year: 1984",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    },
                    {
                        "id": 5,
                        "title": "Lara Lynn",
                        "image": "assets/images/avatar-small/4.jpg",
                        "description": "Birth year: 1984",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    },
                    {
                        "id": 6,
                        "title": "Megan Singleton",
                        "image": "assets/images/avatar-small/5.jpg",
                        "description": "Birth year: 1984",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    },
                    {
                        "id": 7,
                        "title": "Susanna Simmons",
                        "image": "assets/images/avatar-small/6.jpg",
                        "description": "Birth year: 1984",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    },
                    {
                        "id": 8,
                        "title": "Juliette Medina",
                        "image": "assets/images/avatar-small/7.jpg",
                        "description": "Birth year: 1984",
                        "shortDescription": "Country: Germany",
                        "longDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do",
                        "iconLike": "icon-thumb-up",
                        "iconFavorite": "icon-heart",
                        "iconShare": "icon-share-variant"
                    }
                ]
            };
        };
        // Set Data For Google Card - STYLED CARDS 2
        this.getDataForLayout2 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "title": "Fortuitu ad aeroportus",
                        "titleHeader": "Simul quanta praecinctionis",
                        "description": "Contra opinione vulgi, accumsan non tantum temere text. Quod habet radices in fragmen literaturam Romanam classicam a XLV BC, MM facit super annos. Richard McClintock",
                        "image": "assets/images/background/1.jpg",
                        "button": "EXPLORE",
                        "shareButton": "SHARE"
                    },
                    {
                        "id": 2,
                        "title": "Hoc est exortum",
                        "titleHeader": "Pedestres sub imprudentia contentum",
                        "description": "Contra opinione vulgi, accumsan non tantum temere text. Quod habet radices in fragmen literaturam Romanam classicam a XLV BC, MM facit super annos. Richard McClintock",
                        "image": "assets/images/background/2.jpg",
                        "button": "EXPLORE",
                        "shareButton": "SHARE"
                    },
                    {
                        "id": 3,
                        "title": "Communications moderatoris",
                        "titleHeader": "Technica et Internet habeat facultatem",
                        "description": "Contra opinione vulgi, accumsan non tantum temere text. Quod habet radices in fragmen literaturam Romanam classicam a XLV BC, MM facit super annos. Richard McClintock",
                        "image": "assets/images/background/5.jpg",
                        "button": "EXPLORE",
                        "shareButton": "SHARE"
                    },
                    {
                        "id": 4,
                        "title": "Tabulas scripto munus agere providere",
                        "titleHeader": "Ut adeptus est atrium",
                        "description": "Contra opinione vulgi, accumsan non tantum temere text. Quod habet radices in fragmen literaturam Romanam classicam a XLV BC, MM facit super annos. Richard McClintock",
                        "image": "assets/images/background/3.jpg",
                        "button": "EXPLORE",
                        "shareButton": "SHARE"
                    },
                    {
                        "id": 5,
                        "title": "In outpatient nuntiatum ministerium",
                        "titleHeader": "Testis unus",
                        "description": "Contra opinione vulgi, accumsan non tantum temere text. Quod habet radices in fragmen literaturam Romanam classicam a XLV BC, MM facit super annos. Richard McClintock",
                        "image": "assets/images/background/1.jpg",
                        "button": "EXPLORE",
                        "shareButton": "SHARE"
                    }
                ]
            };
        };
        // Set Data For Google Card - FULL IMAGE CARDS
        this.getDataForLayout3 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "image": "assets/images/background/0.jpg",
                        "title": "Erat quia homo ex fireman",
                        "subtitle": "New York and Hamburg, illa ligatum ad columpnam illam in Septentrionalis."
                    },
                    {
                        "id": 2,
                        "image": "assets/images/background/9.jpg",
                        "title": "Et convertit in altilium in dextera",
                        "subtitle": "Erat illo tempore et coram dockworkers horrebant ei molestum insigne."
                    },
                    {
                        "id": 3,
                        "image": "assets/images/background/8.jpg",
                        "title": "Keighley dux cum respiciens",
                        "subtitle": "Virtute suae astu tamen libidini suae igne nautas comitarentur dolus."
                    },
                    {
                        "id": 4,
                        "image": "assets/images/background/10.jpg",
                        "title": "Et gubernatori duxit in Wheelhouse",
                        "subtitle": "Et ita tractantem bombacio ignes, sit modo ex eruditis illorum."
                    },
                    {
                        "id": 5,
                        "image": "assets/images/background/13.jpg",
                        "title": "Ut legati risere pugnae Moore",
                        "subtitle": "Lorem Ipsum is simply dummy text of the printing and typesetting."
                    },
                    {
                        "id": 6,
                        "image": "assets/images/background/11.jpg",
                        "title": "Keighley dux, hactenus eius motus",
                        "subtitle": "More triumphantis expressione malitia et hoc est, ex fireman Doherty."
                    },
                    {
                        "id": 7,
                        "image": "assets/images/background/12.jpg",
                        "title": "Quattuor hic Firemen qui",
                        "subtitle": "Legatum eum conniveret. Quod erat primum ostensum est in Keighley."
                    },
                    {
                        "id": 8,
                        "image": "assets/images/background/0.jpg",
                        "title": "In conspectu erant media",
                        "subtitle": "Et sonitus audiebatur Germanico sparteoli tussis est eis quasi ursa."
                    }
                ]
            };
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item);
                },
                'onExplore': function (item) {
                    that.toastCtrl.presentToast("Explore");
                },
                'onShare': function (item) {
                    that.toastCtrl.presentToast("Share");
                },
                'onLike': function (item) {
                    that.toastCtrl.presentToast("onLike");
                },
                'onFavorite': function (item) {
                    that.toastCtrl.presentToast("onFavorite");
                },
                'onFab': function (item) {
                    that.toastCtrl.presentToast("Fab");
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                theme: item.theme,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ListViewGoogleCardsService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('listView/googleCards/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ListViewGoogleCardsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ListViewGoogleCardsService);
    return ListViewGoogleCardsService;
}());

//# sourceMappingURL=list-view-google-card-service.js.map

/***/ }),

/***/ 980:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParallaxService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ParallaxService = /** @class */ (function () {
    function ParallaxService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'parallax'; };
        this.getTitle = function () { return 'Parallax'; };
        this.getAllThemes = function () {
            return [
                { "title": "Friends", "theme": "layout1" },
                { "title": "Product", "theme": "layout2" },
                { "title": "Basic", "theme": "layout3" },
                { "title": "Location Details", "theme": "layout4" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Parallax - FRIENDS
        this.getDataForLayout1 = function () {
            return {
                "headerImage": "assets/images/background/14.jpg",
                "toolBarTitle": "Parallax-title",
                "title": "Playlist Name",
                "iconLike": "icon-thumb-up",
                "iconFavorite": "icon-heart",
                "iconShare": "icon-share-variant",
                "items": [
                    {
                        "id": 1,
                        "title": "Friends Of Midnight",
                        "description": "Graciela Mitchell",
                        "image": "assets/images/avatar/0.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 2,
                        "title": "Home Of Yesterday",
                        "description": "Sherry Hale",
                        "image": "assets/images/avatar/1.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 3,
                        "title": "Kiss My Friends",
                        "description": "Shawna Norman",
                        "image": "assets/images/avatar/2.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 4,
                        "title": "Talk About His Right",
                        "description": "Gail Harrington",
                        "image": "assets/images/avatar/3.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 5,
                        "title": "Boring Dreams",
                        "description": "Tricia Yang",
                        "image": "assets/images/avatar/4.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 6,
                        "title": "Lazy Song",
                        "description": "Ines Campbell",
                        "image": "assets/images/avatar/5.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 7,
                        "title": "We Won't Make It",
                        "description": "Lindsey Mcgowan",
                        "image": "assets/images/avatar/6.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 8,
                        "title": "I Know I'm Lonely",
                        "description": "Lucy Bender",
                        "image": "assets/images/avatar/7.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 9,
                        "title": "Days For Forever",
                        "description": "Petra Brewer",
                        "image": "assets/images/avatar/1.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 10,
                        "title": "Honey, Let Me Go",
                        "description": "Wendi Michael",
                        "image": "assets/images/avatar/2.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 11,
                        "title": "Forgot His Heart",
                        "description": "Sherri Stokes",
                        "image": "assets/images/avatar/0.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    },
                    {
                        "id": 12,
                        "title": "Plain Old Mind",
                        "description": "Bryan Conway",
                        "image": "assets/images/avatar/3.jpg",
                        "imageAlt": "avatar",
                        "icon": "icon-cloud-download",
                        "duration": "3:42"
                    }
                ]
            };
        };
        // Set Data For Parallax - PRODUCT
        this.getDataForLayout2 = function () {
            return {
                "headerImage": "assets/images/background/7.jpg",
                "toolBarTitle": "Product",
                "title": "Super discount",
                "description": "50% OFF",
                "iconLike": "icon-thumb-up",
                "iconFavorite": "icon-heart",
                "iconShare": "icon-share-variant",
                "items": [
                    {
                        "id": 1,
                        "title": "Black Shirt",
                        "image": "assets/images/avatar/0.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 2,
                        "title": "Black Sweater",
                        "image": "assets/images/avatar/1.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 3,
                        "title": "Shirt",
                        "image": "assets/images/avatar/2.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 4,
                        "title": "White Shirt",
                        "image": "assets/images/avatar/3.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 5,
                        "title": "White T Shirt",
                        "image": "assets/images/avatar/4.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 6,
                        "title": "Hoodies",
                        "image": "assets/images/avatar/5.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 7,
                        "title": "Black Shirt",
                        "image": "assets/images/avatar/0.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 8,
                        "title": "Black Sweater",
                        "image": "assets/images/avatar/1.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 9,
                        "title": "Shirt",
                        "image": "assets/images/avatar/2.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 10,
                        "title": "White Shirt",
                        "image": "assets/images/avatar/3.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 11,
                        "title": "White T Shirt",
                        "image": "assets/images/avatar/4.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    },
                    {
                        "id": 12,
                        "title": "Hoodies",
                        "image": "assets/images/avatar/5.jpg",
                        "description": "Duis aute irure dolor in reprehenderit",
                        "oldPrice": "$42.99",
                        "newPrice": "$35.99"
                    }
                ]
            };
        };
        // Set Data For Parallax - BASIC
        this.getDataForLayout3 = function () {
            return {
                "headerImage": "assets/images/background/2.jpg",
                "avatar": "assets/images/avatar/0.jpg",
                "items": [
                    {
                        "id": 1,
                        "title": "Isaac Raid",
                        "image": "assets/images/avatar/0.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 2,
                        "title": "Jason Graham",
                        "image": "assets/images/avatar/1.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 3,
                        "title": "Abigail Ross",
                        "image": "assets/images/avatar/2.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 4,
                        "title": "Justin Rutherford",
                        "image": "assets/images/avatar/3.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 5,
                        "title": "Nicholas Henderson",
                        "image": "assets/images/avatar/4.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 6,
                        "title": "Elizabeth Mackenzie",
                        "image": "assets/images/avatar/5.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 7,
                        "title": "Melanie Ferguson",
                        "image": "assets/images/avatar/6.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 8,
                        "title": "Fiona Kelly",
                        "image": "assets/images/avatar/7.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 9,
                        "title": "Nicholas King",
                        "image": "assets/images/avatar/8.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 10,
                        "title": "Victoria Mitchell",
                        "image": "assets/images/avatar/9.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 11,
                        "title": "Sophie Lyman",
                        "image": "assets/images/avatar/10.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 12,
                        "title": "Carl Ince",
                        "image": "assets/images/avatar/11.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 13,
                        "title": "Michelle Slater",
                        "image": "assets/images/avatar/12.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 14,
                        "title": "Ryan Mathis",
                        "image": "assets/images/avatar/13.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 15,
                        "title": "Julia Grant",
                        "image": "assets/images/avatar/14.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    },
                    {
                        "id": 16,
                        "title": "Hannah Martin",
                        "image": "assets/images/avatar/15.jpg",
                        "icon": "icon-heart-outline",
                        "favorite": false
                    }
                ]
            };
        };
        // Set Data For Parallax - LOCATION DETAILS
        this.getDataForLayout4 = function () {
            return {
                "headerImage": "assets/images/background/15.jpg",
                "title": "Joe's restaurant",
                "iconLike": "icon-thumb-up",
                "iconFavorite": "icon-comment",
                "iconShare": "icon-share-variant",
                "reviews": "4.12 (78 reviews)",
                "iconButton": "icon-walk",
                "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.",
                "description2": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.",
                "description3": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using Content here, content here, making it look like readable English",
                "description4": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.",
                "iconsStars": [
                    {
                        "isActive": true,
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star"
                    },
                    {
                        "isActive": true,
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star"
                    },
                    {
                        "isActive": true,
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star"
                    },
                    {
                        "isActive": true,
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star"
                    },
                    {
                        "isActive": false,
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star"
                    }
                ],
                "items": [
                    {
                        "id": 1,
                        "name": "ADDRESS:",
                        "value": "Boulevard of food, New York, USA"
                    },
                    {
                        "id": 2,
                        "name": "PHONE:",
                        "value": "+555 555 555"
                    },
                    {
                        "id": 3,
                        "name": "WEB:",
                        "value": "www.joesrestaurant.com"
                    },
                    {
                        "id": 4,
                        "name": "MAIL:",
                        "value": "jimmy@gmail.com"
                    },
                    {
                        "id": 5,
                        "name": "WORKING HOURS:",
                        "value": "7:00 to 23:00 every day"
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onLike': function (item) {
                    that.toastCtrl.presentToast("Like");
                },
                'onFavorite': function (item) {
                    item.favorite = !item.favorite;
                    that.toastCtrl.presentToast("Favorite");
                },
                'onShare': function (item) {
                    that.toastCtrl.presentToast("Share");
                },
                'onFab': function (item) {
                    that.toastCtrl.presentToast("Fab");
                },
                'onRates': function (index) {
                    that.toastCtrl.presentToast("Rates " + (index + 1));
                },
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: {},
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ParallaxService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('parallax/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ;
    ParallaxService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ParallaxService);
    return ParallaxService;
}());

//# sourceMappingURL=parallax-service.js.map

/***/ }),

/***/ 981:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageGalleryService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ImageGalleryService = /** @class */ (function () {
    function ImageGalleryService(af, loadingService) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.getId = function () { return 'imageGallery'; };
        this.getTitle = function () { return 'Image Gallery'; };
        this.getAllThemes = function () {
            return [
                { "title": "Category 1 (Animals)", "theme": "layout1" },
                { "title": "Subcategory 1 (Dogs)", "theme": "subcategory" },
                { "title": "Category 2 (Music)", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Gallery - CATEGORY 1 (ANIMALS)
        this.getDataForLayout1 = function () {
            return {
                "toolBarTitle": "CATEGORY 1 (ANIMALS)",
                "items": [
                    {
                        "id": 1,
                        "title": "Dogs",
                        "image": "assets/images/gallery/dogs/14250733433_271362f4ff_h.jpg",
                        "favorite": true,
                        "items": [
                            {
                                "id": 1,
                                "title": "Dog 1",
                                "image": "assets/images/gallery/dogs/14250733433_271362f4ff_h.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Dog 2",
                                "image": "assets/images/gallery/dogs/174085592_c0b7e5076f_o.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Dog 3",
                                "image": "assets/images/gallery/dogs/174097822_6c0aae32e1_o.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Dog 4",
                                "image": "assets/images/gallery/dogs/4733823624_a758c8e8e9_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Dog 5",
                                "image": "assets/images/gallery/dogs/4939588185_4dc4fa8cca_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Dog 6",
                                "image": "assets/images/gallery/dogs/6698150783_161b18182e_b.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Dog 7",
                                "image": "assets/images/gallery/dogs/6787786882_4e74d00628_b.jpg"
                            },
                            {
                                "id": 8,
                                "title": "Dog 8",
                                "image": "assets/images/gallery/dogs/8206632393_36622366c6_k.jpg"
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "title": "Horses",
                        "image": "assets/images/gallery/horses/1242426877_9cdace7a19_b.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Horses 1",
                                "image": "assets/images/gallery/horses/1242426877_9cdace7a19_b.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Horses 2",
                                "image": "assets/images/gallery/horses/2795764001_1e899c8560_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Horses 3",
                                "image": "assets/images/gallery/horses/3571783564_b02308c31c_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Horses 4",
                                "image": "assets/images/gallery/horses/3724307231_7077e8a5da_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Horses 5",
                                "image": "assets/images/gallery/horses/548518280_b408f3d2a5_o.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Horses 6",
                                "image": "assets/images/gallery/horses/6990704333_abff211880_b.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Horses 7",
                                "image": "assets/images/gallery/horses/8621170742_15cd967dc8_b.jpg"
                            },
                            {
                                "id": 8,
                                "title": "Horses 8",
                                "image": "assets/images/gallery/horses/9581032545_e388ee7d3e_b.jpg"
                            }
                        ]
                    },
                    {
                        "id": 3,
                        "title": "Cats",
                        "image": "assets/images/gallery/cats/15636456045_f99e3a5ccd_k.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Cats 1",
                                "image": "assets/images/gallery/cats/15636456045_f99e3a5ccd_k.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Cats 2",
                                "image": "assets/images/gallery/cats/3285731954_a31261bd38_o.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Cats 3",
                                "image": "assets/images/gallery/cats/560380352_8b58b0611c_o.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Cats 4",
                                "image": "assets/images/gallery/cats/6131811835_b7cdb594f3_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Cats 4",
                                "image": "assets/images/gallery/cats/6944735673_7d201cbb98_k.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Cats 5",
                                "image": "assets/images/gallery/cats/8470085922_ed703dcda3_b.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Cats 6",
                                "image": "assets/images/gallery/cats/8854205418_3739d5b3e9_h.jpg"
                            },
                            {
                                "id": 8,
                                "title": "Cats 7",
                                "image": "assets/images/gallery/cats/9308613838_de3df521b0_b.jpg"
                            },
                            {
                                "id": 9,
                                "title": "Cats 8",
                                "image": "assets/images/gallery/cats/9657345685_b680cd813b_k.jpg"
                            }
                        ]
                    },
                    {
                        "id": 4,
                        "title": "Kangaroos",
                        "image": "assets/images/gallery/kangaroos/14018941654_b24dc40edd_k.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Kangaroo 1",
                                "image": "assets/images/gallery/kangaroos/14018941654_b24dc40edd_k.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Kangaroo 2",
                                "image": "assets/images/gallery/kangaroos/4849609708_06a24058ec_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Kangaroo 3",
                                "image": "assets/images/gallery/kangaroos/4901737404_cf375a44d3_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Kangaroo 4",
                                "image": "assets/images/gallery/kangaroos/4962092282_15a076cb8c_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Kangaroo 5",
                                "image": "assets/images/gallery/kangaroos/5366854530_35d6de81c2_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Kangaroo 6",
                                "image": "assets/images/gallery/kangaroos/7643187732_3753aa9b70_k.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Kangaroo 7",
                                "image": "assets/images/gallery/kangaroos/8275651371_0e5e6bffc4_k.jpg"
                            },
                            {
                                "id": 8,
                                "title": "Kangaroo 8",
                                "image": "assets/images/gallery/kangaroos/8672908837_1ec6c9c967_b.jpg"
                            }
                        ]
                    },
                    {
                        "id": 5,
                        "title": "Foxes",
                        "image": "assets/images/gallery/foxes/2741843163_5aefaae694_b.jpg",
                        "favorite": true,
                        "items": [
                            {
                                "id": 1,
                                "title": "Fox 1",
                                "image": "assets/images/gallery/foxes/2741843163_5aefaae694_b.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Fox 2",
                                "image": "assets/images/gallery/foxes/5461393397_b7bbff4c87_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Fox 3",
                                "image": "assets/images/gallery/foxes/5872864880_b1739e6e76_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Fox 4",
                                "image": "assets/images/gallery/foxes/6904712695_751bb39e16_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Fox 5",
                                "image": "assets/images/gallery/foxes/6904714319_c4dbbb2b14_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Fox 6",
                                "image": "assets/images/gallery/foxes/6977256115_af1011fbfb_h.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Fox 7",
                                "image": "assets/images/gallery/foxes/7132259891_878e6513c4_k.jpg"
                            },
                            {
                                "id": 8,
                                "title": "Fox 8",
                                "image": "assets/images/gallery/foxes/7238361098_5d244ea023_b.jpg"
                            }
                        ]
                    },
                    {
                        "id": 6,
                        "title": "Eagles",
                        "image": "assets/images/gallery/eagles/12111485434_015bcf17e0_k.jpg",
                        "favorite": true,
                        "items": [
                            {
                                "id": 1,
                                "title": "Eagles 1",
                                "image": "assets/images/gallery/eagles/12111485434_015bcf17e0_k.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Eagles 2",
                                "image": "assets/images/gallery/eagles/12387369564_ee3e9810f3_k.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Eagles 3",
                                "image": "assets/images/gallery/eagles/3358262896_8a623dc7ca_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Eagles 4",
                                "image": "assets/images/gallery/eagles/6130420503_b5516d49e4_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Eagles 5",
                                "image": "assets/images/gallery/eagles/7924818318_d4f0355bdd_k.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Eagles 6",
                                "image": "assets/images/gallery/eagles/8626880581_7308f6a8a7_k.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Eagles 7",
                                "image": "assets/images/gallery/eagles/9395841437_95035aed96_k.jpg"
                            }
                        ]
                    },
                    {
                        "id": 7,
                        "title": "Squirrels",
                        "image": "assets/images/gallery/squirrels/12683899725_ec12a691c5_k.jpg",
                        "favorite": true,
                        "items": [
                            {
                                "id": 1,
                                "title": "Squirrels 1",
                                "image": "assets/images/gallery/squirrels/12683899725_ec12a691c5_k.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Squirrels 2",
                                "image": "assets/images/gallery/squirrels/12935575784_bcb43443f9_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Squirrels 3",
                                "image": "assets/images/gallery/squirrels/15365307348_82551c3cb6_h.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Squirrels 4",
                                "image": "assets/images/gallery/squirrels/2438938256_655ed4f254_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Squirrels 5",
                                "image": "assets/images/gallery/squirrels/5194088091_f8af189189_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Squirrels 6",
                                "image": "assets/images/gallery/squirrels/5339749381_f1c0a3040b_b.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Squirrels 7",
                                "image": "assets/images/gallery/squirrels/6359846085_c88e231c01_b.jpg"
                            },
                            {
                                "id": 8,
                                "title": "Squirrels 8",
                                "image": "assets/images/gallery/squirrels/6583159839_0ba9c33a75_b.jpg"
                            }
                        ]
                    },
                    {
                        "id": 8,
                        "title": "Bears",
                        "image": "assets/images/gallery/bears/14316604273_778ccdac73_b.jpg",
                        "favorite": true,
                        "items": [
                            {
                                "id": 1,
                                "title": "Bears 1",
                                "image": "assets/images/gallery/bears/14316604273_778ccdac73_b.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Bears 2",
                                "image": "assets/images/gallery/bears/14567666406_61df9c0d52_k.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Bears 3",
                                "image": "assets/images/gallery/bears/15665160302_f87492c246_k.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Bears 4",
                                "image": "assets/images/gallery/bears/3946668599_90958b634c_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Bears 5",
                                "image": "assets/images/gallery/bears/5396353993_d8bcf19d5e_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Bears 6",
                                "image": "assets/images/gallery/bears/6346303116_7f3d463a68_b.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Bears 7",
                                "image": "assets/images/gallery/bears/8568153824_ae2b3d3a02_k.jpg"
                            },
                            {
                                "id": 8,
                                "title": "Bears 8",
                                "image": "assets/images/gallery/bears/8797595259_e99bf75d2e_k.jpg"
                            }
                        ]
                    }
                ]
            };
        };
        // Set Data For Gallery - SUBCATEGORY 1 (DOGS)
        this.getDataForSubcategory = function () {
            return {
                "title": "SUBCATEGORY 1 (DOG)",
                "items": [
                    {
                        "id": 1,
                        "title": "Dog 1",
                        "image": "assets/images/gallery/dogs/14250733433_271362f4ff_h.jpg",
                        "favorite": true
                    },
                    {
                        "id": 2,
                        "title": "Dog 2",
                        "image": "assets/images/gallery/dogs/174085592_c0b7e5076f_o.jpg",
                        "favorite": true
                    },
                    {
                        "id": 3,
                        "title": "Dog 3",
                        "image": "assets/images/gallery/dogs/174097822_6c0aae32e1_o.jpg",
                        "favorite": true
                    },
                    {
                        "id": 4,
                        "title": "Dog 4",
                        "image": "assets/images/gallery/dogs/4733823624_a758c8e8e9_b.jpg",
                        "favorite": true
                    },
                    {
                        "id": 5,
                        "title": "Dog 5",
                        "image": "assets/images/gallery/dogs/4939588185_4dc4fa8cca_b.jpg",
                        "favorite": true
                    },
                    {
                        "id": 6,
                        "title": "Dog 6",
                        "image": "assets/images/gallery/dogs/6698150783_161b18182e_b.jpg",
                        "favorite": true
                    },
                    {
                        "id": 7,
                        "title": "Dog 7",
                        "image": "assets/images/gallery/dogs/6787786882_4e74d00628_b.jpg",
                        "favorite": true
                    },
                    {
                        "id": 8,
                        "title": "Dog 8",
                        "image": "assets/images/gallery/dogs/8206632393_36622366c6_k.jpg",
                        "favorite": true
                    }
                ]
            };
        };
        // Set Data For Gallery - CATEGORY 2 (MUSIC)
        this.getDataForLayout3 = function () {
            return {
                "toolBarTitle": "CATEGORY 2 (MUSIC)",
                "items": [
                    {
                        "id": 1,
                        "title": "Violins",
                        "image": "assets/images/gallery/violins/147587554_205e3ed653_o.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Violin 1",
                                "image": "assets/images/gallery/violins/147587554_205e3ed653_o.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Violin 2",
                                "image": "assets/images/gallery/violins/2322113757_808c904f04_o.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Violin 3",
                                "image": "assets/images/gallery/violins/2323048092_b6c70654ef_o.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Violin 4",
                                "image": "assets/images/gallery/violins/2348623142_af7802400f_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Violin 5",
                                "image": "assets/images/gallery/violins/459221457_09e40e82a8_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Violin 6",
                                "image": "assets/images/gallery/violins/5950552903_7dbea63895_b.jpg"
                            }
                        ]
                    },
                    {
                        "id": 2,
                        "title": "Drums",
                        "image": "assets/images/gallery/drums/14258762970_33f3a049ed_b.jpg",
                        "favorite": true,
                        "items": [
                            {
                                "id": 1,
                                "title": "Drum 1",
                                "image": "assets/images/gallery/drums/14258762970_33f3a049ed_b.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Drum 2",
                                "image": "assets/images/gallery/drums/2419154841_a4c2015605_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Drum 3",
                                "image": "assets/images/gallery/drums/6500936951_bf8d21a4b7_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Drum 4",
                                "image": "assets/images/gallery/drums/7587609934_f90d316fa0_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Drum 5",
                                "image": "assets/images/gallery/drums/7672718592_ca4ccf7315_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Drum 6",
                                "image": "assets/images/gallery/drums/8229110151_3c963f6a8d_k.jpg"
                            }
                        ]
                    },
                    {
                        "id": 3,
                        "title": "Saxophones",
                        "image": "assets/images/gallery/saxophones/3022692180_fd02682a44_b.jpg",
                        "favorite": true,
                        "items": [
                            {
                                "id": 1,
                                "title": "Saxophones 1",
                                "image": "assets/images/gallery/saxophones/3022692180_fd02682a44_b.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Saxophones 2",
                                "image": "assets/images/gallery/saxophones/4268421378_e2d3ecdf1b_o.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Saxophones 3",
                                "image": "assets/images/gallery/saxophones/4586115189_d4886a2118_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Saxophones 4",
                                "image": "assets/images/gallery/saxophones/5823606898_77db8827b1_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Saxophones 5",
                                "image": "assets/images/gallery/saxophones/7807389560_57b1d5b5f8_k.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Saxophones 6",
                                "image": "assets/images/gallery/saxophones/870265218_716ebe2cb8_o.jpg"
                            }
                        ]
                    },
                    {
                        "id": 4,
                        "title": "Trumpets",
                        "image": "assets/images/gallery/trumpets/14163752896_453a37b84a_h.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Trumpet 1",
                                "image": "assets/images/gallery/trumpets/14163752896_453a37b84a_h.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Trumpet 2",
                                "image": "assets/images/gallery/trumpets/2531134817_ba97791524_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Trumpet 3",
                                "image": "assets/images/gallery/trumpets/4867822712_1b1d3da2cd_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Trumpet 4",
                                "image": "assets/images/gallery/trumpets/4949712803_7fc832d2cc_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Trumpet 5",
                                "image": "assets/images/gallery/trumpets/6186270505_cc834a94c6_o.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Trumpet 6",
                                "image": "assets/images/gallery/trumpets/7865792422_ef00cb7840_k.jpg"
                            },
                            {
                                "id": 7,
                                "title": "Trumpet 7",
                                "image": "assets/images/gallery/trumpets/8044715752_9855063570_k.jpg"
                            }
                        ]
                    },
                    {
                        "id": 5,
                        "title": "Pianos",
                        "image": "assets/images/gallery/pianos/14287113341_fe14885b70_b.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Piano 1",
                                "image": "assets/images/gallery/pianos/14287113341_fe14885b70_b.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Piano 2",
                                "image": "assets/images/gallery/pianos/5052063297_61b6386db5_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Piano 3",
                                "image": "assets/images/gallery/pianos/5200112333_502355c045_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Piano 4",
                                "image": "assets/images/gallery/pianos/5637747696_42cba72967_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Piano 5",
                                "image": "assets/images/gallery/pianos/7310209818_537dd46419_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Piano 6",
                                "image": "assets/images/gallery/pianos/8247668743_36fcf1f4bb_b.jpg"
                            }
                        ]
                    },
                    {
                        "id": 6,
                        "title": "Accordions",
                        "image": "assets/images/gallery/accordions/153706234_c473e8eabd_o.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Accordion 1",
                                "image": "assets/images/gallery/accordions/153706234_c473e8eabd_o.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Accordion 2",
                                "image": "assets/images/gallery/accordions/2681657661_177a5edbc5_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Accordion 3",
                                "image": "assets/images/gallery/accordions/3369363243_94fb76891d_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Accordion 4",
                                "image": "assets/images/gallery/accordions/5316360932_719617fa1d_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Accordion 5",
                                "image": "assets/images/gallery/accordions/6151326630_349b892d5a_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Accordion 6",
                                "image": "assets/images/gallery/accordions/7607076588_593e509440_h.jpg"
                            }
                        ]
                    },
                    {
                        "id": 7,
                        "title": "Accordions",
                        "image": "assets/images/gallery/accordions/153706234_c473e8eabd_o.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Accordion 1",
                                "image": "assets/images/gallery/accordions/153706234_c473e8eabd_o.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Accordion 2",
                                "image": "assets/images/gallery/accordions/2681657661_177a5edbc5_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Accordion 3",
                                "image": "assets/images/gallery/accordions/3369363243_94fb76891d_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Accordion 4",
                                "image": "assets/images/gallery/accordions/5316360932_719617fa1d_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Accordion 5",
                                "image": "assets/images/gallery/accordions/6151326630_349b892d5a_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Accordion 6",
                                "image": "assets/images/gallery/accordions/7607076588_593e509440_h.jpg"
                            }
                        ]
                    },
                    {
                        "id": 8,
                        "title": "Accordions",
                        "image": "assets/images/gallery/accordions/153706234_c473e8eabd_o.jpg",
                        "favorite": false,
                        "items": [
                            {
                                "id": 1,
                                "title": "Accordion 1",
                                "image": "assets/images/gallery/accordions/153706234_c473e8eabd_o.jpg"
                            },
                            {
                                "id": 2,
                                "title": "Accordion 2",
                                "image": "assets/images/gallery/accordions/2681657661_177a5edbc5_b.jpg"
                            },
                            {
                                "id": 3,
                                "title": "Accordion 3",
                                "image": "assets/images/gallery/accordions/3369363243_94fb76891d_b.jpg"
                            },
                            {
                                "id": 4,
                                "title": "Accordion 4",
                                "image": "assets/images/gallery/accordions/5316360932_719617fa1d_b.jpg"
                            },
                            {
                                "id": 5,
                                "title": "Accordion 5",
                                "image": "assets/images/gallery/accordions/6151326630_349b892d5a_b.jpg"
                            },
                            {
                                "id": 6,
                                "title": "Accordion 6",
                                "image": "assets/images/gallery/accordions/7607076588_593e509440_h.jpg"
                            }
                        ]
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            return {
                onFavorite: function (item) {
                    item.favorite = !item.favorite;
                }
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: {},
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ImageGalleryService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('imageGallery/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ImageGalleryService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__loading_service__["a" /* LoadingService */]])
    ], ImageGalleryService);
    return ImageGalleryService;
}());

//# sourceMappingURL=image-gallery-service.js.map

/***/ }),

/***/ 982:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MapsService = /** @class */ (function () {
    function MapsService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'maps'; };
        this.getTitle = function () { return 'Maps'; };
        this.getAllThemes = function () {
            return [
                { "title": "GMAPS + Location  Details", "theme": "layout1" },
                { "title": "GMAPS + About Us", "theme": "layout2" },
                { "title": "Full Screen View", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Google Maps - GMAPS + LOCATION DETAILS
        this.getDataForLayout1 = function () {
            return {
                "iconLike": "icon-thumb-up",
                "iconFavorite": "icon-heart",
                "iconShare": "icon-share-variant",
                "title": "Museum of modern art",
                "titleDescription": "Art Boulevard, New York, USA",
                "reviews": "4.12 (78 reviews)",
                "contentTitle": "In short",
                "contentDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Luctus semper elit platea; Velit aptent euismod pede euismod facilisis? In ultrices venenatis mauris. Consequat gravida pretium ligula lectus; Lacus natoque elit elit: Imperdiet cursus fermentum suspendisse; Cum iaculis venenatis!",
                "iconLoacation": "icon-map-marker-radius",
                "iconLoacationText": "Design Street, New York, USA",
                "iconWatch": "icon-alarm",
                "iconWatchText": "8:00 to 16:00 working days",
                "iconPhone": "icon-phone",
                "iconPhoneText": "333 222 111",
                "iconEarth": "icon-earth",
                "iconEarthText": "www.csform.com",
                "iconEmail": "icon-email-outline",
                "iconEmailText": "dev@csform.com",
                "iconsStars": [
                    {
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star",
                        "isActive": true
                    },
                    {
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star",
                        "isActive": true
                    },
                    {
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star",
                        "isActive": true
                    },
                    {
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star",
                        "isActive": true
                    },
                    {
                        "iconActive": "icon-star-outline",
                        "iconInactive": "icon-star",
                        "isActive": false
                    }
                ],
                "map": {
                    "lat": 40.712562,
                    "lng": -74.005911,
                    "zoom": 15,
                    "mapTypeControl": true,
                    "streetViewControl": true
                }
            };
        };
        // Set Data For Google Maps - GMAPS + ABOUT US
        this.getDataForLayout2 = function () {
            return {
                "iconLike": "icon-thumb-up",
                "iconFavorite": "icon-heart",
                "iconShare": "icon-share-variant",
                "title": "Creative Studio Form",
                "titleDescription": "Design & Development agency",
                "contentTitle": "About us",
                "contentDescription": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Luctus semper elit platea; Velit aptent euismod pede euismod facilisis? In ultrices venenatis mauris. Consequat gravida pretium ligula lectus; Lacus natoque elit elit: Imperdiet cursus fermentum suspendisse; Cum iaculis venenatis!",
                "iconLoacation": "icon-map-marker-radius",
                "iconLoacationText": "Design Street, New York, USA",
                "iconWatch": "icon-alarm",
                "iconWatchText": "8:00 to 16:00 working days",
                "iconPhone": "icon-phone",
                "iconPhoneText": "333 222 111",
                "iconEarth": "icon-earth",
                "iconEarthText": "www.csform.com",
                "iconEmail": "icon-email-outline",
                "iconEmailText": "dev@csform.com",
                "titleMap": "Here we are :",
                "map": {
                    "lat": 40.712562,
                    "lng": -74.005911,
                    "zoom": 15,
                    "mapTypeControl": true,
                    "streetViewControl": true
                }
            };
        };
        // Set Data For Google Maps - FULL SCREEN VIEW
        this.getDataForLayout3 = function () {
            return {
                "map": {
                    "lat": 40.712562,
                    "lng": -74.005911,
                    "zoom": 15,
                    "mapTypeControl": true,
                    "streetViewControl": true
                }
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onLike': function (item) {
                    that.toastCtrl.presentToast("Like");
                },
                'onFavorite': function (item) {
                    that.toastCtrl.presentToast("Favorite");
                },
                'onShare': function (item) {
                    that.toastCtrl.presentToast("Share");
                },
                'onRates': function (index) {
                    that.toastCtrl.presentToast("Rates " + (index + 1));
                }
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: {},
                theme: item.theme,
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    MapsService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('maps/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    MapsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], MapsService);
    return MapsService;
}());

//# sourceMappingURL=maps-service.js.map

/***/ }),

/***/ 983:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QRCodeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var QRCodeService = /** @class */ (function () {
    function QRCodeService(loadingService, toastCtrl) {
        var _this = this;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'qrcode'; };
        this.getTitle = function () { return 'Scanner'; };
        this.getAllThemes = function () {
            return [
                { "title": "Click here", "theme": "layout1" },
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getDataForLayout1 = function () {
            return {};
        };
        this.getDataForLayout2 = function () {
            return {};
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onFab': function (item) {
                    that.toastCtrl.presentToast("Fab");
                },
                'onScan': function (item) {
                    that.toastCtrl.presentToast(JSON.stringify(item));
                },
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: _this.getDataForTheme(item),
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    QRCodeService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        return new __WEBPACK_IMPORTED_MODULE_1_rxjs_Observable__["Observable"](function (observer) {
            that.loadingService.hide();
            observer.next(_this.getDataForTheme(item));
            observer.complete();
        });
    };
    QRCodeService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_2__toast_service__["a" /* ToastService */]])
    ], QRCodeService);
    return QRCodeService;
}());

//# sourceMappingURL=qrcode-service.js.map

/***/ }),

/***/ 984:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RadioButtonService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RadioButtonService = /** @class */ (function () {
    function RadioButtonService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'radioButton'; };
        this.getTitle = function () { return 'Radio Button'; };
        this.getAllThemes = function () {
            return [
                { "title": "Simple", "theme": "layout1" },
                { "title": "With avatars", "theme": "layout2" },
                { "title": "Simple 2", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Radio Button - SIMPLE
        this.getDataForLayout1 = function () {
            return {
                "title": "Your country",
                "selectedItem": 3,
                "items": [
                    { "id": 1, "title": "Norway" },
                    { "id": 2, "title": "Ireland" },
                    { "id": 4, "title": "Slovak Republic" },
                    { "id": 3, "title": "Canada" },
                    { "id": 5, "title": "United Kingdom" },
                    { "id": 6, "title": "Philippines" },
                    { "id": 7, "title": "Italy" },
                    { "id": 8, "title": "Brazil" },
                    { "id": 9, "title": "Russian Federation" },
                    { "id": 10, "title": "Mexico" },
                    { "id": 11, "title": "Cyprus" },
                    { "id": 12, "title": "Czech Republic" },
                    { "id": 13, "title": "Austria" }
                ]
            };
        };
        // Set Data For Radio Button - WITH AVATARS
        this.getDataForLayout2 = function () {
            return {
                "title": "Following",
                "selectedItem": 4,
                "items": [
                    { "id": 1, "title": "Grant Marshall", "subtitle": "marshall@yahoo.com", "avatar": "assets/images/avatar/0.jpg" },
                    { "id": 2, "title": "Jessica Miles", "subtitle": "miles@mail.com", "avatar": "assets/images/avatar/1.jpg" },
                    { "id": 4, "title": "Natasha Gamble", "subtitle": "gamble@outlook.com", "avatar": "assets/images/avatar/2.jpg" },
                    { "id": 3, "title": "Holman Valencia", "subtitle": "valencia@outlook.com", "avatar": "assets/images/avatar/3.jpg" },
                    { "id": 5, "title": "Prince Phelps", "subtitle": "phelps@gmail.com", "avatar": "assets/images/avatar/4.jpg" },
                    { "id": 6, "title": "Perry Bradley", "subtitle": "bradley@outlook.com", "avatar": "assets/images/avatar/5.jpg" },
                    { "id": 7, "title": "Fitzgerald Stanton", "subtitle": "stanton@mail.com", "avatar": "assets/images/avatar/6.jpg" },
                    { "id": 8, "title": "Byrd Hewitt", "subtitle": "hewitt@outlook.com", "avatar": "assets/images/avatar/7.jpg" },
                    { "id": 9, "title": "Barbara Bernard", "subtitle": "bernard@yahoo.com", "avatar": "assets/images/avatar/8.jpg" },
                    { "id": 10, "title": "Cline Lindsay", "subtitle": "lindsay@gmail.com", "avatar": "assets/images/avatar/9.jpg" }
                ]
            };
        };
        // Set Data For Radio Button - SIMPLE 2
        this.getDataForLayout3 = function () {
            return {
                "title": "Loctions",
                "selectedItem": 4,
                "items": [
                    { "id": 1, "title": "Dunbar", "subtitle": "Bangladesh" },
                    { "id": 2, "title": "Motley", "subtitle": "Kuwait" },
                    { "id": 4, "title": "Boonville", "subtitle": "Mexico" },
                    { "id": 3, "title": "Chesapeake", "subtitle": "Netherlands Antilles" },
                    { "id": 5, "title": "Sanders", "subtitle": "Liechtenstein" },
                    { "id": 6, "title": "Bath", "subtitle": "Finland" },
                    { "id": 7, "title": "Holtville", "subtitle": "Greenland" },
                    { "id": 8, "title": "Indio", "subtitle": "Brazi" },
                    { "id": 9, "title": "Cazadero", "subtitle": "United Kingdom" },
                    { "id": 10, "title": "Bridgetown", "subtitle": "Poland" },
                    { "id": 11, "title": "Fowlerville", "subtitle": "Micronesia" },
                    { "id": 12, "title": "Enlow", "subtitle": "Congo" },
                    { "id": 13, "title": "Marne", "subtitle": "Chile" }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onSelect': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    RadioButtonService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('radioButton/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    RadioButtonService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], RadioButtonService);
    return RadioButtonService;
}());

//# sourceMappingURL=radio-button-service.js.map

/***/ }),

/***/ 985:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RangeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RangeService = /** @class */ (function () {
    function RangeService(af, loadingService) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.getId = function () { return 'range'; };
        this.getTitle = function () { return 'Range'; };
        this.getAllThemes = function () {
            return [
                { "title": "All", "theme": "layout1" },
            ];
        };
        // Set Data For Range Services
        this.getDataForTheme = function (menuItem) {
            return {
                // Set Data For Range Services - BASIC
                "layout1": {
                    "title": "BASIC",
                    "value": 10
                },
                // Set Data For Range Services - WITH ICONS
                "layout2": {
                    "title": "WITH ICONS",
                    "iconLeft": "volume-mute",
                    "iconRight": "volume-up",
                    "min": "-200",
                    "max": "200",
                    "value": 0
                },
                // Set Data For Range Services - WITH PREDEFINED STEPS
                "layout3": {
                    "textLeft": "A",
                    "textRight": "A",
                    "title": "WITH PREDEFINED STEPS",
                    "min": "1000",
                    "max": "2000",
                    "step": "100",
                    "value": 20
                },
                // Set Data For Range Services - TWO SLIDERS
                "layout4": {
                    "title": "TWO SLIDERS",
                    "min": "1",
                    "max": "100",
                    "step": "10",
                    "value": {
                        "lower": 20,
                        "upper": 70
                    },
                    "textLeft": "1",
                    "textRight": "10"
                }
            };
        };
        this.getEventsForTheme = function (menuItem) {
            return {
                'onChange': function (item) { }
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    RangeService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('ranges')
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ;
    RangeService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__loading_service__["a" /* LoadingService */]])
    ], RangeService);
    return RangeService;
}());

//# sourceMappingURL=range-service.js.map

/***/ }),

/***/ 986:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToggleService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ToggleService = /** @class */ (function () {
    function ToggleService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'toggle'; };
        this.getTitle = function () { return 'Toggle'; };
        this.getAllThemes = function () {
            return [
                { "title": "With avatars", "theme": "layout1" },
                { "title": "Simple 2", "theme": "layout2" },
                { "title": "Simple", "theme": "layout3" },
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Toggle - WITH AVATARS
        this.getDataForLayout1 = function () {
            return {
                "listTitle": "With avatars",
                "items": [
                    {
                        "id": 0,
                        "title": "Graciela Mitchell",
                        "subtitle": "mitchell@mail.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/0.jpg"
                    },
                    {
                        "id": 1,
                        "title": "Sherry Hale ",
                        "subtitle": "hale@gmail.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/1.jpg"
                    },
                    {
                        "id": 2,
                        "title": "Erna Alexander",
                        "subtitle": "alexander@mail.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/2.jpg"
                    },
                    {
                        "id": 3,
                        "title": "Soto Edwards",
                        "subtitle": "edwards@outlook.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/3.jpg"
                    },
                    {
                        "id": 4,
                        "title": "Marcia Greer",
                        "subtitle": "greer@outlook.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/4.jpg"
                    },
                    {
                        "id": 5,
                        "title": "Janice Wilder",
                        "subtitle": "wilder@yahoo.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/5.jpg"
                    },
                    {
                        "id": 6,
                        "title": "Lindsey Mcgowan",
                        "subtitle": "mcgowan@mail.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/6.jpg"
                    },
                    {
                        "id": 7,
                        "title": "Lucy Bender",
                        "subtitle": "bender@yahoo.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/7.jpg"
                    },
                    {
                        "id": 8,
                        "title": "Wedi Michaeln",
                        "subtitle": "michael@gmail.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/8.jpg"
                    },
                    {
                        "id": 9,
                        "title": "Sophia Cochran",
                        "subtitle": "cochran@yahoo.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/9.jpg"
                    },
                    {
                        "id": 10,
                        "title": "Sherri Stokes",
                        "subtitle": "stokes@mail.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/10.jpg"
                    },
                    {
                        "id": 11,
                        "title": "Britney Soto",
                        "subtitle": "soto@yahoo.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/11.jpg"
                    },
                    {
                        "id": 12,
                        "title": "Lucile Mccormick",
                        "subtitle": "mccormick@outlook.com",
                        "isChecked": false,
                        "avatar": "assets/images/avatar/12.jpg"
                    }
                ]
            };
        };
        // Set Data For Toggle - SIMPLE 2
        this.getDataForLayout2 = function () {
            return {
                "listTitle": "Simple 2",
                "items": [
                    {
                        "id": 1,
                        "title": "mitchell@mail.com",
                        "subtitle": "Graciela",
                        "isChecked": true
                    },
                    {
                        "id": 2,
                        "title": "murray@yahoo.com",
                        "subtitle": "Terrell",
                        "isChecked": false
                    },
                    {
                        "id": 4,
                        "title": "hines@outlook.com",
                        "subtitle": "Branch",
                        "isChecked": false
                    },
                    {
                        "id": 3,
                        "title": "wade@outlook.com",
                        "subtitle": "Jensen",
                        "isChecked": false
                    },
                    {
                        "id": 5,
                        "title": "hale@gmail.com",
                        "subtitle": "Sherry",
                        "isChecked": false
                    },
                    {
                        "id": 6,
                        "title": "harrington@gmail.com",
                        "subtitle": "Gail",
                        "isChecked": true
                    },
                    {
                        "id": 7,
                        "title": "norman@mail.com",
                        "subtitle": "Shawna",
                        "isChecked": false
                    },
                    {
                        "id": 8,
                        "title": "alexander@mail.com",
                        "subtitle": "Erna",
                        "isChecked": false
                    },
                    {
                        "id": 9,
                        "title": "oneil@yahoo.com",
                        "subtitle": "Cohen",
                        "isChecked": false
                    }
                ]
            };
        };
        // Set Data For Toggle - SIMPLE
        this.getDataForLayout3 = function () {
            return {
                "listTitle": "Simple",
                "items": [
                    {
                        "id": 0,
                        "title": "France",
                        "isChecked": true
                    },
                    {
                        "id": 1,
                        "title": "Czech Republic",
                        "isChecked": false
                    },
                    {
                        "id": 2,
                        "title": "Andorra",
                        "isChecked": false
                    },
                    {
                        "id": 3,
                        "title": "Costa Rica",
                        "isChecked": false
                    },
                    {
                        "id": 4,
                        "title": "Trinidad and Tobago",
                        "isChecked": true
                    },
                    {
                        "id": 5,
                        "title": "St. Helena",
                        "isChecked": true
                    },
                    {
                        "id": 6,
                        "title": "Maldives",
                        "isChecked": true
                    },
                    {
                        "id": 7,
                        "title": "Tanzania",
                        "isChecked": false
                    },
                    {
                        "id": 8,
                        "title": "Philippines",
                        "isChecked": false
                    },
                    {
                        "id": 9,
                        "title": "Djibouti",
                        "isChecked": false
                    },
                    {
                        "id": 10,
                        "title": "Brunei Darussalam",
                        "isChecked": false
                    },
                    {
                        "id": 11,
                        "title": "Uzbekistan",
                        "isChecked": false
                    },
                    {
                        "id": 12,
                        "title": "Moldova",
                        "isChecked": false
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onSelect': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ToggleService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('toggle/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ToggleService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ToggleService);
    return ToggleService;
}());

//# sourceMappingURL=toggle-service.js.map

/***/ }),

/***/ 987:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SelectService = /** @class */ (function () {
    function SelectService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'select'; };
        this.getTitle = function () { return 'Select'; };
        this.getAllThemes = function () {
            return [
                { "title": "Single Select", "theme": "layout1" },
                { "title": "Multi Select", "theme": "layout2" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return {
                "title": "Select",
                "layout1": {
                    "title": "City",
                    "selectedItem": 12,
                    "header": "Basic dialog",
                    "items": [
                        {
                            "id": 12,
                            "title": "New York"
                        },
                        {
                            "id": 14,
                            "title": "Paris"
                        },
                        {
                            "id": 13,
                            "title": "Amsterdam"
                        },
                        {
                            "id": 15,
                            "title": "Gotham City"
                        }
                    ]
                },
                "layout2": {
                    "title": "Country",
                    "header": "One touch dialog",
                    "selectedItem": 22,
                    "items": [
                        {
                            "id": 22,
                            "title": "USA"
                        },
                        {
                            "id": 24,
                            "title": "France"
                        },
                        {
                            "id": 23,
                            "title": "Netherland"
                        },
                        {
                            "id": 25,
                            "title": "Gothamland"
                        }
                    ]
                },
                "layout3": {
                    "title": "Address",
                    "header": "With Action sheet",
                    "selectedItem": 31,
                    "items": [
                        {
                            "id": 31,
                            "title": "Choose address"
                        },
                        {
                            "id": 32,
                            "title": "222 Duffield Street"
                        },
                        {
                            "id": 34,
                            "title": "198 Clark Street"
                        },
                        {
                            "id": 33,
                            "title": "588 Kenmore Terrace"
                        }
                    ]
                },
                "layout4": {
                    "title": "Date",
                    "header": "Two option select",
                    "selectedItemMonth": 38,
                    "selectedItemYear": 50,
                    "itemsMonth": [
                        {
                            "id": 38,
                            "title": "January"
                        },
                        {
                            "id": 39,
                            "title": "February"
                        },
                        {
                            "id": 40,
                            "title": "March"
                        },
                        {
                            "id": 41,
                            "title": "April"
                        },
                        {
                            "id": 42,
                            "title": "May"
                        },
                        {
                            "id": 43,
                            "title": "June"
                        },
                        {
                            "id": 44,
                            "title": "July"
                        },
                        {
                            "id": 45,
                            "title": "August"
                        },
                        {
                            "id": 46,
                            "title": "September"
                        },
                        {
                            "id": 47,
                            "title": "October"
                        },
                        {
                            "id": 48,
                            "title": "November"
                        },
                        {
                            "id": 49,
                            "title": "December"
                        }
                    ],
                    "itemsYears": [
                        {
                            "id": 50,
                            "title": "2009"
                        },
                        {
                            "id": 51,
                            "title": "2010"
                        },
                        {
                            "id": 52,
                            "title": "2011"
                        },
                        {
                            "id": 53,
                            "title": "2012"
                        },
                        {
                            "id": 54,
                            "title": "2013"
                        },
                        {
                            "id": 55,
                            "title": "2014"
                        },
                        {
                            "id": 56,
                            "title": "2015"
                        },
                        {
                            "id": 57,
                            "title": "2016"
                        },
                        {
                            "id": 58,
                            "title": "2017"
                        }
                    ]
                },
                "layout5": {
                    "title": "Country",
                    "header": "One touch dialog",
                    "selectedItem": 22,
                    "items": [
                        {
                            "id": 22,
                            "title": "USA"
                        },
                        {
                            "id": 24,
                            "title": "France"
                        },
                        {
                            "id": 23,
                            "title": "Netherland"
                        },
                        {
                            "id": 25,
                            "title": "Gothamland"
                        }
                    ]
                },
                "layout6": {
                    "title": "Date",
                    "header": "Two option select",
                    "selectedItemMonth": 38,
                    "selectedItemYear": 50,
                    "itemsMonth": [
                        {
                            "id": 38,
                            "title": "January"
                        },
                        {
                            "id": 39,
                            "title": "February"
                        },
                        {
                            "id": 40,
                            "title": "March"
                        },
                        {
                            "id": 41,
                            "title": "April"
                        },
                        {
                            "id": 42,
                            "title": "May"
                        },
                        {
                            "id": 43,
                            "title": "June"
                        },
                        {
                            "id": 44,
                            "title": "July"
                        },
                        {
                            "id": 45,
                            "title": "August"
                        },
                        {
                            "id": 46,
                            "title": "September"
                        },
                        {
                            "id": 47,
                            "title": "October"
                        },
                        {
                            "id": 48,
                            "title": "November"
                        },
                        {
                            "id": 49,
                            "title": "December"
                        }
                    ],
                    "itemsYears": [
                        {
                            "id": 50,
                            "title": "2009"
                        },
                        {
                            "id": 51,
                            "title": "2010"
                        },
                        {
                            "id": 52,
                            "title": "2011"
                        },
                        {
                            "id": 53,
                            "title": "2012"
                        },
                        {
                            "id": 54,
                            "title": "2013"
                        },
                        {
                            "id": 55,
                            "title": "2014"
                        },
                        {
                            "id": 56,
                            "title": "2015"
                        },
                        {
                            "id": 57,
                            "title": "2016"
                        },
                        {
                            "id": 58,
                            "title": "2017"
                        }
                    ]
                }
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onSelect': function (item) {
                    that.toastCtrl.presentToast(JSON.stringify(item));
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    SelectService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('select')
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    SelectService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], SelectService);
    return SelectService;
}());

//# sourceMappingURL=select-service.js.map

/***/ }),

/***/ 988:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionSheetService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ActionSheetService = /** @class */ (function () {
    function ActionSheetService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'actionSheet'; };
        this.getTitle = function () { return 'Action Sheet'; };
        this.getAllThemes = function () {
            return [
                { "title": "Basic", "theme": "layout1" },
                { "title": "News", "theme": "layout2" },
                { "title": "With Text Header", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        // Set Data For Action Sheet - BASIC
        this.getDataForLayout1 = function () {
            return {
                "headerTitle": "Basic",
                "headerImage": "assets/images/background/26.jpg",
                "shareIcon": "ios-more",
                "actionSheet": {
                    "buttons": [
                        {
                            "text": "Add to Cart",
                            "role": "destructive"
                        },
                        {
                            "text": "Add to Favorites"
                        },
                        {
                            "text": "Read more info"
                        },
                        {
                            "text": "Delete Item"
                        },
                        {
                            "text": "Cancel",
                            "role": "cancel"
                        }
                    ]
                },
                "items": [
                    {
                        "id": 1,
                        "title": "dixisse isaac",
                        "subtitle": "subtilissima illud",
                        "category": "nOVUM LUDUM",
                        "button": "$14.99",
                        "description": "Comparari cum technicitate progreditur priorem quae habebat effectum in intimo ac numquam perituro & commercii societatis et socialis instrumentis, est unice delectabat, tanto celeritas adoptionis filiorum, in vitae usum deducitur.",
                        "productDescriptions": [
                            {
                                "id": 1,
                                "description": "Invenimus quod de proelio et user mos significant generatae socialis contentus in locis coniciunt in sui civitas active versantur in progressionem in products et servicia et recensionem. Notam ostendere ab advocatis de interest in in occupatus timendus fuerit productum vel servitium quam consummatio, et usus eius et co-progressionem hanc occasionem praebet ad eos, et per facultatem publice agnita quod aliqualiter pro conlatione."
                            },
                            {
                                "id": 2,
                                "description": "Labore et dolore eu consequat cupidatat commodo met enim sit ipsum ut sint duis minim. Sint aliqua pariatur duis Xercitation est ad ut. Sancti minim deserunt laborum voluptate velit esse ipsum veniam deserunt veniam proident sint non exercitationem."
                            },
                            {
                                "id": 3,
                                "description": "Lectionum ac recentius in desktop software quasi operturus Jeep libellorum ex eros etiam accumsan. Non remansit quinque saecula non tantum, sed etiam in saltu electronic condimentum, remanserant per se unum. Is eram in 1960 popularized cum remissionis de Letraset Folia accumsan."
                            },
                            {
                                "id": 4,
                                "description": "Lectionum ac recentius in desktop software quasi operturus Jeep libellorum ex eros etiam accumsan. Non remansit quinque saecula non tantum, sed etiam in saltu electronic condimentum, remanserant per se unum. Is eram in 1960 popularized cum remissionis de Letraset Folia accumsan."
                            }
                        ]
                    }
                ]
            };
        };
        // Set Data For Action Sheet - NEWS
        this.getDataForLayout2 = function () {
            return {
                "headerTitle": "News",
                "headerImage": "assets/images/background/23.jpg",
                "title": "Infinit pontem in Sinis. Quod locus non videre finem pontis. VII deambulatio inter homines pereunt.",
                "subtitle": "per marescalluml",
                "category": "certe",
                "avatar": "assets/images/avatar/3.jpg",
                "shareIcon": "ios-more",
                "actionSheet": {
                    "buttons": [
                        {
                            "text": "Add to Cart",
                            "role": "destructive"
                        },
                        {
                            "text": "Add to Favorites"
                        },
                        {
                            "text": "Read more info"
                        },
                        {
                            "text": "Delete Item"
                        },
                        {
                            "text": "Cancel",
                            "role": "cancel"
                        }
                    ]
                },
                "items": [
                    {
                        "id": 1,
                        "subtitle": "Hoc est, visu nova thriller, posuit orbem in fimbriis modern arte, quae est tenebrosa, haunting, torta - vertit se et in suo modo. Non quod objective, et arte discipulus apud Notingham, nuper Jack quae ducitur 'diluvium,' frigus, saeva enfant inexpugnabiles omnes modern art. Et vadit ad deversorium cubiculum vult bibere et cetera id exuendum statum tunc illa e somno evigilans in lecto suo."
                    },
                    {
                        "id": 2,
                        "subtitle": "Percussas pavore est, sane, sed etiam in auribus eorum amplius diluvium ad inveniendum, qui utitur maxime auctoribus imaginum cinematographicarum praebere video camera ejus vita (hoc est ars, ut videtur). Et obliti sunt ea nocte in hotel cubiculum terminus sursum in quodam loco quis gallery? Si illa ad magistratus? Et factum est, ut quod deest illi amice Jenny?"
                    },
                    {
                        "id": 3,
                        "subtitle": "It takes sursum magis, nimis longum est aedificate (usque ad paginam fere CLX) Ad validam in mia quia actio, sed altiore hoc est a dolor modern thriller, et fortiter mentis habitu femineo (adhuc inusitato scelus ficta). Jaq Hazell vigilare sit an author."
                    },
                    {
                        "id": 4,
                        "subtitle": "Vesalius habet cum ramosis in lupinotuum, conscripserit et comici librorum, sed saeva Lane videt eum quasi animam suam antiquis nota mundo, quamvis tempus suum ingenia sunt paulo plus crevit et hi qui crediderant, cum ex Novus York ad urbem est viridius, affluentes, et suburbana earum."
                    }
                ]
            };
        };
        // Set Data For Action Sheet - WITH TEXT HEADER
        this.getDataForLayout3 = function () {
            return {
                "headerTitle": "With Text Header",
                "shareIcon": "ios-more",
                "actionSheet": {
                    "title": "Choose what to do with this card?",
                    "buttons": [
                        {
                            "text": "Read more",
                            "role": "destructive"
                        },
                        {
                            "text": "Add to Favorites"
                        },
                        {
                            "text": "Delete Card"
                        },
                        {
                            "text": "Disable Card"
                        },
                        {
                            "text": "Cancel",
                            "role": "cancel"
                        }
                    ]
                },
                "items": [
                    {
                        "id": 1,
                        "category": "offer optimus",
                        "image": "assets/images/background/2.jpg",
                        "title": "Aliquam Liberum Ride",
                        "subtitle": "Dunbar, Bangladesh",
                        "button": "$35.99"
                    },
                    {
                        "id": 2,
                        "category": "res pelagus",
                        "image": "assets/images/background/1.jpg",
                        "title": "De Aeris Aperta",
                        "subtitle": "Harold, Spain",
                        "button": "$12.99"
                    },
                    {
                        "id": 3,
                        "category": "optimus unius tortae",
                        "image": "assets/images/background/0.jpg",
                        "title": "Mare turtures",
                        "subtitle": "Hilltop, France",
                        "button": "$13.45"
                    },
                    {
                        "id": 4,
                        "category": "Mons",
                        "image": "assets/images/background/3.jpg",
                        "title": "Mons Trout",
                        "subtitle": "Boonville, Mexico",
                        "button": "$38.60"
                    },
                    {
                        "id": 5,
                        "category": "Aliquam Pontem",
                        "image": "assets/images/background/4.jpg",
                        "title": "Aliquam Pontem",
                        "subtitle": "Bath, Finland",
                        "button": "$40.85"
                    },
                    {
                        "id": 6,
                        "category": "certe optimus",
                        "image": "assets/images/background/5.jpg",
                        "title": "Scaena Eventus Pelagus",
                        "subtitle": "Cazadero, United Kingdom",
                        "button": "$56.55"
                    }
                ]
            };
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
                'onItemClickActionSheet': function (item) {
                    that.toastCtrl.presentToast(item.button.text);
                },
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ActionSheetService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('actionSheet/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ActionSheetService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ActionSheetService);
    return ActionSheetService;
}());

//# sourceMappingURL=action-sheet-service.js.map

/***/ }),

/***/ 989:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeLineService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_dal_dal__ = __webpack_require__(343);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TimeLineService = /** @class */ (function () {
    function TimeLineService(af, loadingService, toastCtrl, dal) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.dal = dal;
        this.getId = function () { return 'timeline'; };
        this.getTitle = function () { return 'Payment History'; };
        this.getAllThemes = function () {
            return [
                { "title": "Payment History", "theme": "layout3" },
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                "onItemClick": function (item) {
                    that.toastCtrl.presentToast(item.title);
                }
            };
        };
        this.getDataForLayout3 = function () {
            // return {
            //     "items": [
            //         {
            //             "id": 1,
            //             "time": "TODAY AT 2:20PM",
            //             "avatar": "https://res.cloudinary.com/brainethic/image/upload/v1520613022/prasad_zlt5l2.jpg",
            //             "title": "Prasad Pawar",
            //             "subtitle": "Paid : 1200 INR",
            //             "description": "For Policy no. 912365478963"
            //         },
            //         {
            //             "id": 2,
            //             "time": "TODAY AT 1:30PM",
            //             "avatar": "assets/images/avatar/11.jpg",
            //             "title": "Bhushan Pawar",
            //             "subtitle": "Paid : 12000 INR",
            //             "description": "For Policy no. 912365478963"
            //         },
            //         {
            //             "id": 3,
            //             "time": "TODAY AT 14:20PM",
            //             "avatar": "assets/images/avatar/12.jpg",
            //             "title": "Ankita Pawar",
            //             "subtitle": "Paid : 3200 INR",
            //             "description": "For Policy no. 912365478963"
            //         },
            //         {
            //             "id": 4,
            //             "time": "TODAY AT 14:20PM",
            //             "avatar": "https://res.cloudinary.com/brainethic/image/upload/v1520613022/prasad_zlt5l2.jpg",
            //             "title": "Pranav Pawar",
            //             "subtitle": "Paid : 4200 INR",
            //             "description": "For Policy no. 912365478963"
            //         }
            //     ]
            // };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    TimeLineService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('timeline/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    TimeLineService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */], __WEBPACK_IMPORTED_MODULE_6__providers_dal_dal__["a" /* DalProvider */]])
    ], TimeLineService);
    return TimeLineService;
}());

//# sourceMappingURL=time-line-service.js.map

/***/ }),

/***/ 990:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FormService = /** @class */ (function () {
    function FormService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'form'; };
        this.getTitle = function () { return 'Form'; };
        this.getAllThemes = function () {
            return [
                { "title": "Form + Write Comment", "theme": "layout1" },
                { "title": "Form + Write Review", "theme": "layout2" },
                { "title": "Form With Address", "theme": "layout3" },
                { "title": "Form add Photo Or  Video", "theme": "layout4" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                onSubmit: function (item) {
                    that.toastCtrl.presentToast(JSON.stringify(item));
                },
                onAddVideoPhoto: function (item) {
                    that.toastCtrl.presentToast(item.description);
                }
            };
        };
        this.getDataForLayout1 = function () {
            return {
                "yourName": "Your Name",
                "title": "Title",
                "description": "Enter a description",
                "button": " Write Comment"
            };
        };
        this.getDataForLayout2 = function () {
            return {
                "title": "Continue",
                "rateTitle": "Rate",
                "descriptionPlaceholder": "Description",
                "btnSubmit": "Write Comment",
                "iconsStars": [
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": false,
                        "icon": "star"
                    }
                ],
            };
        };
        this.getDataForLayout3 = function () {
            return {
                "firstName": "Firs Name",
                "lastName": "Last Name",
                "addressLine1": "Address Line 1",
                "addressLine2": "Address Line 2",
                "city": "City",
                "zipCode": "Zip Code",
                "button": "Write Comment"
            };
        };
        this.getDataForLayout4 = function () {
            return {
                "title": "Your comment",
                "rateTitle": "Rate",
                "descriptionPlaceholder": "Description",
                "btnSubmit": "Write Comment",
                "btnAddPhotoOrVideo": "Add Photo or Video",
                "iconsStars": [
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": true,
                        "icon": "star"
                    },
                    {
                        "isActive": false,
                        "icon": "star"
                    }
                ],
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    FormService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('form/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    FormService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], FormService);
    return FormService;
}());

//# sourceMappingURL=form-service.js.map

/***/ }),

/***/ 991:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CommentService = /** @class */ (function () {
    function CommentService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'comment'; };
        this.getTitle = function () { return 'Comment'; };
        this.getAllThemes = function () {
            return [
                { "title": "Comments Basic", "theme": "layout1" },
                { "title": "Comments With Timeline", "theme": "layout2" },
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
            };
        };
        this.getDataForLayout1 = function () {
            return {
                "headerTitle": "Commnets Basic",
                "allComments": "2121 Comments",
                "items": [
                    {
                        "id": 1,
                        "image": "assets/images/avatar/16.jpg",
                        "title": "Erica Romaguera",
                        "time": "18 August 2018 at 12:20pm",
                        "description": "If you could have any kind of pet, what would you choose?"
                    },
                    {
                        "id": 2,
                        "image": "assets/images/avatar/15.jpg",
                        "title": "Caleigh Jerde",
                        "time": "18 August 2018 at 8:13pm",
                        "description": "If you could learn any language, what would you choose?"
                    },
                    {
                        "id": 3,
                        "image": "assets/images/avatar/14.jpg",
                        "title": "Lucas Schultz",
                        "time": "18 August 2018 at 5:22pm",
                        "description": "Life is about making an impact, not making an income."
                    },
                    {
                        "id": 4,
                        "image": "assets/images/avatar/13.jpg",
                        "title": "Carole Marvin",
                        "time": "18 August 2018 at 7:36am",
                        "description": "I’ve learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel"
                    },
                    {
                        "id": 5,
                        "image": "assets/images/avatar/12.jpg",
                        "title": "Doriana Feeney",
                        "time": "18 August 2018 at 5:28am",
                        "description": "Definiteness of purpose is the starting point of all achievement."
                    },
                    {
                        "id": 6,
                        "image": "assets/images/avatar/11.jpg",
                        "title": "Nia Gutkowski",
                        "time": "18 August 2018 at 11:27pm",
                        "description": "Life is what happens to you while you’re busy making other plans"
                    }
                ]
            };
        };
        this.getDataForLayout2 = function () {
            return {
                "headerTitle": "Comment With Timeline",
                "allComments": "2121 Comments",
                "items": [
                    {
                        "id": 1,
                        "image": "assets/images/avatar/16.jpg",
                        "title": "Erica Romaguera",
                        "time": "18 August 2018 at 12:20pm",
                        "description": "If you could have any kind of pet, what would you choose?"
                    },
                    {
                        "id": 2,
                        "image": "assets/images/avatar/15.jpg",
                        "title": "Caleigh Jerde",
                        "time": "18 August 2018 at 8:13pm",
                        "description": "If you could learn any language, what would you choose?"
                    },
                    {
                        "id": 3,
                        "image": "assets/images/avatar/14.jpg",
                        "title": "Lucas Schultz",
                        "time": "18 August 2018 at 5:22pm",
                        "description": "Life is about making an impact, not making an income."
                    },
                    {
                        "id": 4,
                        "image": "assets/images/avatar/13.jpg",
                        "title": "Carole Marvin",
                        "time": "18 August 2018 at 7:36am",
                        "description": "I’ve learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel"
                    },
                    {
                        "id": 5,
                        "image": "assets/images/avatar/12.jpg",
                        "title": "Doriana Feeney",
                        "time": "18 August 2018 at 5:28am",
                        "description": "Definiteness of purpose is the starting point of all achievement."
                    },
                    {
                        "id": 6,
                        "image": "assets/images/avatar/11.jpg",
                        "title": "Nia Gutkowski",
                        "time": "18 August 2018 at 11:27pm",
                        "description": "Life is what happens to you while you’re busy making other plans"
                    }
                ]
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    CommentService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('comment/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    CommentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], CommentService);
    return CommentService;
}());

//# sourceMappingURL=comment-service.js.map

/***/ }),

/***/ 992:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProfileService = /** @class */ (function () {
    function ProfileService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'profile'; };
        this.getTitle = function () { return 'Profile'; };
        this.getAllThemes = function () {
            return [
                // { "title": "Profile With Avatar", "theme": "layout1" },
                // { "title": "Profile with Slider + Comments", "theme": "layout2" },
                // { "title": "Profile Basic", "theme": "layout3" },
                // { "title": "Profile with Slider", "theme": "layout4" },
                {
                    "title": "Your Profile", "theme": "layout5"
                }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
                'onLike': function (item) {
                    if (item && item.like) {
                        if (item.like.isActive) {
                            item.like.isActive = false;
                            item.like.number--;
                        }
                        else {
                            item.like.isActive = true;
                            item.like.number++;
                        }
                    }
                },
                'onInstagram': function (item) {
                    that.toastCtrl.presentToast("onInstagram");
                },
                'onFacebook': function (item) {
                    that.toastCtrl.presentToast("onFacebook");
                },
                'onTwitter': function (item) {
                    that.toastCtrl.presentToast("onTwitter");
                },
                'onComment': function (item) {
                    if (item && item.comment) {
                        if (item.comment.isActive) {
                            item.comment.isActive = false;
                            item.comment.number--;
                        }
                        else {
                            item.comment.isActive = true;
                            item.comment.number++;
                        }
                    }
                }
            };
        };
        // getDataForLayout1 = (): any => {
        //     return {
        //         "image": "assets/images/avatar/24.jpg",
        //         "title": "Claire Stewart",
        //         "subtitle": "Extreme coffee lover. Twitter maven. Internet practitioner. Beeraholic.",
        //         "category": "populary",
        //         "items": [
        //             {
        //                 "id": 1,
        //                 "category": "Operating Systems",
        //                 "title": "Windows Server 2019 remains in release limbo",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "number": "4",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 2,
        //                 "category": "Mobile",
        //                 "title": "When it comes to mobile, you pretty much have no privacy rights",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "number": "4",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 3,
        //                 "category": "Software",
        //                 "title": "PowerPoint 2016 cheat sheet",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "number": "4",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             }
        //         ]
        //     };
        // };
        // getDataForLayout2 = (): any => {
        //     return {
        //         "image": "assets/images/avatar/20.jpg",
        //         "title": "Benjamin Wilson",
        //         "subtitle": "Extreme coffee lover. Twitter maven. Internet practitioner. Beeraholic.",
        //         "category": "populary",
        //         "followers": "Followers",
        //         "valueFollowers": "439",
        //         "following": "Following",
        //         "valueFollowing": "297",
        //         "posts": "Posts",
        //         "valuePosts": "43",
        //         "items": [
        //             {
        //                 "id": 1,
        //                 "category": "Operating Systems",
        //                 "title": "Windows Server 2019 remains in release limbo",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 2,
        //                 "category": "Mobile",
        //                 "title": "When it comes to mobile, you pretty much have no privacy rights",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 3,
        //                 "category": "Software",
        //                 "title": "PowerPoint 2016 cheat sheet",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             }
        //         ]
        //     };
        // };
        // getDataForLayout3 = (): any => {
        //     return {
        //         "image": "assets/images/avatar/21.jpg",
        //         "title": "Carolyn Guerrero",
        //         "subtitle": "Extreme coffee lover. Twitter maven. Internet practitioner. Beeraholic.",
        //         "category": "populary",
        //         "followers": "Followers",
        //         "valueFollowers": "439",
        //         "following": "Following",
        //         "valueFollowing": "297",
        //         "posts": "Posts",
        //         "valuePosts": "43",
        //         "items": [
        //             {
        //                 "id": 1,
        //                 "category": "architecture news in Building",
        //                 "backgroundCard": "assets/images/background/15.jpg",
        //                 "title": "International Design Museum of China",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 2,
        //                 "category": "architecture news in Building",
        //                 "backgroundCard": "assets/images/background/21.jpg",
        //                 "title": "Just because a building looks ugly",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 3,
        //                 "category": "bridge architecture and design",
        //                 "backgroundCard": "assets/images/background/14.jpg",
        //                 "title": "world's longest sea bridge",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             }
        //         ]
        //     };
        // };
        // getDataForLayout4 = (): any => {
        //     return {
        //         "image": "https://res.cloudinary.com/brainethic/image/upload/v1520613022/prasad_zlt5l2.jpg",
        //         "title": "Prasad Pawar",
        //         "subtitle": "Extreme coffee lover. Twitter maven. Internet practitioner. Beeraholic.",
        //         "category": "populary",
        //         "followers": "Followers",
        //         "valueFollowers": "439",
        //         "following": "Following",
        //         "valueFollowing": "297",
        //         "posts": "Posts",
        //         "valuePosts": "43",
        //         "items": [
        //             {
        //                 "id": 1,
        //                 "category": "architecture news in  Building",
        //                 "backgroundCard": "assets/images/background/15.jpg",
        //                 "title": "International Design Museum of China",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 2,
        //                 "category": "architecture news in Building",
        //                 "backgroundCard": "assets/images/background/21.jpg",
        //                 "title": "Just because a building looks ugly",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             },
        //             {
        //                 "id": 3,
        //                 "category": "bridge architecture and design",
        //                 "backgroundCard": "assets/images/background/14.jpg",
        //                 "title": "World's longest sea bridge",
        //                 "like": {
        //                     "icon": "thumbs-up",
        //                     "text": "Like",
        //                     "isActive": true
        //                 },
        //                 "comment": {
        //                     "icon": "ios-chatbubbles",
        //                     "number": "4",
        //                     "text": "Comments",
        //                     "isActive": false
        //                 }
        //             }
        //         ]
        //     };
        // };
        this.getDataForLayout5 = function () {
            return {
                "headerImage": "https://res.cloudinary.com/brainethic/image/upload/v1554399051/white-background_ivlwrb.jpg",
                "image": "https://res.cloudinary.com/brainethic/image/upload/v1554399045/profileIcon_xj581d.png",
                "title": "Prasad Pawar",
                "subtitle": "Extreme coffee lover. Twitter maven. Internet practitioner. Beeraholic.",
                "category": "populary",
                "followers": "9165478963",
                "valueFollowers": "Policy No",
                "following": "5 Lk",
                "valueFollowing": "S.A.",
                "posts": "23/04/2023",
                "valuePosts": "D.O.M.",
                "iconFacebook": "logo-facebook",
                "iconTwitter": "logo-twitter",
                "iconInstagram": "logo-instagram",
                "items": [
                    {
                        "id": 1,
                        "iconPhone": "ios-phone-portrait",
                        "iconMail": "mail-open",
                        "iconGlobe": "globe",
                        "phone": "8625845488",
                        "mail": "prasad.k.pawar@gmail.com",
                        "globe": "prasad.k.pawar.com",
                        "content": "Content",
                        "subtitle": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                        "title": "About",
                    }
                ]
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    ProfileService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('profile/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ProfileService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], ProfileService);
    return ProfileService;
}());

//# sourceMappingURL=profile-service.js.map

/***/ }),

/***/ 993:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PaymentService = /** @class */ (function () {
    function PaymentService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'payment'; };
        this.getTitle = function () { return 'Payment'; };
        this.getAllThemes = function () {
            return [
                { "title": "Payment", "theme": "layout1" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                onPay: function (item) {
                    that.toastCtrl.presentToast(JSON.stringify(item));
                }
            };
        };
        this.getDataForLayout1 = function () {
            return {
                "title": "Payment",
                "images": "assets/images/background/32.jpg",
                "cardNumber": "Card Number",
                "cardHolder": "User Name",
                "amount": "Amount",
                "experienceDate": "Policy Number",
                "code": "CVC Code",
                "button": "Continue"
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    PaymentService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('payment/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    PaymentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], PaymentService);
    return PaymentService;
}());

//# sourceMappingURL=payment-service.js.map

/***/ }),

/***/ 994:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SegmentService = /** @class */ (function () {
    function SegmentService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'segment'; };
        this.getTitle = function () { return 'Segment'; };
        this.getAllThemes = function () {
            return [
                { "title": "Segment List", "theme": "layout1" },
                { "title": "Segment Card", "theme": "layout2" },
                { "title": "Segment Post", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                'onButton': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
                'onItemClick': function (item) {
                    that.toastCtrl.presentToast(item.title);
                },
                'onLike': function (item) {
                    if (item && item.like) {
                        if (item.like.isActive) {
                            item.like.isActive = false;
                            item.like.number--;
                        }
                        else {
                            item.like.isActive = true;
                            item.like.number++;
                        }
                    }
                },
                'onComment': function (item) {
                    if (item && item.comment) {
                        if (item.comment.isActive) {
                            item.comment.isActive = false;
                            item.comment.number--;
                        }
                        else {
                            item.comment.isActive = true;
                            item.comment.number++;
                        }
                    }
                }
            };
        };
        this.getDataForLayout1 = function () {
            return {
                "headerTitle": "Segment List",
                "segmentButton1": "new product",
                "segmentButton2": "sale",
                // Data Page 1
                "page1": {
                    "background": "assets/images/background/9.jpg",
                    "items": [
                        {
                            "id": 1,
                            "image": "assets/images/avatar/17.jpg",
                            "title": "Black Shirt",
                            "category": "new product",
                            "price": "$1.99",
                        },
                        {
                            "id": 2,
                            "image": "assets/images/avatar/18.jpg",
                            "title": "Black Sweater",
                            "category": "new product",
                            "price": "$3.99",
                        },
                        {
                            "id": 3,
                            "image": "assets/images/avatar/19.jpg",
                            "title": "Shirt",
                            "category": "new product",
                            "price": "$3.99",
                        },
                        {
                            "id": 4,
                            "image": "assets/images/avatar/20.jpg",
                            "title": "White Shirt",
                            "category": "new product",
                            "comments": "512",
                            "price": "$3.99",
                        },
                        {
                            "id": 5,
                            "image": "assets/images/avatar/21.jpg",
                            "title": "White T shirt",
                            "category": "new product",
                            "price": "$3.99",
                        }
                    ]
                },
                // Data Page 2
                "page2": {
                    "background": "assets/images/background/8.jpg",
                    "items": [
                        {
                            "id": 1,
                            "image": "assets/images/avatar/22.jpg",
                            "title": "T shirt",
                            "category": "sale",
                            "price": "$1.99",
                        },
                        {
                            "id": 2,
                            "image": "assets/images/avatar/23.jpg",
                            "title": "Hoodies",
                            "category": "sale",
                            "price": "$3.99",
                        },
                        {
                            "id": 3,
                            "image": "assets/images/avatar/19.jpg",
                            "title": "Black Shirt",
                            "category": "sale",
                            "price": "$3.99",
                        },
                        {
                            "id": 4,
                            "image": "assets/images/avatar/18.jpg",
                            "title": " White T shirt",
                            "category": "sale",
                            "comments": "512",
                            "price": "$3.99",
                        },
                        {
                            "id": 5,
                            "image": "assets/images/avatar/17.jpg",
                            "title": "Shirt",
                            "category": "sale",
                            "price": "$3.99",
                        }
                    ]
                }
            };
        };
        this.getDataForLayout2 = function () {
            return {
                "headerTitle": "Segment Card",
                "segmentButton1": "new news",
                "segmentButton2": "old news",
                // Data Page 1
                "page1": {
                    "items": [
                        {
                            "id": 1,
                            "image": "assets/images/background/1.jpg",
                            "title": "Easy Carrot Cake",
                            "category": "Recipes",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 2,
                            "image": "assets/images/background/2.jpg",
                            "title": "Lake Ladoga",
                            "category": "Summer",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 3,
                            "image": "assets/images/background/3.jpg",
                            "title": "Vasco da Gama Bridge",
                            "category": "Architecture",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 4,
                            "image": "assets/images/background/4.jpg",
                            "title": "Cactus Flowers",
                            "category": "Flowerss",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 5,
                            "image": "assets/images/background/5.jpg",
                            "title": "Stetsonia coryne with flower",
                            "category": "Flowerss",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        }
                    ]
                },
                // Data Page 2
                "page2": {
                    "items": [
                        {
                            "id": 1,
                            "image": "assets/images/background/6.jpg",
                            "title": "Trichocereus lamprochlorus",
                            "category": "Flowerss",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 2,
                            "image": "assets/images/background/7.jpg",
                            "title": "Opuntia phaeacantha leaves",
                            "category": "Flowerss",
                            "description": "ALorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 3,
                            "image": "assets/images/background/8.jpg",
                            "title": "Cactus Spines",
                            "category": "Flowerss",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 4,
                            "image": "assets/images/background/2.jpg",
                            "title": "Lake Ladoga",
                            "category": "Summer",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        },
                        {
                            "id": 5,
                            "image": "assets/images/background/1.jpg",
                            "title": "Carrot Cake",
                            "category": "Recipes",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry...",
                        }
                    ]
                }
            };
        };
        this.getDataForLayout3 = function () {
            return {
                "headerTitle": "Segment Post",
                "segmentButton1": "new offer",
                "segmentButton2": "best offer",
                // Data Page 1
                "page1": {
                    "items": [
                        {
                            "id": 1,
                            "image": "assets/images/background/1.jpg",
                            "time": "25 January 2018",
                            "title": "Easy Carrot Cake",
                            "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 2,
                            "image": "assets/images/background/2.jpg",
                            "time": "03 May 2018",
                            "title": "Lake Ladoga",
                            "description": "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 3,
                            "image": "assets/images/background/3.jpg",
                            "time": "30 July 2018",
                            "title": "Vasco da Gama Bridge",
                            "description": "It is a long established fact that a reader will be distracted by the readable content",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 4,
                            "image": "assets/images/background/4.jpg",
                            "time": "28 April 2018",
                            "title": "Cactus Flowers",
                            "description": "There are many variations of passages of Lorem Ipsum available, but the majority",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 5,
                            "image": "assets/images/background/5.jpg",
                            "time": "15 November 2017",
                            "title": "Stetsonia coryne with flower",
                            "description": "If you are going to use a passage of Lorem Ipsum, you need to be sure there",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        }
                    ]
                },
                // Data Page 2
                "page2": {
                    "items": [
                        {
                            "id": 1,
                            "image": "assets/images/background/6.jpg",
                            "time": "09 May 2018",
                            "title": "Trichocereus lamprochlorus",
                            "description": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 2,
                            "image": "assets/images/background/7.jpg",
                            "time": "08 July 2018",
                            "title": "Opuntia phaeacantha leaves",
                            "description": "Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 3,
                            "image": "assets/images/background/8.jpg",
                            "time": "11 September 2018",
                            "title": "Cactus Spines",
                            "description": "The standard chunk of Lorem Ipsum used since the 1500s is reproduced below",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 4,
                            "image": "assets/images/background/2.jpg",
                            "time": "23 July 2018",
                            "title": "Lake Ladoga",
                            "description": "Lorem Ipsum is therefore always free from repetition, injected humour.",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        },
                        {
                            "id": 5,
                            "image": "assets/images/background/1.jpg",
                            "time": "05 June 2018",
                            "title": "Carrot Cake",
                            "description": "Lorem Ipsum as their default model text, and a search for 'lorem ipsum'",
                            "like": {
                                "icon": "thumbs-up",
                                "number": "12",
                                "isActive": false
                            },
                            "comment": {
                                "icon": "ios-chatbubbles",
                                "number": "4",
                                "isActive": false
                            }
                        }
                    ]
                }
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    SegmentService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('segment/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    SegmentService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], SegmentService);
    return SegmentService;
}());

//# sourceMappingURL=segment-service.js.map

/***/ }),

/***/ 995:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AlertService = /** @class */ (function () {
    function AlertService(af, loadingService, toastCtrl) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.toastCtrl = toastCtrl;
        this.getId = function () { return 'alert'; };
        this.getTitle = function () { return 'Alert'; };
        this.getAllThemes = function () {
            return [
                // { "title": "Alert Info", "theme": "layout1" },
                // { "title": "Alert Warning", "theme": "layout2" },
                { "title": "Alert Subscribe", "theme": "layout3" }
            ];
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.theme.charAt(0).toUpperCase() +
                menuItem.theme.slice(1)]();
        };
        this.getEventsForTheme = function (menuItem) {
            var that = _this;
            return {
                onButton: function (item) {
                    that.toastCtrl.presentToast(item.title);
                }
            };
        };
        this.getDataForLayout1 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "image": "assets/images/background/4.jpg",
                        "title": "Trichocereus lamprochlorus",
                        "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                    },
                    {
                        "id": 2,
                        "image": "assets/images/background/8.jpg",
                        "title": "Opuntia phaeacantha leaves",
                        "description": " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."
                    },
                    {
                        "id": 3,
                        "image": "assets/images/background/7.jpg",
                        "title": "Cactus Spines",
                        "description": "Many desktop publishing packages and web page editors now use Lorem Ipsum. "
                    },
                    {
                        "id": 4,
                        "image": "assets/images/background/6.jpg",
                        "title": "Stetsonia coryne with flower",
                        "description": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots."
                    },
                    {
                        "id": 4,
                        "image": "assets/images/background/5.jpg",
                        "title": "Cactus",
                        "description": "All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks."
                    }
                ]
            };
        };
        this.getDataForLayout2 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "category": "architecture",
                        "image": "assets/images/background/14.jpg",
                        "title": "First World Hotel",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 2,
                        "category": "architecture",
                        "image": "assets/images/background/15.jpg",
                        "title": "The Venetian",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 3,
                        "category": "architecture",
                        "image": "assets/images/background/23.jpg",
                        "title": "MGM Grand Las Vegas",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 4,
                        "category": "architecture",
                        "image": "assets/images/background/21.jpg",
                        "title": "Izmailovo Hotel",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 5,
                        "category": "architecture",
                        "image": "assets/images/background/9.jpg",
                        "title": "Encore Las Vegas",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 6,
                        "category": "architecture",
                        "image": "assets/images/background/13.jpg",
                        "title": "Flamingo Las Vegas",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    }
                ]
            };
        };
        this.getDataForLayout3 = function () {
            return {
                "items": [
                    {
                        "id": 1,
                        "image": "assets/images/background/17.jpg",
                        "time": "MARCH 14, 2017",
                        "title": "Swivel chair",
                        "description": "You sit comfortably since the chair is adjustable in height.",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 2,
                        "image": "assets/images/background/20.jpg",
                        "time": "MARCH 14, 2017",
                        "title": "Table top",
                        "description": "Pre-drilled holes for legs, for easy assembly.",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 3,
                        "image": "assets/images/background/15.jpg",
                        "time": "MARCH 14, 2017",
                        "title": "Office desks",
                        "description": "It’s easy to keep your desk neat.",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 4,
                        "image": "assets/images/background/23.jpg",
                        "time": "MARCH 14, 2017",
                        "title": "Computer desks",
                        "description": "The legs can be mounted to the right or left.",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 5,
                        "image": "assets/images/background/11.jpg",
                        "time": "MARCH 14, 2017",
                        "title": "Work lamps",
                        "description": "The lamp is lightweight and easy to move anywhere.",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    },
                    {
                        "id": 6,
                        "image": "assets/images/background/14.jpg",
                        "time": "MARCH 14, 2017",
                        "title": "Bookcases",
                        "description": "A simple unit can be enough storage for a limited space",
                        "iconLike": "thumbs-up",
                        "iconComment": "ios-chatbubbles",
                        "numberLike": "12",
                        "numberCommnet": "4",
                    }
                ]
            };
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
    }
    AlertService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('alert/' + item.theme)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_5__loading_service__["a" /* LoadingService */], __WEBPACK_IMPORTED_MODULE_4__toast_service__["a" /* ToastService */]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert-service.js.map

/***/ })

});
//# sourceMappingURL=2.js.map