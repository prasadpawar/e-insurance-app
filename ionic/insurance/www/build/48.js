webpackJsonp([48],{

/***/ 764:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TabPage3Module", function() { return TabPage3Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tab_page_3__ = __webpack_require__(998);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TabPage3Module = /** @class */ (function () {
    function TabPage3Module() {
    }
    TabPage3Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tab_page_3__["a" /* TabPage3 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tab_page_3__["a" /* TabPage3 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__tab_page_3__["a" /* TabPage3 */]
            ]
        })
    ], TabPage3Module);
    return TabPage3Module;
}());

//# sourceMappingURL=tab-page-3.module.js.map

/***/ }),

/***/ 785:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__loading_service__ = __webpack_require__(196);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsService = /** @class */ (function () {
    function TabsService(af, loadingService) {
        var _this = this;
        this.af = af;
        this.loadingService = loadingService;
        this.getId = function () { return 'tabs'; };
        this.getTitle = function () { return 'Tabs'; };
        this.getAllThemes = function () {
            return [
                { "title": "Footer tab - text", "theme": "layout1" },
                { "title": "Footer tab - icons", "theme": "layout2" },
                { "title": "Header tab - text", "theme": "layout3" }
            ];
        };
        this.getEventsForTheme = function (menuItem) {
            return {};
        };
        this.getDataForTheme = function (menuItem) {
            return _this['getDataFor' +
                menuItem.charAt(0).toUpperCase() +
                menuItem.slice(1)]();
        };
        // Set Data For Tabs - PAGE 1
        this.getDataForTab1 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/1.jpg",
                "description": "Sed satis est ut probe exercuisse. Experiri problems est ut minae ultra solitum, sed re vera non excusat, quia in illis agit pessimus modo fieri potest.",
                "title": "Census Introduction"
            };
        };
        // Set Data For Tabs - PAGE 2
        this.getDataForTab2 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/2.jpg",
                "description": "In secunda parte, constitue 'est maxime alumni. Qui non scit, cum non communicent cum homine lingua Latina igitur difficultatem se sentire. Et ante facere damnationem mens eorum: tunc loquetur. Haec est secunda pars dicere cum fiducia et sine ope facere periculum faciendi animo sententia.",
                "title": "Majoribus tuis Census"
            };
        };
        // Set Data For Tabs - PAGE 3
        this.getDataForTab3 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/3.jpg",
                "description": "Tertia Vade est provectae doctrinam. Si primo didicit duas partes, et tertiam partem hæc faciam vobis: scientes quod de Anglis. ",
                "title": "Vos may reperio scissa est impar, aut damnum"
            };
        };
        // Set Data For Tabs - PAGE 4
        this.getDataForTab4 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/4.jpg",
                "description": "Tibi poterit, de qua fiducia et assistent coadunationibus a-membra loqui linguis difficile. Cras interdum sollicitudin ante facile legitur vasa quibus difficile verba.",
                "title": "Ordo est ut media Teresiae"
            };
        };
        // Set Data For Tabs - PAGE 5
        this.getDataForTab5 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/5.jpg",
                "description": "In finem, non possumus dicere non debemus quod ab Anglis liber PDF Guru ad alumni. Vos can adepto a liberum web oratio infra per exemplum a visitare. Omnes tres partes in unum zip lima.",
                "title": "Procurator sine dixeritis visionem"
            };
        };
        // Set Data For Tabs - PAGE 6
        this.getDataForTab6 = function () {
            return {
                "backgroundImage": "assets/images/avatar-large/6.jpg",
                "description": "Ut nos omnes scitis quod in Latina lingua est maxime momenti in hoc mundo. Et dicunt ei possumus est internationalis lingua communicationis. Iusta putant vel si patriam loci in quo nunc es, ad iter ad loci linguam intelligere non posset, tum in aliis linguis Latina non solum ut auxilium vobis. Ita magna est nimis discere et intelligunt linguae.",
                "title": "Carpe occasiones Brexit"
            };
        };
        this.getShowItemId = function (item) {
            return _this.getId() + item.theme.charAt(0).toUpperCase() + "" + item.theme.slice(1);
        };
        this.prepareParams = function (item) {
            var result = {
                title: item.title,
                data: [],
                events: _this.getEventsForTheme(item)
            };
            result[_this.getShowItemId(item)] = true;
            return result;
        };
    }
    TabsService.prototype.load = function (item) {
        var _this = this;
        var that = this;
        that.loadingService.show();
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('tab/' + item)
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    that.loadingService.hide();
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    that.loadingService.hide();
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                that.loadingService.hide();
                observer.next(_this.getDataForTheme(item));
                observer.complete();
            });
        }
    };
    ;
    TabsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_4__loading_service__["a" /* LoadingService */]])
    ], TabsService);
    return TabsService;
}());

//# sourceMappingURL=tabs-service.js.map

/***/ }),

/***/ 998:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabPage3; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_toast_service__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_tabs_service__ = __webpack_require__(785);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TabPage3 = /** @class */ (function () {
    function TabPage3(tabsService, toastCtrl) {
        var _this = this;
        this.tabsService = tabsService;
        this.toastCtrl = toastCtrl;
        this.params = {};
        this.tabsService.load("tab3").subscribe(function (snapshot) {
            _this.params = snapshot;
        });
    }
    TabPage3.prototype.ngOnChanges = function (changes) {
        this.params = changes['data'].currentValue;
    };
    TabPage3.prototype.onItemClick = function (item) {
        this.toastCtrl.presentToast("Folow");
    };
    TabPage3 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/tab-page-3/tab-page-3.html"*/'<!---Settings TAB lauout-3-->\n\n<ion-content tabPage3>\n\n  <div background-size [ngStyle]="{\'background-image\': \'url(\' + params.backgroundImage + \')\'}" *ngIf=" params.backgroundImage != null">\n\n    <div padding class="tab-decription">\n\n      <h2 tabs-title text-center>{{params.title}}</h2>\n\n      <p tabs-subtitle text-center>{{params.description}}</p>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/tab-page-3/tab-page-3.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_tabs_service__["a" /* TabsService */], __WEBPACK_IMPORTED_MODULE_1__services_toast_service__["a" /* ToastService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_tabs_service__["a" /* TabsService */], __WEBPACK_IMPORTED_MODULE_1__services_toast_service__["a" /* ToastService */]])
    ], TabPage3);
    return TabPage3;
}());

//# sourceMappingURL=tab-page-3.js.map

/***/ })

});
//# sourceMappingURL=48.js.map