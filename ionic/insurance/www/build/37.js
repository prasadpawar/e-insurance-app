webpackJsonp([37],{

/***/ 685:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentLayout1Module", function() { return PaymentLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__payment_layout_1__ = __webpack_require__(886);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaymentLayout1Module = /** @class */ (function () {
    function PaymentLayout1Module() {
    }
    PaymentLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__payment_layout_1__["a" /* PaymentLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__payment_layout_1__["a" /* PaymentLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__payment_layout_1__["a" /* PaymentLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], PaymentLayout1Module);
    return PaymentLayout1Module;
}());

//# sourceMappingURL=payment-layout-1.module.js.map

/***/ }),

/***/ 773:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_home_service__ = __webpack_require__(774);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_app_settings__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.service = service;
        this.data = {};
        this.isBuyButtonEnabled = false;
        service.load().subscribe(function (snapshot) {
            _this.data = snapshot;
        });
        this.isBuyButtonEnabled = __WEBPACK_IMPORTED_MODULE_3__services_app_settings__["a" /* AppSettings */].BUY_BUTTON;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/home/home.html"*/'<!--Fist Screen-->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n            <ion-icon class="icon-menu" name="menu"></ion-icon>\n\n        </button>\n\n        <div buy *ngIf="isBuyButtonEnabled">\n\n            <ion-title>{{data.toolbarTitle}}</ion-title>\n\n            <button ion-button defoult-button\n\n                onclick="window.open(\'https://codecanyon.net/item/ionic-3-ui-themetemplate-app-material-design-blue-light/20234423?ref=CreativeForm\')">\n\n                BUY NOW\n\n            </button>\n\n        </div>\n\n        <!---Title-->\n\n        <ion-title *ngIf="!isBuyButtonEnabled">{{data.toolbarTitle}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n\n\n<!--Fist Screen Content-->\n\n<ion-content>\n\n    <div *ngIf="data.background!=null" background-size default-background\n\n        [ngStyle]="{\'background-image\': \'url(\' + data.background + \')\'}">\n\n        <div class="ionic-description" text-center>\n\n            <h2 item-title>{{data.title}}</h2>\n\n            <h2 item-title>{{data.subtitle}}</h2>\n\n            <p item-subtitle>{{data.subtitle2}}</p>\n\n        </div>\n\n        <!-- <a [href]="data.link">\n\n            {{data.description}}\n\n        </a> -->\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__services_home_service__["a" /* HomeService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_home_service__["a" /* HomeService */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 774:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_angularfire2_database__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_settings__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomeService = /** @class */ (function () {
    function HomeService(af) {
        this.af = af;
        // Set data for - HOME PAGE
        this.getData = function () {
            return {
                "toolbarTitle": "Welcome to E-Insurance App",
                "title": "Online Insurance App",
                "subtitle": "For Mobile",
                "subtitle2": "Insure And Be Secure !!",
                "background": "https://res.cloudinary.com/brainethic/image/upload/v1546862799/WhatsApp_Image_2019-01-07_at_5.33.29_PM_fouj6p.jpg"
            };
        };
    }
    HomeService.prototype.load = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_3__app_settings__["a" /* AppSettings */].IS_FIREBASE_ENABLED) {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                _this.af
                    .object('home')
                    .valueChanges()
                    .subscribe(function (snapshot) {
                    observer.next(snapshot);
                    observer.complete();
                }, function (err) {
                    observer.error([]);
                    observer.complete();
                });
            });
        }
        else {
            return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
                observer.next(_this.getData());
                observer.complete();
            });
        }
    };
    HomeService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], HomeService);
    return HomeService;
}());

//# sourceMappingURL=home-service.js.map

/***/ }),

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dal_dal__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_valid_valid__ = __webpack_require__(344);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(773);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PaymentLayout1 = /** @class */ (function () {
    function PaymentLayout1(dal, validator, navCtrl) {
        this.dal = dal;
        this.validator = validator;
        this.navCtrl = navCtrl;
        this.paymentReq = {
            userId: "",
            cardNo: "",
            cvv: "",
            expiryDate: new Date(),
            amount: "",
            policyNo: ""
        };
    }
    PaymentLayout1.prototype.paymentVal = function () {
        this.paymentStatus = null;
        if (this.validator.emptyVal(this.paymentReq.userId)) {
            this.paymentStatus = "Please Enter User Name !!";
            console.log("Please Enter User Name !!");
        }
        else {
            if (this.validator.emptyVal(this.paymentReq.cardNo)) {
                this.paymentStatus = "Please Enter card no !!";
                console.log("Please Enter card no !!");
            }
            else if (this.validator.emptyVal(this.paymentReq.policyNo)) {
                this.paymentStatus = "Please Enter Policy Number !!";
                console.log("Please Enter Policy Number !!");
            }
            else if (this.validator.emptyVal(this.paymentReq.amount)) {
                this.paymentStatus = "Please Enter Amount !!";
                console.log("Please Enter Amount !!");
            }
            else if (this.validator.emptyVal(this.paymentReq.cvv)) {
                this.paymentStatus = "Please Enter CVV !!";
                console.log("Please Enter CVV !!");
            }
        }
        return this.paymentStatus;
    };
    PaymentLayout1.prototype.userPayment = function () {
        var _this = this;
        // console.log(this.validation.emptyVal(this.loginReq.userId), "this is validation service")
        if (null != this.paymentVal()) {
            return;
        }
        this.dal.userPayment(this.paymentReq).subscribe(function (x) {
            console.log(x);
            if (x.amount) {
                console.log("Payment Successful !!");
                _this.paymentStatus = "Payment Successful !!";
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                // window.location.href = `/${environment.baseHref}/#/dashboard`
            }
            else {
                console.log("Payment Failed !!");
                _this.paymentStatus = "Payment Failed !!";
                // window.location.href = `/${environment.baseHref}/#/`
                // Swal.fire('Oops...', 'Username or Password entered is wrong!', 'error')
            }
        }, function (e) { });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], PaymentLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], PaymentLayout1.prototype, "events", void 0);
    PaymentLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'payment-layout-1',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/payment/layout-1/payment.html"*/'<!--Theme Payment - Payment -->\n\n<ion-header>\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <div *ngIf="data != null">\n\n      <ion-title>{{data.title}}</ion-title>\n\n    </div>\n\n  </ion-navbar>\n\n</ion-header>\n\n<!-- Content -->\n\n<ion-content>\n\n  <ion-grid padding>\n\n    <ion-row padding *ngIf="data != null">\n\n      <ion-col col-12>\n\n        <img [src]="data.images" alt="">\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <ion-item margin-top no-lines box-shadow>\n\n          <ion-input no-margin type="text" [placeholder]="data.cardHolder" [(ngModel)]="paymentReq.userId">\n\n          </ion-input>\n\n        </ion-item>\n\n        <ion-item margin-top no-lines box-shadow>\n\n          <ion-input no-margin type="text" [placeholder]="data.cardNumber" [(ngModel)]="paymentReq.cardNo">\n\n          </ion-input>\n\n        </ion-item>\n\n        <ion-item margin-top no-lines box-shadow>\n\n          <ion-input type="text" [placeholder]="data.experienceDate" [(ngModel)]="paymentReq.policyNo">\n\n          </ion-input>\n\n        </ion-item>\n\n        <ion-item margin-top no-lines box-shadow>\n\n          <ion-input no-margin type="text" [placeholder]="data.amount" [(ngModel)]="paymentReq.amount"></ion-input>\n\n        </ion-item>\n\n        <ion-item margin-top no-lines box-shadow>\n\n          <ion-input no-margin type="text" [placeholder]="data.code" [(ngModel)]="paymentReq.cvv"></ion-input>\n\n        </ion-item>\n\n      </ion-col>\n\n      <ion-col col-12 align-self-end>\n\n        <button ion-button button-clear-outline round outline block text-capitalizen *ngIf="data != null"\n\n          (click)="userPayment()">{{data.button}}</button>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/payment/layout-1/payment.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_dal_dal__["a" /* DalProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_valid_valid__["a" /* ValidProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */]])
    ], PaymentLayout1);
    return PaymentLayout1;
}());

//# sourceMappingURL=payment-layout-1.js.map

/***/ })

});
//# sourceMappingURL=37.js.map