webpackJsonp([90],{

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfileLayout5Module", function() { return ProfileLayout5Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile_layout_5__ = __webpack_require__(891);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfileLayout5Module = /** @class */ (function () {
    function ProfileLayout5Module() {
    }
    ProfileLayout5Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_5__["a" /* ProfileLayout5 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile_layout_5__["a" /* ProfileLayout5 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__profile_layout_5__["a" /* ProfileLayout5 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ProfileLayout5Module);
    return ProfileLayout5Module;
}());

//# sourceMappingURL=profile-layout-5.module.js.map

/***/ }),

/***/ 891:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileLayout5; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_dal_dal__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_fingerprint_aio_ngx__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_settersandgetters_settersandgetters__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utility_utility__ = __webpack_require__(345);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_android_fingerprint_auth__ = __webpack_require__(349);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ProfileLayout5 = /** @class */ (function () {
    function ProfileLayout5(androidFingerprintAuth, alertCtrl, dal, navCtrl, navParams, utility, faio, setAndGet) {
        this.androidFingerprintAuth = androidFingerprintAuth;
        this.alertCtrl = alertCtrl;
        this.dal = dal;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.utility = utility;
        this.faio = faio;
        this.setAndGet = setAndGet;
        this.slider = {};
        this.otpValReq = {
            otp: "",
            userId: "",
            mobile: "",
            email: ""
        };
    }
    ProfileLayout5.prototype.ngOnInit = function () {
        // this.getCustomerList();
        this.getBackupData();
        this.txt = "Please enter your OTP";
    };
    ProfileLayout5.prototype.otpPrompt = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.txt,
            message: 'Enter OTP sent to your registered mobile or email !!',
            inputs: [
                {
                    name: 'otp',
                    placeholder: 'Enter OTP here'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) {
                    }
                },
                {
                    text: 'validate',
                    handler: function (data) {
                        _this.otpValReq.otp = data.otp;
                        console.log("otp prompt>>>>", data);
                        _this.otpAuth();
                    }
                }
            ]
        });
        alert.present();
    };
    ProfileLayout5.prototype.getBackupData = function () {
        var _this = this;
        this.dal.getCustomer({ name: this.dal.getUserId() }).subscribe(function (x) {
            _this.customerBackup = x[0];
            console.log("backup data>>>>", _this.customerBackup);
            _this.otpValReq.userId = _this.customerBackup.name;
            _this.otpValReq.mobile = _this.customerBackup.mobile;
            _this.custLocation = _this.customerBackup.location;
            _this.otpValReq.email = _this.customerBackup.email;
        });
    };
    ProfileLayout5.prototype.getCustomerList = function () {
        var _this = this;
        this.dal.getCustomer({ name: this.dal.getUserId() }).subscribe(function (y) {
            _this.customers = y[0];
            console.log("customer list >>>>>", _this.customers);
        });
    };
    ProfileLayout5.prototype.getOtp = function () {
        var _this = this;
        this.dal.requestOtp(this.otpValReq).subscribe(function (z) {
            // this.utility.presentAlert("otp received to your registered mobile and email");
            console.log("otp received>>>>>>>>", z);
            _this.otpPrompt();
        });
    };
    ProfileLayout5.prototype.otpAuth = function () {
        var _this = this;
        this.dal.validateOtp(this.otpValReq).subscribe(function (p) {
            if (p.success == true) {
                _this.getCustomerList();
                // console.log("after true OTP>>>>>>>>>>", this.customers);
                _this.utility.presentAlert("OTP validated and Data is Retrieved successfully !!");
            }
            else {
                _this.txt = "OTP Failed to validate !! please try again..";
                _this.otpPrompt();
                // this.otpAuth()
                return;
            }
        });
    };
    ProfileLayout5.prototype.bioAuth = function () {
        var _this = this;
        console.log("faio says>>>>>>>", this.androidFingerprintAuth);
        this.androidFingerprintAuth.isAvailable()
            .then(function (result) {
            if (result.isAvailable) {
                // it is available
                _this.androidFingerprintAuth.encrypt({ clientId: _this.otpValReq.userId, username: _this.otpValReq.userId, password: _this.otpValReq.userId })
                    .then(function (result) {
                    if (result.withFingerprint) {
                        // this.utility.presentAlert("Successfully Retrived Data.")
                        // this.utility.presentAlert(result);
                        _this.getOtp();
                        console.log('Successfully encrypted credentials.');
                        console.log('Encrypted credentials: ' + result.token);
                    }
                    else
                        _this.utility.presentAlert("Didn\'t authenticate!");
                })
                    .catch(function (error) {
                    if (error === _this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
                        console.log('Fingerprint authentication cancelled');
                        _this.utility.presentAlert("Fingerprint authentication cancelled");
                    }
                    else
                        console.error(error);
                });
            }
            else {
                // fingerprint auth isn't available
            }
        })
            .catch(function (error) {
            console.error(error);
            _this.utility.presentAlert(error);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout5.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], ProfileLayout5.prototype, "events", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ProfileLayout5.prototype, "content", void 0);
    ProfileLayout5 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'profile-layout-5',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-5/profile.html"*/'<!--Profile- Profile 1-->\n\n<ion-content>\n\n  <ion-grid *ngIf="data != null">\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-card transparent text-center>\n\n          <ion-item box-shadow transparent text-center *ngIf="data != null" background-size\n\n            [ngStyle]="{\'background-image\': \'url(\' + data.headerImage + \')\'}">\n\n            <ion-avatar>\n\n              <img [src]="data.image" alt="">\n\n            </ion-avatar>\n\n            <h1 item-title>{{otpValReq.userId}}</h1>\n\n            <p text-wrap item-title>{{custLocation}}</p>\n\n          </ion-item>\n\n          <ion-row no-padding margin-top>\n\n            <!-- <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valueFollowers}}</span>\n\n              <span ion-text text-caprasad@iamrisingstar:~/data/projects/be-project/e-insurance-app$ pitalize color="accent">{{data.followers}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valueFollowing}}</span>\n\n              <span ion-text text-capitalize color="accent">{{data.following}}</span>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col>\n\n            <button ion-button button-clear clear no-padding>\n\n              <span small-font font-bold ion-text color="accent">{{data.valuePosts}}</span>\n\n              <span ion-text text-capitalize color="accent">{{data.posts}}</span>\n\n            </button>\n\n          </ion-col> -->\n\n            <!-- Social Button-->\n\n            <ion-col col-12 text-center>\n\n              <button ion-button button-icon icon-only button-icon (click)="bioAuth()">\n\n                <ion-icon icon-small name="md-archive"></ion-icon>\n\n              </button>\n\n\n\n              <!--<button ion-button icon-only button-icon (click)="onEvent(\'onTwitter\', item, $event)">\n\n                <ion-icon icon-small name="{{data.iconTwitter}}"></ion-icon>\n\n              </button> -->\n\n\n\n              <!-- <button ion-button icon-only button-icon (click)="onEvent(\'onInstagram\', item, $event)">\n\n                <ion-icon icon-small name="{{data.iconInstagram}}"></ion-icon>\n\n              </button> -->\n\n\n\n            </ion-col>\n\n            <!-- Info Author-->\n\n            <div *ngIf="customers != null">\n\n              <ion-col col-12 text-left margin-top box-shadow>\n\n                <ion-item transparent>\n\n                  <h1 subitem-title ion-text>Private Data</h1>\n\n                  <p subitem-subtitle padding-top text-wrap>This data is secured with biometric and otp !!</p>\n\n\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Policy No :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.policyNo}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Sum Assurred :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.sumAssured}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Plan :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.plan}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Policy Term :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.term}} years</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Mobile No. :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.mobile}}</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Issued By :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.location}} Division</p>\n\n                </ion-item>\n\n                <ion-item no-lines transparent>\n\n                  <h1 subitem-title ion-text><b>Purpose :</b></h1>\n\n                  <ion-icon icon-medium name="md-archive" item-start></ion-icon>\n\n                  <p no-margin subitem-title>{{customers.purpose}}</p>\n\n                </ion-item>\n\n\n\n              </ion-col>\n\n            </div>\n\n\n\n          </ion-row>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/profile/layout-5/profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__ionic_native_android_fingerprint_auth__["a" /* AndroidFingerprintAuth */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_dal_dal__["a" /* DalProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */], __WEBPACK_IMPORTED_MODULE_5__providers_utility_utility__["a" /* UtilityProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_fingerprint_aio_ngx__["a" /* FingerprintAIO */], __WEBPACK_IMPORTED_MODULE_4__providers_settersandgetters_settersandgetters__["a" /* SettersandgettersProvider */]])
    ], ProfileLayout5);
    return ProfileLayout5;
}());

//# sourceMappingURL=profile-layout-5.js.map

/***/ })

});
//# sourceMappingURL=90.js.map