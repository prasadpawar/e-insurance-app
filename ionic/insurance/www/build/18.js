webpackJsonp([18,76,77,78],{

/***/ 703:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegmentLayout1Module", function() { return SegmentLayout1Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__segment_layout_1__ = __webpack_require__(904);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SegmentLayout1Module = /** @class */ (function () {
    function SegmentLayout1Module() {
    }
    SegmentLayout1Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__segment_layout_1__["a" /* SegmentLayout1 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__segment_layout_1__["a" /* SegmentLayout1 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__segment_layout_1__["a" /* SegmentLayout1 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], SegmentLayout1Module);
    return SegmentLayout1Module;
}());

//# sourceMappingURL=segment-layout-1.module.js.map

/***/ }),

/***/ 704:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegmentLayout2Module", function() { return SegmentLayout2Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__segment_layout_2__ = __webpack_require__(905);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SegmentLayout2Module = /** @class */ (function () {
    function SegmentLayout2Module() {
    }
    SegmentLayout2Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__segment_layout_2__["a" /* SegmentLayout2 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__segment_layout_2__["a" /* SegmentLayout2 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__segment_layout_2__["a" /* SegmentLayout2 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], SegmentLayout2Module);
    return SegmentLayout2Module;
}());

//# sourceMappingURL=segment-layout-2.module.js.map

/***/ }),

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SegmentLayout3Module", function() { return SegmentLayout3Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__segment_layout_3__ = __webpack_require__(906);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SegmentLayout3Module = /** @class */ (function () {
    function SegmentLayout3Module() {
    }
    SegmentLayout3Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__segment_layout_3__["a" /* SegmentLayout3 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__segment_layout_3__["a" /* SegmentLayout3 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__segment_layout_3__["a" /* SegmentLayout3 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], SegmentLayout3Module);
    return SegmentLayout3Module;
}());

//# sourceMappingURL=segment-layout-3.module.js.map

/***/ }),

/***/ 749:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemDetailsPageTimeLineModule", function() { return ItemDetailsPageTimeLineModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__item_details_segment__ = __webpack_require__(950);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_segment_layout_1_segment_layout_1_module__ = __webpack_require__(703);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_segment_layout_2_segment_layout_2_module__ = __webpack_require__(704);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_segment_layout_3_segment_layout_3_module__ = __webpack_require__(705);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var ItemDetailsPageTimeLineModule = /** @class */ (function () {
    function ItemDetailsPageTimeLineModule() {
    }
    ItemDetailsPageTimeLineModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__item_details_segment__["a" /* ItemDetailsPageSegment */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__item_details_segment__["a" /* ItemDetailsPageSegment */]),
                __WEBPACK_IMPORTED_MODULE_3__components_segment_layout_1_segment_layout_1_module__["SegmentLayout1Module"], __WEBPACK_IMPORTED_MODULE_4__components_segment_layout_2_segment_layout_2_module__["SegmentLayout2Module"], __WEBPACK_IMPORTED_MODULE_5__components_segment_layout_3_segment_layout_3_module__["SegmentLayout3Module"]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ItemDetailsPageTimeLineModule);
    return ItemDetailsPageTimeLineModule;
}());

//# sourceMappingURL=item-details-segment.module.js.map

/***/ }),

/***/ 904:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentLayout1; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SegmentLayout1 = /** @class */ (function () {
    function SegmentLayout1() {
        var _this = this;
        this.selectedItem = "Page1";
        this.onEvent = function (event, item, e) {
            if (e) {
                e.stopPropagation();
            }
            if (_this.events[event]) {
                _this.events[event](item);
            }
        };
    }
    SegmentLayout1.prototype.ngOnChanges = function (changes) {
        this.data = changes['data'].currentValue;
    };
    SegmentLayout1.prototype.isEnabled = function (value) {
        return this.selectedItem == value;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('data'),
        __metadata("design:type", Object)
    ], SegmentLayout1.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('events'),
        __metadata("design:type", Object)
    ], SegmentLayout1.prototype, "events", void 0);
    SegmentLayout1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'segment-layout-1',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/segment/layout-1/segment.html"*/'<!-- Theme Segment - Segment List -->\n\n<ion-header>\n\n  <ion-navbar>\n\n      <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n      <ion-title *ngIf="data != null">{{data.headerTitle}}</ion-title>\n\n  </ion-navbar>\n\n  <!--Segment Section -->\n\n  <ion-toolbar no-padding>\n\n    <div segment-block *ngIf="data != null">\n\n      <ion-segment has-header [(ngModel)]="selectedItem">\n\n        <ion-segment-button text-capitalize value="Page1">\n\n          {{data.segmentButton1}}\n\n        </ion-segment-button>\n\n        <ion-segment-button text-capitalize value="Page2">\n\n          {{data.segmentButton2}}\n\n        </ion-segment-button>\n\n      </ion-segment>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n<!-- Content-->\n\n<ion-content>\n\n  <ion-grid no-padding *ngIf="data != null">\n\n    <!--- SETTINGS PAGE1 SCREEN -->\n\n    <ion-row *ngIf="isEnabled(\'Page1\')">\n\n      <ion-col col-12>\n\n        <div background-size [ngStyle]="{\'background-image\': \'url(\' + data.page1.background + \')\'}"></div>\n\n        <ion-item no-lines no-padding *ngFor="let item of data.page1.items; let i= index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n          <ion-thumbnail item-start no-margin padding-right>\n\n            <img [src]="item.image" alt="">\n\n          </ion-thumbnail>\n\n          <h2 item-title>{{item.title}}</h2>\n\n          <h3 item-subitem>{{item.category}}</h3>\n\n          <div item-content>\n\n            <span span-small>{{item.price}}</span>\n\n          </div>\n\n        </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <!--- SETTINGS PAGE2 SCREEN -->\n\n    <ion-row *ngIf="isEnabled(\'Page2\')">\n\n      <ion-col col-12>\n\n        <div background-size [ngStyle]="{\'background-image\': \'url(\' + data.page2.background + \')\'}"></div>\n\n        <!-- <img [src]="data.login.background" alt=""> -->\n\n        <ion-item no-lines no-padding *ngFor="let item of data.page2.items; let i= index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n          <ion-thumbnail item-start no-margin padding-right>\n\n            <img [src]="item.image" alt="">\n\n          </ion-thumbnail>\n\n          <h2 item-title>{{item.title}}</h2>\n\n          <h3 item-subitem>{{item.category}}</h3>\n\n          <div item-content>\n\n            <span span-small>{{item.price}}</span>\n\n          </div>\n\n        </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/segment/layout-1/segment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], SegmentLayout1);
    return SegmentLayout1;
}());

//# sourceMappingURL=segment-layout-1.js.map

/***/ }),

/***/ 905:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentLayout2; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SegmentLayout2 = /** @class */ (function () {
    function SegmentLayout2() {
        var _this = this;
        this.selectedItem = "Page1";
        this.onEvent = function (event, item, e) {
            if (e) {
                e.stopPropagation();
            }
            if (_this.events[event]) {
                _this.events[event](item);
            }
        };
    }
    SegmentLayout2.prototype.ngOnChanges = function (changes) {
        this.data = changes['data'].currentValue;
    };
    SegmentLayout2.prototype.isEnabled = function (value) {
        return this.selectedItem == value;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('data'),
        __metadata("design:type", Object)
    ], SegmentLayout2.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('events'),
        __metadata("design:type", Object)
    ], SegmentLayout2.prototype, "events", void 0);
    SegmentLayout2 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'segment-layout-2',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/segment/layout-2/segment.html"*/'<!-- Theme Segment - Segment List -->\n\n<ion-header>\n\n  <ion-navbar>\n\n      <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n      <ion-title *ngIf="data != null">{{data.headerTitle}}</ion-title>\n\n  </ion-navbar>\n\n  <!--Segment Section -->\n\n  <ion-toolbar no-padding>\n\n    <div segment-block *ngIf="data != null">\n\n      <ion-segment has-header [(ngModel)]="selectedItem">\n\n        <ion-segment-button text-capitalize value="Page1">\n\n          {{data.segmentButton1}}\n\n        </ion-segment-button>\n\n        <ion-segment-button text-capitalize value="Page2">\n\n          {{data.segmentButton2}}\n\n        </ion-segment-button>\n\n      </ion-segment>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n<!-- Content-->\n\n<ion-content>\n\n  <ion-grid no-padding *ngIf="data != null">\n\n    <!--- SETTINGS PAGE1 SCREEN -->\n\n    <ion-row *ngIf="isEnabled(\'Page1\')">\n\n        <ion-col col-12 col-lg-4 *ngFor="let item of data.page1.items; let i= index" >\n\n          <ion-card text-left box-shadow margin-bottom padding>\n\n            <img [src]="item.image" />\n\n            <ion-card-content no-padding margin-top (click)="onEvent(\'onItemClick\', item, $event)">\n\n              <span font-bold font-small ion-text color="accent">{{item.category}}</span>\n\n              <h2 card-title text-capitalize>{{item.title}}</h2>\n\n              <p card-body-text>{{item.description}}</p>\n\n            </ion-card-content>\n\n          </ion-card>\n\n        </ion-col>\n\n    </ion-row>\n\n    <!--- SETTINGS PAGE2 SCREEN -->\n\n    <ion-row *ngIf="isEnabled(\'Page2\')">\n\n      <ion-col col-12 col-lg-4 *ngFor="let item of data.page2.items; let i= index">\n\n        <ion-card text-left box-shadow margin-bottom padding>\n\n            <img [src]="item.image" />\n\n          <ion-card-content no-padding margin-top (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <span font-bold font-small ion-text color="accent">{{item.category}}</span>\n\n            <h2 card-title text-capitalize>{{item.title}}</h2>\n\n            <p card-body-text>{{item.description}}</p>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/segment/layout-2/segment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], SegmentLayout2);
    return SegmentLayout2;
}());

//# sourceMappingURL=segment-layout-2.js.map

/***/ }),

/***/ 906:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SegmentLayout3; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SegmentLayout3 = /** @class */ (function () {
    function SegmentLayout3() {
        var _this = this;
        this.selectedItem = "Page1";
        this.onEvent = function (event, item, e) {
            if (e) {
                e.stopPropagation();
            }
            if (_this.events[event]) {
                _this.events[event](item);
            }
        };
    }
    SegmentLayout3.prototype.ngOnChanges = function (changes) {
        this.data = changes['data'].currentValue;
    };
    SegmentLayout3.prototype.isEnabled = function (value) {
        return this.selectedItem == value;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('data'),
        __metadata("design:type", Object)
    ], SegmentLayout3.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('events'),
        __metadata("design:type", Object)
    ], SegmentLayout3.prototype, "events", void 0);
    SegmentLayout3 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'segment-layout-3',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/segment/layout-3/segment.html"*/'<!-- Theme Segment - Segment Post -->\n\n<ion-header>\n\n  <ion-navbar>\n\n      <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n      <ion-title *ngIf="data != null">{{data.headerTitle}}</ion-title>\n\n  </ion-navbar>\n\n  <!--Segment Section -->\n\n  <ion-toolbar no-padding>\n\n    <div segment-block *ngIf="data != null">\n\n      <ion-segment has-header [(ngModel)]="selectedItem">\n\n        <ion-segment-button text-capitalize value="Page1">\n\n          {{data.segmentButton1}}\n\n        </ion-segment-button>\n\n        <ion-segment-button text-capitalize value="Page2">\n\n          {{data.segmentButton2}}\n\n        </ion-segment-button>\n\n      </ion-segment>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n<!-- Content-->\n\n<ion-content>\n\n  <ion-grid no-padding *ngIf="data != null">\n\n    <!--- SETTINGS PAGE1 SCREEN -->\n\n    <ion-row *ngIf="isEnabled(\'Page1\')">\n\n        <ion-col col-12 col-lg-6 col-xl-4 *ngFor="let item of data.page1.items; let i= index"  (click)="onEvent(\'onItemClick\', item, $event)">\n\n          <!-- Card -->\n\n          <ion-card padding box-shadow (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <img [src]="item.image" />\n\n             <ion-card-header>\n\n               <span font-bold font-small ion-text color="accent">{{item.time}}</span>\n\n               <h2 card-title>{{item.title}}</h2>\n\n               <p card-subtitle text-wrap>{{item.description}}</p>\n\n             </ion-card-header>\n\n            <ion-card-content no-padding>\n\n              <ion-row no-padding>\n\n                <ion-col col-auto no-padding>\n\n                  <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onLike\', item, $event)">\n\n                    <ion-icon [ngClass]="{\'active\' : item.like.isActive}" [name]="item.like.icon"></ion-icon>\n\n                    {{item.like.number}}\n\n                  </button>\n\n                </ion-col>\n\n                <ion-col col-auto no-padding>\n\n                  <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onComment\', item, $event)">\n\n                    <ion-icon  [ngClass]="{\'active\' : item.comment.isActive}" [name]="item.comment.icon"></ion-icon>\n\n                    {{item.comment.number}}\n\n                  </button>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-card-content>\n\n          </ion-card>\n\n        </ion-col>\n\n    </ion-row>\n\n    <!--- SETTINGS PAGE2 SCREEN -->\n\n    <ion-row *ngIf="isEnabled(\'Page2\')">\n\n      <ion-col col-12 col-lg-6 col-xl-4 *ngFor="let item of data.page2.items; let i= index" (click)="onEvent(\'onItemClick\', item, $event)">\n\n        <!-- Card -->\n\n        <ion-card padding box-shadow>\n\n          <img [src]="item.image" />\n\n           <ion-card-header>\n\n             <span font-bold font-small ion-text color="accent">{{item.time}}</span>\n\n             <h2 card-title>{{item.title}}</h2>\n\n             <p card-subtitle text-wrap>{{item.description}}</p>\n\n           </ion-card-header>\n\n          <ion-card-content no-padding>\n\n            <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onLike\', item, $event)">\n\n              <ion-icon [ngClass]="{\'active\' : item.like.isActive}" [name]="item.like.icon"></ion-icon>\n\n              {{item.like.number}}\n\n            </button>\n\n            <button ion-button button-icon-clear text-capitalize icon-start clear (click)="onEvent(\'onComment\', item, $event)">\n\n              <ion-icon  [ngClass]="{\'active\' : item.comment.isActive}" [name]="item.comment.icon"></ion-icon>\n\n              {{item.comment.number}}\n\n            </button>\n\n          </ion-card-content>\n\n        </ion-card>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/segment/layout-3/segment.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], SegmentLayout3);
    return SegmentLayout3;
}());

//# sourceMappingURL=segment-layout-3.js.map

/***/ }),

/***/ 950:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemDetailsPageSegment; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemDetailsPageSegment = /** @class */ (function () {
    function ItemDetailsPageSegment(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.params = {};
        // If we navigated to this page, we will have an item available as a nav param
        this.page = navParams.get('page');
        this.service = navParams.get('service');
        if (this.service) {
            this.params = this.service.prepareParams(this.page, navCtrl);
            this.params.data = this.service.load(this.page);
        }
        else {
            navCtrl.setRoot("HomePage");
        }
    }
    ItemDetailsPageSegment = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-segment/item-details-segment.html"*/'<!--Segment Boxes component-->\n\n\n\n<!--PAGE SEGMENT-LIST-->\n\n<segment-layout-1 *ngIf="params.segmentLayout1" [data]="params.data | async" [events]="params.events">\n\n</segment-layout-1>\n\n\n\n<!--PAGE SEGMENT-CARD-->\n\n<segment-layout-2 *ngIf="params.segmentLayout2" [data]="params.data | async" [events]="params.events">\n\n</segment-layout-2>\n\n\n\n<!--PAGE SEGMENT-POST-->\n\n<segment-layout-3 *ngIf="params.segmentLayout3" [data]="params.data | async" [events]="params.events">\n\n</segment-layout-3>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-segment/item-details-segment.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ItemDetailsPageSegment);
    return ItemDetailsPageSegment;
}());

//# sourceMappingURL=item-details-segment.js.map

/***/ })

});
//# sourceMappingURL=18.js.map