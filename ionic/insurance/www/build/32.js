webpackJsonp([32,61],{

/***/ 719:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeLineLayout3Module", function() { return TimeLineLayout3Module; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__time_line_layout_3__ = __webpack_require__(920);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TimeLineLayout3Module = /** @class */ (function () {
    function TimeLineLayout3Module() {
    }
    TimeLineLayout3Module = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__time_line_layout_3__["a" /* TimeLineLayout3 */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__time_line_layout_3__["a" /* TimeLineLayout3 */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__time_line_layout_3__["a" /* TimeLineLayout3 */]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], TimeLineLayout3Module);
    return TimeLineLayout3Module;
}());

//# sourceMappingURL=time-line-layout-3.module.js.map

/***/ }),

/***/ 757:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemDetailsPageTimeLineModule", function() { return ItemDetailsPageTimeLineModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__item_details_time_line__ = __webpack_require__(958);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_time_line_layout_3_time_line_layout_3_module__ = __webpack_require__(719);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ItemDetailsPageTimeLineModule = /** @class */ (function () {
    function ItemDetailsPageTimeLineModule() {
    }
    ItemDetailsPageTimeLineModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__item_details_time_line__["a" /* ItemDetailsPageTimeLine */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__item_details_time_line__["a" /* ItemDetailsPageTimeLine */]),
                __WEBPACK_IMPORTED_MODULE_3__components_time_line_layout_3_time_line_layout_3_module__["TimeLineLayout3Module"]
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["i" /* CUSTOM_ELEMENTS_SCHEMA */]]
        })
    ], ItemDetailsPageTimeLineModule);
    return ItemDetailsPageTimeLineModule;
}());

//# sourceMappingURL=item-details-time-line.module.js.map

/***/ }),

/***/ 920:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TimeLineLayout3; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_dal_dal__ = __webpack_require__(343);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TimeLineLayout3 = /** @class */ (function () {
    function TimeLineLayout3(dal) {
        this.dal = dal;
    }
    TimeLineLayout3.prototype.ngOnInit = function () {
        this.getPaymentList();
    };
    TimeLineLayout3.prototype.getPaymentList = function () {
        var _this = this;
        this.dal.getPayment().subscribe(function (y) {
            _this.payments = y;
            console.log("page.ts>>>>>", y);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('data'),
        __metadata("design:type", Object)
    ], TimeLineLayout3.prototype, "data", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])('events'),
        __metadata("design:type", Object)
    ], TimeLineLayout3.prototype, "events", void 0);
    TimeLineLayout3 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'time-line-layout-3',template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/time-line/layout-3/time-line.html"*/'<!--Themes Line - Timeline With Comments -->\n\n<!--Themes Simple -->\n\n<ion-content>\n\n\n\n  <ion-grid>\n\n    <ion-row>\n\n      <ion-col col-11 offset-1>\n\n        <ion-list margin-top>\n\n          <div item-block margin-bottom *ngFor="let payment of payments; let i= index"\n\n            (click)="onEvent(\'onItemClick\', item, $event)">\n\n            <span font-bold small-font ion-text color="accent">{{payment.expiryDate}}</span>\n\n            <div item-avatar box-shadow>\n\n              <ion-item margin-top no-lines>\n\n                <ion-avatar item-start>\n\n                  <img src="https://res.cloudinary.com/brainethic/image/upload/v1520613022/prasad_zlt5l2.jpg">\n\n                </ion-avatar>\n\n                <h2 item-title><b>Name</b> : {{payment.userId}}</h2>\n\n                <h3 item-subtitle><b>Policy No</b> :{{payment.policyNo}}</h3>\n\n              </ion-item>\n\n              <div content padding-bottom>\n\n                <h3 text-wrap item-subtitle no-margin text-capitalize>{{payment.amount}} <b>INR</b></h3>\n\n              </div>\n\n            </div>\n\n          </div>\n\n        </ion-list>\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-grid>\n\n</ion-content>'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/components/time-line/layout-3/time-line.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_dal_dal__["a" /* DalProvider */]])
    ], TimeLineLayout3);
    return TimeLineLayout3;
}());

//# sourceMappingURL=time-line-layout-3.js.map

/***/ }),

/***/ 958:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemDetailsPageTimeLine; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemDetailsPageTimeLine = /** @class */ (function () {
    function ItemDetailsPageTimeLine(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.params = {};
        // If we navigated to this page, we will have an item available as a nav param
        this.page = navParams.get('page');
        this.service = navParams.get('service');
        if (this.service) {
            this.params = this.service.prepareParams(this.page, navCtrl);
            this.params.data = this.service.load(this.page);
        }
        else {
            navCtrl.setRoot("HomePage");
        }
    }
    ItemDetailsPageTimeLine = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-time-line/item-details-time-line.html"*/'<!--Check Boxes component-->\n\n<ion-header>\n\n    <ion-navbar>\n\n        <button ion-button menuToggle>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n        <ion-title>{{params.title}}</ion-title>\n\n    </ion-navbar>\n\n</ion-header>\n\n<!--Content-->\n\n\n\n<!--PAGE CHECK BOX - Simple-->\n\n<time-line-layout-1 has-header *ngIf="params.timelineLayout1" [data]="params.data | async" [events]="params.events">\n\n</time-line-layout-1>\n\n\n\n<!--PAGE CHECK BOX - With Avatar-->\n\n<time-line-layout-2 has-header *ngIf="params.timelineLayout2" [data]="params.data | async" [events]="params.events">\n\n</time-line-layout-2>\n\n\n\n<!--PAGE CHECK BOX - With Details-->\n\n<time-line-layout-3 has-header *ngIf="params.timelineLayout3" [data]="params.data | async" [events]="params.events">\n\n</time-line-layout-3>\n\n'/*ion-inline-end:"/home/prasad/data/projects/be-project/e-insurance-app/ionic/insurance/src/pages/item-details-time-line/item-details-time-line.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* NavParams */]])
    ], ItemDetailsPageTimeLine);
    return ItemDetailsPageTimeLine;
}());

//# sourceMappingURL=item-details-time-line.js.map

/***/ })

});
//# sourceMappingURL=32.js.map