import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, Content, AlertController, NavController, NavParams } from 'ionic-angular';
import { DalProvider } from '../../../providers/dal/dal';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { SettersandgettersProvider } from '../../../providers/settersandgetters/settersandgetters';
import { UtilityProvider } from '../../../providers/utility/utility';
import { database } from 'firebase';
// import swal from 'sweetalert';
import { text } from '@angular/core/src/render3/instructions';
import { AndroidFingerprintAuth } from '@ionic-native/android-fingerprint-auth';

@IonicPage()
@Component({
    selector: 'profile-layout-5',
    templateUrl: 'profile.html'
})
export class ProfileLayout5 {
    @Input() data: any;
    @Input() events: any;
    @ViewChild(Content)
    content: Content;
    slider = {};
    customers: any;
    customerBackup: any;
    txt: any;
    ngOnInit() {
        // this.getCustomerList();
        this.getBackupData();
        this.txt = "Please enter your OTP";
    }




    constructor(private androidFingerprintAuth: AndroidFingerprintAuth, private alertCtrl: AlertController, private dal: DalProvider, public navCtrl: NavController, public navParams: NavParams, public utility: UtilityProvider,
        private faio: FingerprintAIO, public setAndGet: SettersandgettersProvider) { }




    otpPrompt() {
        let alert = this.alertCtrl.create({
            title: this.txt,
            message: 'Enter OTP sent to your registered mobile or email !!',
            inputs: [
                {
                    name: 'otp',
                    placeholder: 'Enter OTP here'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: (data: any) => {

                    }
                },
                {
                    text: 'validate',
                    handler: (data: any) => {
                        this.otpValReq.otp = data.otp;
                        console.log("otp prompt>>>>", data);
                        this.otpAuth()


                    }
                }
            ]
        });
        alert.present();
    }

    custName: any;
    custLocation: any;
    custMobile: any;
    custEmail: any;

    getBackupData() {
        this.dal.getCustomer({ name: this.dal.getUserId() }).subscribe(x => {

            this.customerBackup = x[0];
            console.log("backup data>>>>", this.customerBackup)

            this.otpValReq.userId = this.customerBackup.name;
            this.otpValReq.mobile = this.customerBackup.mobile;
            this.custLocation = this.customerBackup.location;
            this.otpValReq.email = this.customerBackup.email;


        })
    }

    getCustomerList() {

        this.dal.getCustomer({ name: this.dal.getUserId() }).subscribe(y => {

            this.customers = y[0];
            console.log("customer list >>>>>", this.customers)

        })

    }

    otpValReq = {
        otp: "",
        userId: "",
        mobile: "",
        email: ""
    }

    getOtp() {
        this.dal.requestOtp(this.otpValReq).subscribe(z => {
            // this.utility.presentAlert("otp received to your registered mobile and email");
            console.log("otp received>>>>>>>>", z);
            this.otpPrompt()

        })
    }

    otpAuth() {

        this.dal.validateOtp(this.otpValReq).subscribe(p => {

            if (p.success == true) {

                this.getCustomerList();
                // console.log("after true OTP>>>>>>>>>>", this.customers);
                this.utility.presentAlert("OTP validated and Data is Retrieved successfully !!")

            } else {

                this.txt = "OTP Failed to validate !! please try again.."
                this.otpPrompt()
                // this.otpAuth()
                return;
            }

        })


    }

    bioAuth() {

        console.log("faio says>>>>>>>", this.androidFingerprintAuth)
        this.androidFingerprintAuth.isAvailable()
            .then((result) => {
                if (result.isAvailable) {
                    // it is available

                    this.androidFingerprintAuth.encrypt({ clientId: this.otpValReq.userId, username: this.otpValReq.userId, password: this.otpValReq.userId })
                        .then(result => {
                            if (result.withFingerprint) {
                                // this.utility.presentAlert("Successfully Retrived Data.")
                                // this.utility.presentAlert(result);
                                this.getOtp();
                                console.log('Successfully encrypted credentials.');
                                console.log('Encrypted credentials: ' + result.token);

                            } else this.utility.presentAlert("Didn\'t authenticate!");
                        })
                        .catch(error => {
                            if (error === this.androidFingerprintAuth.ERRORS.FINGERPRINT_CANCELLED) {
                                console.log('Fingerprint authentication cancelled');
                                this.utility.presentAlert("Fingerprint authentication cancelled");
                            } else console.error(error)
                        });

                } else {
                    // fingerprint auth isn't available
                }
            })
            .catch(error => {
                console.error(error)
                this.utility.presentAlert(error);
            });


    }


    // slideHasChanged(slider, index): void {
    //     this.slider[index] = slider;
    //     if (2 == slider._activeIndex) {
    //         if (this.data.items) {
    //             this.data.items.splice(index, 1);
    //         } else {
    //             this.data.splice(index, 1);
    //         }
    //     }
    // }

    // onClickEvent(index): void {
    //     if (this.slider[index]) {
    //         this.slider[index].slidePrev(300);
    //     }
    // }

    // onEvent(event: string, item: any, e: any) {
    //     if (e) {
    //         e.stopPropagation();
    //     }
    //     if (this.events[event]) {
    //         this.events[event](item);
    //     }
    // }
}
