import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlumniRoutingModule } from './alumni-routing.module';

import { AlumniDashLayoutComponent } from '../../com/alumni-dashboard/admin-dash-layout/alumni-dash-layout.component';
import { AlumniDashComponent } from '../../com/alumni-dashboard/admin-dash/alumni-dash.component';
import { FormsModule } from '@angular/forms';
import { RegisterUserComponent } from '../../com/alumni-dashboard/register-user/register-user.component';
import { PaymentComponent } from '../../com/alumni-dashboard/payment/payment.component';
import { PaymentHistoryComponent } from '../../com/alumni-dashboard/payment-history/payment-history.component';
import { AlumniAptComponent } from '../../com/alumni-dashboard/admin-userlist/alumni-apt.component';

@NgModule({
  declarations: [
    AlumniAptComponent,
    AlumniDashComponent,
    AlumniDashLayoutComponent,
    RegisterUserComponent,
    PaymentComponent,
    PaymentHistoryComponent
  ],
  imports: [
    CommonModule,
    AlumniRoutingModule,
    FormsModule
  ]
})
export class AlumniModule { }
