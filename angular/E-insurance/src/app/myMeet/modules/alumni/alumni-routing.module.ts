import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlumniDashLayoutComponent } from '../../com/alumni-dashboard/admin-dash-layout/alumni-dash-layout.component';
import { AlumniDashComponent } from '../../com/alumni-dashboard/admin-dash/alumni-dash.component';
import { AlumniAptComponent } from '../../com/alumni-dashboard/admin-userlist/alumni-apt.component';
import { RegisterUserComponent } from '../../com/alumni-dashboard/register-user/register-user.component';
import { PaymentComponent } from '../../com/alumni-dashboard/payment/payment.component';
import { PaymentHistoryComponent } from '../../com/alumni-dashboard/payment-history/payment-history.component';

const routes: Routes = [
  {
    path: '', component: AlumniDashLayoutComponent,
    //canActivate: [AuthGuard],
    children: [
      { path: 'dash', component: AlumniDashComponent },
      { path: 'manage-user', component: AlumniAptComponent },
      { path: 'reg-user', component: RegisterUserComponent },
      { path: 'pay', component: PaymentComponent },
      { path: 'pay-history', component: PaymentHistoryComponent },


      { path: '**', redirectTo: '/dash' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlumniRoutingModule { }
