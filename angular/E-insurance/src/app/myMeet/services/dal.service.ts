import { Injectable } from '@angular/core';
import { CustomHttpService } from './custom-http.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DalService {


  constructor(private customHttp: CustomHttpService) { }



  login(payload) {
    return this.customHttp.postReq(`${environment.serverURL}/login`, payload)
  }

  register(payload) {
    return this.customHttp.postReq(`${environment.serverURL}/users/register`, payload)
  }

  registerCustomer(payload) {
    return this.customHttp.postReq(`${environment.serverURL}/customers/registerCustomer`, payload)
  }

  getCustomer() {
    return this.customHttp.getReq(`${environment.serverURL}/customers/listCustomer`)
  }

  deleteCustomer(name) {
    return this.customHttp.deleteReq(`${environment.serverURL}/customers/deleteCustomer`, { name })
  }

  getPayments() {
    return this.customHttp.getReq(`${environment.serverURL}/payments/get-payments`)
  }

  makePayment(payload) {
    return this.customHttp.postReq(`${environment.serverURL}/payments/save-payments`, payload)
  }

}
