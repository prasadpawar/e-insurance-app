import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { DalService } from 'src/app/myMeet/services/dal.service';
import { Router } from '@angular/router';
import { ValidationsService } from 'src/app/myMeet/services/validations.service';


@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {


  constructor(private dal: DalService, private router: Router, private validation: ValidationsService) { }
  currPolicyNo: String

  regReq = {
    policyNo: "",
    name: "",
    mobile: "",
    plan: "",
    term: "",
    purpose: "",
    sumAssured: "",
    location: "",
    email: ""
  }



  ngOnInit() {
    this.registerStatus = "Register New Policy"
  }

  registerStatus: any;

  registerVal() {
    this.registerStatus = null
    if (this.validation.emptyVal(this.regReq.policyNo)) {
      Swal.fire("error..", "Please Enter User Policy No !!", "error")
      this.registerStatus = "Please Enter User Policy No !!";
    } else
      if (this.validation.emptyVal(this.regReq.name)) {
        Swal.fire("error..", "Please Enter Name !!", "error")
        this.registerStatus = "Please Enter Name !!";
      }
      else
        if (this.validation.emptyVal(this.regReq.mobile)) {
          Swal.fire("error..", "Please Enter Mobile no. !!", "error")
          this.registerStatus = "Please Enter Mobile Number !!";
        }
        else
          if (this.validation.emptyVal(this.regReq.plan)) {
            Swal.fire("error..", "Please Enter Policy Plan !!", "error")
            this.registerStatus = "Please Enter Policy Plan !!";
          }
          else
            if (this.validation.emptyVal(this.regReq.term)) {
              Swal.fire("error..", "Please Enter Policy Term !!", "error")
              this.registerStatus = "Please Enter Policy Term !!";
            }
            else
              if (this.validation.emptyVal(this.regReq.sumAssured)) {
                Swal.fire("error..", "Please Enter Sum Assured !!", "error")
                this.registerStatus = "Please Enter Sum Assured !!";
              }
              else if
                (this.validation.emptyVal(this.regReq.purpose)) {
                Swal.fire("error..", "Please Enter Purpose !!", "error")
                this.registerStatus = "Please Enter purpose !!";
              }
              else if
                (this.validation.emptyVal(this.regReq.email)) {
                Swal.fire("error..", "Please Enter Email !!", "error")
                this.registerStatus = "Please Enter Email !!";
              }

    return this.registerStatus
  }



  registerCustomer() {


    if (null != this.registerVal()) {
      return
    }

    this.dal.registerCustomer(this.regReq).subscribe(y => {

      console.log(y);
      if (y._id) {

        console.log("Customer Registered Successfully!!")
        Swal.fire("Success..", "User Registered Successful !", "success")
        // this.registerStatus = "Customer Registered Successfully !!";
        // window.location.href = `/${environment.baseHref}/#/dashboard`
      } else if (y.code == 11000) {

        console.log("Customer already exist !!")
        Swal.fire("error..", "Customer already exist !!", "error")
        // this.registerStatus = "Customer already exist !! ";

      } else {
        Swal.fire("error..", "Customer registration Failed !! try again", "error")
        // this.registerStatus = "Customer registration Failed !! try again";

      }
      // console.log("User Registered Successfully !!")


    })

  }


}
