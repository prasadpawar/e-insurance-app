import { Component, OnInit } from '@angular/core';
import { DalService } from 'src/app/myMeet/services/dal.service';
import { Router } from '@angular/router';
import { ValidationsService } from 'src/app/myMeet/services/validations.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  constructor(private dal: DalService, private router: Router, private validation: ValidationsService) { }
  currUserId: String;

  payReq = {

    policyNo: "",
    name: "",
    userId: this.currUserId,
    amount: "",
    date: new Date(),
    expiryDate: "",
    cvv: "",
    cardNo: ""

  }




  getUserId() {
    let u = localStorage.getItem("user")
    try {
      u = JSON.parse(u)
      u = JSON.parse(u)
    } catch (error) {

    }
    if (u) {
      this.currUserId = u["userId"]
    } else return null

  }




  ngOnInit() {

    this.getUserId()
    console.log("userId", this.currUserId)
    this.payReq.userId = this.currUserId
    this.payStatus = "Register New Policy"
  }

  payStatus: any;

  payVal() {
    this.payStatus = null
    if (this.validation.emptyVal("" + this.payReq.policyNo)) {
      Swal.fire("error..", "Please Enter User Policy No !!", "error")
      this.payStatus = "Please Enter User Policy No !!";
    } else
      if (this.validation.emptyVal(this.payReq.name)) {
        Swal.fire("error..", "Please Enter Name !!", "error")
        this.payStatus = "Please Enter Name !!";
      }
      else
        if (this.validation.emptyVal("" + this.payReq.cardNo)) {
          Swal.fire("error..", "Please Enter card no. !!", "error")
          this.payStatus = "Please enter card number !!";
        }
        else
          if (this.validation.emptyVal("" + this.payReq.amount)) {
            Swal.fire("error..", "Please Enter Amount !!", "error")
            this.payStatus = "Please Enter Amount !!";
          }
          else
            if (this.validation.emptyVal("" + this.payReq.expiryDate)) {
              Swal.fire("error..", "Please Enter Expiry Date !!", "error")
              this.payStatus = "Please Enter Expiry Date !!";
            }
            else
              if (this.validation.emptyVal("" + this.payReq.cvv)) {
                Swal.fire("error..", "Please Enter cvv !!", "error")
                this.payStatus = "Please Enter CVV !!";
              }

    return this.payStatus
  }



  attemptPay() {

    if (null != this.payVal()) {
      return
    }

    this.dal.makePayment(this.payReq).subscribe(y => {

      console.log(y);
      if (y._id) {
        console.log("Payment Successful!!")
        console.log("payment details>>", this.payReq)
        Swal.fire("Success..", "Payment Successful !", "success")
        // this.payStatus = "Customer Registered Successfully !!";
        // window.location.href = `/${environment.baseHref}/#/dashboard`
      } else {
        Swal.fire("error..", "Payment Request Failed !! try again", "error")
        // this.payStatus = "Customer registration Failed !! try again";

      }
      // console.log("User Registered Successfully !!")


    })





  }

}
