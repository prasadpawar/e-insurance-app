import { Component, OnInit } from '@angular/core';
import { DalService } from 'src/app/myMeet/services/dal.service';

@Component({
  selector: 'app-payment-history',
  templateUrl: './payment-history.component.html',
  styleUrls: ['./payment-history.component.css']
})
export class PaymentHistoryComponent implements OnInit {

  constructor(private dal: DalService) { }

  payments: any;
  ngOnInit() {
    this.getPayments()
  }

  apt = {
    approved: 0,
    rejected: 0,
    total: 0
  }

  getPayments() {
    this.dal.getPayments().subscribe(r => {
      console.log("payments >>>>>>", r)
      this.payments = r
      this.apt.total = r.length

    })
  }
}
