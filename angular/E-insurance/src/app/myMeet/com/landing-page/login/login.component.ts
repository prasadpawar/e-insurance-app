import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DalService } from 'src/app/myMeet/services/dal.service';
import { ValidationsService } from 'src/app/myMeet/services/validations.service';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private router: Router, private dal: DalService, private validation: ValidationsService) { }


  loginReq = {
    userId: "",
    password: "",
  }

  regReq = {
    userId: "",
    password: "",
    rePassword: "",
  }


  loginStatus: any;
  registerStatus: any;

  ngOnInit() {
    this.loginStatus = "We're glad to see you again!";
    this.registerStatus = "Let's create your account!";
  }

  loginVal() {
    this.loginStatus = null
    if (this.validation.emptyVal(this.loginReq.userId)) {
      this.loginStatus = "Please Enter User Name !!";
    } else
      if (this.validation.emptyVal(this.loginReq.password)) {
        this.loginStatus = "Please Enter Password !!";
      }
    return this.loginStatus
  }

  registerVal() {
    this.registerStatus = null
    if (this.validation.emptyVal(this.regReq.userId)) {
      this.registerStatus = "Please Enter User Name !!";
    } else
      if (this.validation.emptyVal(this.regReq.password)) {
        this.registerStatus = "Please Enter Password !!";
      } else {

        if (this.validation.emptyVal(this.regReq.rePassword)) {
          this.registerStatus = "Please Enter repeat Password !!";
        }
      }
    return this.registerStatus
  }




  attemptLogin() {


    // console.log(this.validation.emptyVal(this.loginReq.userId), "this is validation service")
    if (null != this.loginVal()) {
      return
    }
    this.dal.login(this.loginReq).subscribe(x => {
      console.log(x)
      if (x.isSuccess == true) {
        localStorage.setItem("user", JSON.stringify(this.loginReq))
        console.log("Loggedin Successfully !!")
        this.loginStatus = "Loggedin Successfully !!";


        window.location.href = `/#/admin/dash`
      } else {
        console.log("Login Failed !!")
        this.loginStatus = "Login Failed !!";
        // window.location.href = `/${environment.baseHref}/#/`
        Swal.fire('Oops...', 'Username or Password entered is wrong!', 'error')

      }

    }, e => {

    })

  }

  attemptRegister() {

    if (null != this.registerVal()) {
      return
    }

    this.dal.register(this.regReq).subscribe(y => {

      console.log(y);
      if (y._id) {
        console.log("User Registered Successfully!!")
        this.registerStatus = "User Registered Successfully !!";
        // window.location.href = `/${environment.baseHref}/#/dashboard`
      } else if (y.code == 11000) {

        console.log("User already exist !!")
        this.registerStatus = "User already exist !! try another Username";

      } else {
        this.registerStatus = "User registration Failed !! try again";

      }
      // console.log("User Registered Successfully !!")


    })





  }


}
